<?php
session_start();
$id_sesion = session_id();
$mod = array("carrera", "plan","ciclo", "infraestructura", "programa", "desglose_infraestructura");
include ('app/modulos.php');
include ('app/sesion.php');

if(isset($_GET["id"])){
  if(trim($_GET["id"])!=""){
    $lista_des= des_infraestructura_lista_programa($_GET["id"]);
    $cabecera = programa($_GET["id"]);
    if($cabecera['id']==""){
      header("Location: infraestructurahis.php");
    }
  }
}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=4; include 'inc/header.php'; ?>

    <section id="principal">

      <div class="container">
        <div class="row">
          <div class="col-md-10">
          
          <?php 
            $lista_carreras= programa_carreras(); //$lista_carreras=carrera_lista();
            include ('inc/buscador.php'); 
          ?>

          <br><br>
          <?php 

           if(isset($_GET["id"]) && trim($_GET["id"])!=""){
              echo "<div class='cabecera'>" . $cabecera['carrera']['nombre']  . " - " . $cabecera['ciclo']['plan']['modalidad'] . " (" . $cabecera['ciclo']['descripcion'] .  " " . $cabecera['year'] . ")</div>";

              echo "<div>";
              echo "<a class='boton btnmin' href='infraestructurahisfrm.php?id=" . $_GET["id"] . "'>Agregar</a>";
              if(count($lista_des)>0)
                echo "<a class='boton btnmin' href='./reporte/infraestructura.php?id=" . $_GET["id"] . "' target='blank'>Generar Reporte</a>";
              echo "</div>";


              if(count($lista_des)>0){
        
             ?>
            <br>
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="col-md-4">Tipo</th>
                 <th>Cantidad</th>
                 <th class="col-md-1 acciones">Acción</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                
                  foreach ($lista_des as $l) {
                    echo "<tr>";
                    echo "<td>" . $l['infraestructura']['tipo'] . "</td><td>". $l['cantidad'] . "</td>";
                    echo "<td class='acciones'>";
                    echo "<a href='infraestructurahisfrm.php?id=" . $l['programa']['id'] . "&idi=" . $l['infraestructura']['id'] . "&type=update'><i class='fa fa-pencil' aria-hidden='true'></i></a>";
                    echo "<a onclick='loadWindow(500,175);loadScreens(\"inc/eliminar.php?id=" . $l['programa']['id'] . "&id2=".$l['infraestructura']['id']."&nombre=".$l['infraestructura']['tipo']."&entidad=desglose_infraestructura&finaldest=infraestructurahis.php?id=" . $l['programa']['id'] . "\",\"window\",this);'><i class='fa fa-trash' aria-hidden='true'></i></a>";
                    echo "</td>";  
                    echo "</tr>";  
                  }
                  
                ?>
              </tbody>
            </table>

            <?php

                } else{
                  echo "<br>Sin resultados";
                }
             }
            ?>
          </div>
          <div class="col-md-2">
              <div class="opciones">
                <span id="titulo">Infraestructura</span>
                  <a class="boton" href="infraestructura.php">Ver lista</a>
              </div>
              
          </div> 
        </div>
      </div>
    
    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
