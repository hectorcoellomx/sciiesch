<?php
session_start();
$id_sesion = session_id();
include ('app/modulos.php');
include ('app/sesion.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <title>SAI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=1; include 'inc/header.php'; ?>
    <section id="principal">
      <div class="container">
        <div class="row">
          <div class="col-md-8">
            <div class="ventanas">
              <span id="titulo">Alumnos</span>
              <div class="texto">Control de alumnos inscritos</div>
            </div>
            <div class="ventanas">
              <span id="titulo">Docentes</span>
              <div class="texto">Control de docentes inscritos</div>
            </div>
            <div class="ventanas">
              <span id="titulo">Infraestructura</span>
              <div class="texto">Control de infraestructura registrada</div>
            </div>

            <div id="menusecundario" class="botones">
                <div class="boton">Generar Reportes</div>
                <div class="boton">Usuarios del Sistema</div>
            </div>

          </div>
          <div class="col-md-4">
            <?php
            /*
            function getMayor($fecha1, $fecha2){
              return(strtotime($fecha1) > strtotime($fecha2));
            }

            if(getMayor("10-04-2016","10-04-2016")){
              echo "Es mayor";
            }else {
              echo "No es mayor";
            }*/

             ?>
            <img id="estudiantes" src="./img/estudiante.png">
          </div>
        </div>
      </div>
    </section>

    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
