<?php 
include ('../app/core/bd.php');
include ('../app/carrera/model.php');
include('../app/carrera/functions.php');
include ('../app/plan/model.php');
include('../app/plan/functions.php');
include ('../app/ciclo/model.php');
include('../app/ciclo/functions.php');

if(isset($_GET["nocontrol"])){

$lista=ciclo_lista();
?>
<span class="encabezado">Elige Ciclo
<div class="iconbox">
	<div id="disk" class="icon"></div>
	<div class="icon"></div>
	<div class="icon"></div>
	<div class="icon"></div>
</div>
</span>
<div class="cuerpo eliminar">
	Año:
	<select id="year" name="year">
      <?php 
        $y=date("Y");
        while ($y>=2000) {
          $sele="";
          if($update && $item["year"]==$y){
            $sele="selected";
          }
          echo "<option value='" . $y . "' " . $sele . ">" . $y . "</option>";  
          $y--;
        }
      ?>
    </select>
    <br>
    Ciclo:
	 <select id="id_ciclo" name="id_ciclo">
		<option selected disabled>Seleccionar</option>
		<?php 
		  if(is_array($lista)){
		    foreach ($lista as $l) {
		      echo '<option value="' . $l['id'] . '">' . $l['descripcion']  . '</option>';
		    }
		  } 
		?>
	</select>		
	<input id="nocontrol_docente" type="hidden" value="<?php echo $_GET["nocontrol"]; ?>"></input>
 	<div class="botones">
	    <div class="boton" onclick="irCiclo(1);">Aceptar</div>
	    <div class="boton" onclick="closeWindow();">Cancelar</div>
	</div>	
</div>

<?php } else{
	echo "Error";
} ?>
