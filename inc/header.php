<div id="fondo"></div>
<div id="window"></div>

<header id="main-header">

<div class="container">
    <div class="row">
			<div class="col-xs-12 col-sm-4 col-md-4">
				<img id="logo" src="./img/logo.png">
				<div id="titulo">
					<h2>Universidad Salazar</h2>
					<span>Sistema Analítico Institucional</span>
				</div>
			</div>
			<div class="col-xs-12 col-sm-8 col-md-8">
				<div id="user"><i class="fa fa-user"></i><span><?php echo $user['nombre']; ?></span>
				 <div id="menuuser">
				  <?php 
				  	echo ($user['tipo']==0)? '<a href="administracion.php">Administración</a>' : '';
				  ?>
				  <a href="logout.php">Cerrar sesión</a>
				 </div>
				</div>

				<ul id="main-menu">
				    <li <?php echo ($menu==1) ? 'class="activo"' : ''; ?> ><a href="index.php">Inicio</a></li>
				    <li <?php echo ($menu==2) ? 'class="activo"' : ''; ?> ><a href="alumnohis.php">Alumnos</a></li>
				    <li <?php echo ($menu==3) ? 'class="activo"' : ''; ?> ><a href="docentehis.php">Docentes</a></li>
				    <li <?php echo ($menu==4) ? 'class="activo"' : ''; ?> ><a href="infraestructurahis.php">Infraestructura</a></li>
				    <li <?php echo ($menu==5) ? 'class="activo"' : ''; ?> ><a href="programa.php">Programas</a></li>
				</ul>


			</div>
		</div>
	</div>

</header>
