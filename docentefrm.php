<?php
session_start();
$id_sesion = session_id();
$mod = array("estado","municipio","persona","docente");
include ('app/modulos.php');
include ('app/sesion.php');

$error_msj="";

 //POST
  if(isset($_POST["submit"])){

    if(!isset($_POST["update"])){
      $res=docente_nuevo();
      if($res=="done"){
        header('Location: docentecar.php?ciclo=activos&year='.date("Y").'&nocontrol='.$_POST['nocontrol']);
      }
      else
        $error_msj="No se ha podido realizar su registro, verificar que el 'No. Control' y/o Docente no se encuentre registrado.";
    }else{
      $res=docente_update();
      if($res=="done")
        header('Location: docente.php');
    }
  }

  // GET UPDATE OR CREATE
  $update=false;
  if(isset($_GET["type"]) && $_GET["type"]=="update"){
    if(isset($_GET["id"])){
      if($_GET["id"]!=""){
        $item=docente($_GET["id"]);
        $update=true;
      }
    }
  }


$lista_estado=estado_lista();

?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=3; include 'inc/header.php'; ?>


    <section id="principal">
      <form method="post" class="formulario">
      <div class="container">
        <div class="frmtitulo"><?php if($update){ echo "Actualizar docente"; } else { echo "Agregar docente"; } ?></div>
        <div class="row">
          <div class="col-md-6">
              <div class="fila row">
                <div class="tit col-md-4">
                    No. Control
                </div>
                <div class="dato col-md-8">
                    <input name="nocontrol" type="text" required <?php if($update){ echo "disabled value=" . $item["nocontrol"]; } ?>>
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Nombre
                </div>
                <div class="dato col-md-8">
                    <input name="nombre" type="text" required <?php if($update){ echo "value='" . $item["nombre"] . "'"; } ?>>
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    A. Paterno
                </div>
                <div class="dato col-md-8">
                    <input name="paterno" type="text" required <?php if($update){ echo "value='" . $item["paterno"] . "'"; } ?>>
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    A. Materno
                </div>
                <div class="dato col-md-8">
                    <input name="materno" type="text" required <?php if($update){ echo "value='" . $item["materno"] . "'"; } ?>>
                </div>
              </div>

               <div class="fila row">
                <div class="tit col-md-4">
                    Género
                </div>
                <div class="dato col-md-8">
                    <select name="genero">
                      <option value="0" <?php if($update && $item["genero"]==0){ echo "selected";} ?>>Femenino</option>
                      <option value="1" <?php if($update && $item["genero"]==1){ echo "selected";} ?>>Masculino</option>
                    </select>
                </div>

              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Fecha de Nacimiento
                </div>
                <div class="dato col-md-8">
                    <input name="nacimiento" placeholder="aaaa-mm-dd" type="date" required <?php if($update){ echo "value='" . $item["nacimiento"] . "'"; } ?>>
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Nacionalidad
                </div>
                <div class="dato col-md-8">
                     <select name="nacionalidad">
                        <option value="mexicana" <?php if($update && $item["nacionalidad"]=="mexicana"){ echo "selected";} ?>>Méxicana</option>
                        <option value="extranjera" <?php if($update && $item["nacionalidad"]=="extranjera"){ echo "selected";} ?>>Extranjera</option>
                     </select>
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Curp
                </div>
                <div class="dato col-md-8">
                    <input name="curp" type="text" required <?php if($update){ echo "value='" . $item["curp"] . "'"; } ?>>
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    E-mail
                </div>
                <div class="dato col-md-8">
                    <input name="email" type="text" required <?php if($update){ echo "value='" . $item["email"] . "'"; } ?>>
                </div>
              </div>

            </div>

            <div class="col-md-6">

              <div class="fila row">
                <div class="tit col-md-4">
                    Teléfono
                </div>
                <div class="dato col-md-8">
                    <input name="telefono" type="text" required <?php if($update){ echo "value='" . $item["telefono"] . "'"; } ?>>
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Fecha de Ingreso
                </div>
                <div class="dato col-md-8">
                    <input name="ingreso" type="date" placeholder="aaaa-mm-dd" required <?php if($update){ echo "value='" . $item["ingreso"] . "'"; } ?>>
                </div>
              </div>

              <div class="fila row">
                <div class="tit titpanel col-md-12">
                    Ubicación
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Dirección
                </div>
                <div class="dato col-md-8">
                    <input name="direccion" type="text" required <?php if($update){ echo "value='" . $item['direccion'] . "'"; } ?>>
                </div>
              </div>

               <div class="fila row">
                <div class="tit col-md-4">
                    Ciudad
                </div>
                <div class="dato col-md-8">
                    <input name="ciudad" type="text" required <?php if($update){ echo "value='" . $item["ciudad"] . "'"; } ?>>
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Estado
                </div>
                <div class="dato col-md-8">
                    <select name="id_estado">
                      <?php
                      if(is_array($lista_estado)){
                        foreach ($lista_estado as $l) {
                          $sel="";
                          if($update && $item["estado"]["id"]==$l['id']){
                            $sel="selected";
                          }
                          echo '<option value="' . $l['id'] . '" ' . $sel . '>' . $l['nombre'] . '</option>';
                        }
                      }
                    ?>
                    </select>
                </div>
              </div>

              <div class="fila row">
               <div class="tit col-md-4">
                   Municipio
               </div>
               <div class="dato col-md-8">
                   <input name="municipio" type="text" required <?php if($update){ echo "value='" . $item["municipio"] . "'"; } ?>>
               </div>
             </div>

             <div class="fila row">
               <div class="tit separa col-md-12"></div>
             </div>
             
              <div class="fila row">
                <div class="tit col-md-4">
                    Status
                </div>
                <div class="dato col-md-8">
                  <select name="edo">
                    <option value="0" <?php if($update && $item["edo"]==0){ echo "selected";} ?>>Desactivado</option>
                    <option value="1" <?php if($update && $item["edo"]==1){ echo "selected";} if(!$update) { echo "selected"; } ?>>Activado</option>
                  </select>
                </div>
              </div>

            <?php if($update){ echo "<input type='hidden' name='nocontrol' value='" . $item["nocontrol"] . "'>"; } ?>
            <?php if($update){ echo "<input type='hidden' name='update' value='true'>"; } ?>



          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
              <div class="botones">
                <input class="boton" type="submit" name="submit" value="Guardar">
                <a href='docente.php' class="boton">Cancelar</a>
              </div>
          </div>
        </div>
      </div>
    </form>
    </section>

    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

    <?php if($error_msj!=""){
      echo '<script>alert("' . $error_msj . '");</script>';
    } ?>
  </body>
</html>
