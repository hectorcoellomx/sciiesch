<?php
session_start();
$id_sesion = session_id();
$mod = array("carrera");
include ('app/modulos.php');
include ('app/sesion.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=5; include 'inc/header.php'; ?>

    <?php $lista=carrera_lista(); ?>

    <section id="principal">

      <div class="container">
        <?php rutas("programa,carrera"); ?>
        <div class="row">
          <div class="col-md-10">
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="col-md-2">ID</th>
                 <th class="col-md-5">Nombre</th>
                 <th>Director</th>
                 <th>Coordinador</th>
                 <th class="col-md-1 acciones">Acción</th>
                </tr>
              </thead>
              <tbody>
                <?php

                if(is_array($lista)){

                  foreach ($lista as $l) {
                    echo "<tr>";
                    echo "<td>" . $l['id'] . "</td><td>" . $l['nombre'] . "</td><td>" . $l['director'] . "</td><td>" . $l['coordinador'] . "</td>";
                    echo "<td class='acciones'>";
                    if($user['tipo']==0){
                      echo "<a href='carrerafrm.php?id=" . $l['id'] . "&type=update'><i class='fa fa-pencil' aria-hidden='true'></i></a>";
                      echo "<a onclick='loadWindow(500,175);loadScreens(\"inc/eliminar.php?id=" . $l['id'] . "&nombre=" . $l['nombre'] . "&entidad=carrera&finaldest=carrera.php\",\"window\",this);'><i class='fa fa-trash' aria-hidden='true'></i></a>";
                    }else {
                      echo "N/A";
                    }
                    echo "</td>";
                    echo "</tr>";
                  }

                }

                ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-2">
              <div class="opciones">
                <span id="titulo">Carrera</span>
                <?php if($user['tipo']==0){ ?>
                  <a class="boton" href="carrerafrm.php">Agregar</a>
                <?php }else{
                  echo "Únicamente el usuario Administrador puede agregar nuevas carreras.";
                } ?>
              </div>
          </div>
        </div>
      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
