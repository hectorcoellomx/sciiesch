<?php 
session_start();
$id_sesion = session_id();
$mod = array("carrera", "plan","ciclo", "infraestructura", "programa", "desglose_infraestructura");
include ('app/modulos.php');
include ('app/sesion.php');

// IS UPDATE
$update=false;
if(isset($_GET["type"]) && $_GET["type"]=="update"){
  if(isset($_GET["id"]) && isset($_GET["idi"])){
    if($_GET["id"]!=""){
      $update=true;
    }
  }
}else{

// IS NORMAL (Necesita ID)
 if(isset($_GET["id"])){
    if(trim($_GET["id"])==""){
      header("Location: infraestructurahis.php");
    }
 }else{
    header("Location: infraestructurahis.php");
 }

}

$yaexiste=false;

 //POST SUBMIT
  if(isset($_POST["submit"])){

    if(!isset($_POST["update"])){ // FOR CREATE
      $res=des_infraestructura_nuevo();
      if($res!="done"){
        if($res=="1062")
          $yaexiste=true;
      }
      else{
        if($_POST["continuar"]==0) {
          header('Location: infraestructurahis.php?id='.$_GET["id"]);
        }
      }
    }else{ // FOR UPDATE
      $res=des_infraestructura_update();
      if($res=="done")
        header('Location: infraestructurahis.php?id='.$_GET["id"]);
    }
  }

 
$lista=infraestructura_lista();
$cabecera= programa(trim($_GET['id']));
if($cabecera['id']==""){
  header("Location: infraestructurahis.php");
}
$lista_des= des_infraestructura_lista_programa($_GET["id"]);

if($update){
  $item=des_infraestructura($_GET["id"],$_GET["idi"]);
}

?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=5; include 'inc/header.php'; ?>
    
    <section id="principal">

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <form id="formulario" method="post" class="formulario">
              <div class="frmtitulo"><?php if($update){ echo "Actualizar registro"; } else { echo "Agregar registro"; } ?></div>

              <?php 
                echo "<div>" . $cabecera['carrera']['nombre']  . " - " . $cabecera['ciclo']['plan']['modalidad'] . " (" . $cabecera['ciclo']['descripcion'] .  " " . $cabecera['year'] . ")</div>";
                if($update)
                  echo "<b>" . $item['infraestructura']['tipo'] . "</b><br>";
               ?>
               <br>
              <?php if(!$update){  ?>
              <div class="fila row">
                <div class="tit col-md-4">
                    Infraestructura
                </div>
                <div class="dato col-md-8">
                    
                    <?php
                        echo '<select name="id_infraestructura" required>';
                        if(is_array($lista)){
                          foreach ($lista as $l) {
                            $add="";
                            foreach ($lista_des as $lis) {
                              if($lis['infraestructura']['id']==$l['id'])
                                $add="disabled";
                            }

                            echo '<option value="' . $l['id'] . '" ' . $add . '>' . $l['tipo'] . '</option>';
                          }
                        }
                        echo '</select>';
                       
                    ?>
                </div>
              </div>
              <?php }else{
                      
                      echo "<input type='hidden' name='id_infraestructura' value='" . $_GET["idi"] . "'>";
                    }
              ?>
              <div class="fila row">
                <div class="tit col-md-4">
                    Cantidad
                </div>
                <div class="dato col-md-8">
                    <input type="number" name="cantidad" required <?php if($update){ echo "value=" . $item['cantidad']; } ?>>
                </div>
              </div>
              <?php if(!$update){  ?>
              <div class="fila row">
                <div class="tit col-md-4">
                    Continuar Agregando
                </div>
                <div class="dato col-md-8">
                    <select name="continuar">
                      <option value="1">Si</option>
                      <option value="0">No</option>
                    </select>
                </div>
              </div>
              <?php }  ?>
              
              <?php echo "<input type='hidden' name='id_programa' value='" . $cabecera["id"] . "'>"; ?>
              <?php if($update){ echo "<input type='hidden' name='update' value='true'>"; } ?>

              <div class="botones">
                <input class="boton" type="submit" name="submit" value="Guardar">
                <a href='infraestructurahis.php?id=<?php echo $cabecera["id"]; ?>' class="boton">Regresar</a>
              </div>

            </form>
          </div>
        </div>
      </div>
    
    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>
    <?php if($yaexiste){
        echo "<script>alert('No se ha podido guardar el registo. Este dato ya existe registrado.');</script>";
      } ?>
  </body>
</html>
