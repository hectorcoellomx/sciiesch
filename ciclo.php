<?php
session_start();
$id_sesion = session_id();
$mod = array("plan", "ciclo");
include ('app/modulos.php');
include ('app/sesion.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=5; include 'inc/header.php'; ?>

    <?php $lista=ciclo_lista(); ?>

    <section id="principal">

      <div class="container">
        <?php rutas("programa,ciclo"); ?>
        <div class="row">
          <div class="col-md-10">
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="col-md-2">ID</th>
                 <th>Descripcion</th>
                 <th>Plan</th>
                 <th class="col-md-1 acciones">Acción</th>
                </tr>
              </thead>
              <tbody>
                <?php

                if(is_array($lista)){

                  foreach ($lista as $l) {
                    echo "<tr>";
                    echo "<td>" . $l['id'] . "</td><td>" . $l['descripcion'] . "</td><td>" . $l['plan']['modalidad'] . "</td>";
                    echo "<td class='acciones'>";
                    if($user['tipo']==0){
                      echo "<a href='ciclofrm.php?id=" . $l['id'] . "&type=update'><i class='fa fa-pencil' aria-hidden='true'></i></a>";
                      echo "<a onclick='loadWindow(500,175);loadScreens(\"inc/eliminar.php?id=" . $l['id'] . "&nombre=" . $l['descripcion'] . "&entidad=ciclo&finaldest=ciclo.php\",\"window\",this);'><i class='fa fa-trash' aria-hidden='true'></i></a>";
                    }else {
                      echo "N/A";
                    }
                    echo "</td>";
                    echo "</tr>";
                  }

                }

                ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-2">
              <div class="opciones">
                <span id="titulo">Ciclo</span>
                <?php if($user['tipo']==0){ ?>
                  <a class="boton" href="ciclofrm.php">Agregar</a>
                <?php }else{
                  echo "Únicamente el usuario Administrador puede agregar nuevos ciclos.";
                } ?>
              </div>
          </div>
        </div>
      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
