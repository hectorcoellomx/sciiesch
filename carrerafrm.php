<?php
session_start();
$id_sesion = session_id();
$mod = array("carrera");
include ('app/modulos.php');
include ('app/sesion.php');

if($user['tipo']!=0){
  header('Location: carrera.php');
}

 //POST
  if(isset($_POST["submit"])){

    if(!isset($_POST["update"])){
      $res=carrera_nuevo();
      if($res=="done")
        header('Location: carrera.php');
    }else{
      $res=carrera_update();
      if($res=="done")
        header('Location: carrera.php');
    }
  }

  // GET UPDATE OR CREATE
  $update=false;
  if(isset($_GET["type"]) && $_GET["type"]=="update"){
    if(isset($_GET["id"])){
      if($_GET["id"]!=""){
        $item=carrera($_GET["id"]);
        $update=true;
      }
    }
  }

?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=5; include 'inc/header.php'; ?>

    <section id="principal">

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <form id="formulario" method="post" class="formulario">
              <div class="frmtitulo"><?php if($update){ echo "Actualizar carrera"; } else { echo "Agregar carrera"; } ?></div>
              <div class="fila row">
                <div class="tit col-md-4">
                    Nombre
                </div>
                <div class="dato col-md-8">
                    <input type="text" name="nombre" required <?php if($update){ echo "value='" . $item["nombre"] . "'"; } ?>>
                </div>
              </div>
              <div class="fila row">
                <div class="tit col-md-4">
                    Director
                </div>
                <div class="dato col-md-8">
                    <input type="text" name="director" required <?php if($update){ echo "value='" . $item["director"] . "'"; } ?>>
                </div>
              </div>
              <div class="fila row">
                <div class="tit col-md-4">
                    Coordinador
                </div>
                <div class="dato col-md-8">
                    <input type="text" name="coordinador" required <?php if($update){ echo "value='" . $item["coordinador"] . "'"; } ?>>
                </div>
              </div>
               <?php if($update){ echo "<input type='hidden' name='id' value='" . $item["id"] . "'>"; } ?>
              <?php if($update){ echo "<input type='hidden' name='update' value='true'>"; } ?>

              <div class="botones">
                <input class="boton" type="submit" name="submit" value="Guardar">
                <a href='carrera.php' class="boton">Cancelar</a>
              </div>

            </form>
          </div>
        </div>
      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
