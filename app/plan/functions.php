<?php

function plan($id){
	$plan = new Plan($id);
	$datos = array("id"=>$plan->getId(), "modalidad"=>$plan->getModalidad());
    return $datos;
}

function plan_lista(){
	$plan = new Plan();
	$datos=$plan->get();
    return $datos;
}

function plan_nuevo(){
	$user_data = helper_plan_data_array();
	if(!empty($user_data)){
		$plan = new Plan();
    	return $plan->set($user_data);
    } else{
    	return "Invalido";
    }
}

function plan_update(){
	$user_data = helper_plan_data_array();
	if(!empty($user_data)){
		$plan = new Plan();
	    return $plan->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function plan_delete(){
	$user_data = helper_plan_data_array();
	if(!empty($user_data)){
		$plan = new Plan();
		return $plan->delete($user_data['id']);
	} else{
    	return "Invalid";
    }
}

// 	RECIBE POST ARRAY Y CONVIERTE A ARRAY

function helper_plan_data_array() {
	$user_data = array();

	if(array_key_exists('id', $_POST)) {
		$user_data['id'] = $_POST['id'];
	}

	if(array_key_exists('modalidad', $_POST)) {
		$user_data['modalidad'] = $_POST['modalidad'];
	}
	return $user_data;
}


?>
