<?php

class Plan extends bd
{
    private $id;
    private $modalidad;

    function __construct($id='') {
        if($id!=''){
            $this->query = "SELECT * FROM plan WHERE id = '$id'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                foreach ($this->rows[0] as $propiedad=>$valor):
                $this->$propiedad = utf8_encode($valor);
                endforeach;
            }
        }
    }

    public function get($id=''){
        $this->query = "SELECT * FROM plan";
        $this->get_results_from_query();

        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {  //$this->lista[] = $this->rows[$i];
                $cat = new Plan();
                foreach ($this->rows[$i] as $propiedad=>$valor):
                    $cat->$propiedad = utf8_encode($valor);
                endforeach;
                $datos = array("id"=>$cat->getId(), "modalidad"=>$cat->getModalidad());
                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
        if(array_key_exists('modalidad', $user_data)) {
                    foreach ($user_data as $campo=>$valor) {
                        $$campo = trim(utf8_decode($valor));
                    }
                    if($modalidad!=""){
                        $this->query = "INSERT INTO plan(modalidad) VALUES ('$modalidad')";
                        $this->execute_single_query();
                        $mensaje = $this->msj;
                    } else {
                        $mensaje = 'Invalid';
                    }

            } else {
                $mensaje = 'Invalid';
            }

            return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id!="" && $modalidad!=""){
            $this->query = "UPDATE plan SET modalidad='$modalidad' WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($user_id='') {
        if($user_id!=""){
            $this->query = "DELETE FROM plan WHERE id = '$user_id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getId(){
        return $this->id;
    }

    function getModalidad(){
        return $this->modalidad;
    }

    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
