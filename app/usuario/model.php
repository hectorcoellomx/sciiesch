<?php

class Usuario extends bd
{
    private $id;
    private $username;
    private $password;
    private $nombre;
    private $tipo;

    function __construct($id='') {
        if($id!=''){
            $this->query = "SELECT * FROM administrador WHERE id = '$id'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                foreach ($this->rows[0] as $propiedad=>$valor):
                    $this->$propiedad = utf8_encode($valor);
                endforeach;
            }
        }
    }

    public function get($edo=''){

        $this->query = "SELECT * FROM administrador ORDER BY `administrador`.`tipo` ASC";

        $this->get_results_from_query();
        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {  //$this->lista[] = $this->rows[$i];
                $cat = new Usuario();
                foreach ($this->rows[$i] as $propiedad=>$valor):
                    $cat->$propiedad = utf8_encode($valor);
                endforeach;
                $datos = array("id"=>$cat->getId(), "username"=>$cat->getUsername(), "nombre"=>$cat->getNombre(), "password"=>$cat->getPassword(), "tipo"=>$cat->getTipo());
                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($data=array()){
            if(trim($data['username'])!="" && trim($data['nombre'])!="" && trim($data['password'])!=""){
              $tipo = 1;
                $this->query = "INSERT INTO administrador(username,nombre,password,tipo) VALUES ('" . $data['username'] . "', '" . $data['nombre'] . "', md5(". $data['password'] . "), " . $tipo . ")";
                $this->execute_single_query();
                $mensaje = $this->msj;
            } else {
                $mensaje = 'Invalid';
            }

            return $mensaje;
    }

    public function edit($data=array()){
      $mensaje = 'Invalid';
      if(trim($data['id'])!="" && trim($data['username'])!="" && trim($data['nombre'])!="" && trim($data['cambiarpass'])!=""){

          if($data['cambiarpass']=="1") {
              if(trim($data['password'])!=""){
                  $this->query = "UPDATE administrador SET username='" . $data['username'] . "', nombre='" . $data['nombre'] . "', password=md5(" . $data['password'] . ") WHERE id = '" . $data['id'] . "'";
              }else{
                $this->query = "";
              }
          }else{
            $this->query = "UPDATE administrador SET username='" . $data['username'] . "', nombre='" . $data['nombre'] . "' WHERE id = '" . $data['id'] . "'";
          }
          if($this->query!=""){
            $this->execute_single_query();
            $mensaje = $this->msj;
          }
      }else{

      }
      return $mensaje;
    }

    public function delete($id='') {
        if($id!=""){
            $this->query = "DELETE FROM administrador WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getId(){
        return $this->id;
    }
    function getUsername(){
        return $this->username;
    }
    function getPassword(){
        return $this->password;
    }
    function getNombre(){
        return $this->nombre;
    }
    function getTipo(){
        return $this->tipo;
    }



    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
