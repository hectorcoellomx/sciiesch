<?php

function usuario($id){
	$usuario = new Usuario($id);
	$datos = array("id"=>$usuario->getId(), "username"=>$usuario->getUsername(), "password"=>$usuario->getPassword(), "nombre"=>$usuario->getNombre(), "tipo"=>$usuario->getTipo());
    return $datos;
}

function usuario_lista(){
	$usuario = new Usuario();
	$datos=$usuario->get();
    return $datos;
}

function usuario_nuevo(){
	$data = get_post_usuario();
	if(!empty($data)){
		$usuario = new Usuario();
    	return $usuario->set($data);
    } else{
    	return "Invalido";
    }
}

function usuario_update(){
	$data = get_post_usuario();
	if(!empty($data)){
		$usuario = new Usuario();
	    return $usuario->edit($data);
    } else{
    	return format_resp("Invalid");
    }
}

function usuario_delete(){
	$data = get_post_usuario();
	if(!empty($data)){
		$usuario = new Usuario();
		return $usuario->delete($data['id']);
	} else{
    	return format_resp("Invalid");
    }
}


function get_post_usuario() {
	$user_data = array();

	if(array_key_exists('id', $_POST)) {
		$user_data['id'] = $_POST['id'];
	}

	if(array_key_exists('username', $_POST)) {
		$user_data['username'] = $_POST['username'];
	}

	if(array_key_exists('nombre', $_POST)) {
		$user_data['nombre'] = $_POST['nombre'];
	}

	if(array_key_exists('password', $_POST)) {
		$user_data['password'] = $_POST['password'];
	}

	if(array_key_exists('tipo', $_POST)) {
		$user_data['tipo'] = $_POST['tipo'];
	}

	if(array_key_exists('cambiarpass', $_POST)) {
		$user_data['cambiarpass'] = $_POST['cambiarpass'];
	}

	return $user_data;
}

?>
