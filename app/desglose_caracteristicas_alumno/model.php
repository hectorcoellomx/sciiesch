<?php

class Desglose_caracteristicas_alumno extends bd
{
    private $alumno;
    private $caracteristicas;
    private $descripcion;
    private $valor;
    private $fecha;

    function __construct($id_caracteristica='', $nocontrol='', $fecha='', $accion='') {
        if($fecha!="" && $nocontrol!="" && $id_caracteristica!='' && $accion!=''){

            if($accion=="agregar")
              $this->query = "SELECT * FROM des_alumno_caracteristica WHERE fecha='" . $fecha . "' AND nocontrol_alumno='" . $nocontrol . "' AND id_caracteristica='" . $id_caracteristica . "'";
            else
              $this->query = "SELECT * FROM des_alumno_caracteristica WHERE fecha<='" . $fecha . "' AND nocontrol_alumno='" . $nocontrol . "' AND id_caracteristica='" . $id_caracteristica . "' ORDER BY fecha DESC";

            $this->get_results_from_query();
            if(count($this->rows) >= 1){
              //echo "a-";
                foreach ($this->rows[0] as $propiedad=>$valor):
                    if ($propiedad=="nocontrol_alumno")
                        $this->alumno = alumno($valor);
                    elseif ($propiedad=="id_caracteristica")
                        $this->caracteristicas = caracteristicas($valor);
                    else
                        $this->$propiedad = utf8_encode($valor);
                endforeach;
            }/*else{
              echo "b";
            }*/
        }
    }

    public function get($nocontrol='', $fecha='', $accion=''){
        if($fecha!="" && $nocontrol!="" && $accion=="check")
            $this->query = "SELECT DISTINCT id_caracteristica FROM des_alumno_caracteristica WHERE fecha<='" . $fecha . "' AND nocontrol_alumno='" . $nocontrol . "' ORDER BY fecha DESC";

        if($fecha!="" && $nocontrol!="" && $accion=="full"){

            $sql_mayores= "SELECT id_caracteristica as id_c, nocontrol_alumno as nocontrol_d,  max(fecha) as f FROM des_alumno_caracteristica WHERE fecha<='" . $fecha . "' AND nocontrol_alumno='" . $nocontrol . "' GROUP BY id_caracteristica";
            $this->query = "SELECT id_caracteristica, valor, nocontrol_alumno, descripcion, fecha FROM des_alumno_caracteristica INNER JOIN (" . $sql_mayores . ") t2 ON (des_alumno_caracteristica.id_caracteristica=t2.id_c AND des_alumno_caracteristica.fecha=t2.f AND des_alumno_caracteristica.nocontrol_alumno=t2.nocontrol_d)";

            //die($this->query);
        }

        $this->get_results_from_query();
        $lista = array();

        if (count($this->rows) >= 1) {
            if($accion=="check")
              return count($this->rows);
            else{
              for ($i=0 ; $i < count($this->rows); $i++ ) {
                  $cat = new Desglose_caracteristicas_alumno();
                  foreach ($this->rows[$i] as $propiedad=>$valor):
                      if ($propiedad=="nocontrol_alumno")
                          $cat->alumno = alumno($valor);
                      elseif ($propiedad=="id_caracteristica")
                          $cat->caracteristicas = caracteristicas($valor);
                      else
                          $cat->$propiedad = utf8_encode($valor);
                  endforeach;

                  $datos = array("alumno"=>$cat->getAlumno(),"descripcion"=>$cat->getDescripcion(), "valor"=>$cat->getValor(),"fecha"=>$cat->getFecha(),"caracteristicas"=>$cat->getCaracteristicas());
                  $lista[] = $datos;
              }
          }

        }else{
          if($accion=="check")
            return count($this->rows);
        }

        return $lista;

    }

    public function set($fecha='',$nocontrol_alumno='', $id_caracteristica='', $descripcion='', $valor=''){
        /*foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }*/
        if($id_caracteristica!="" && $nocontrol_alumno!="" && $fecha!=""){
            $this->query = "INSERT INTO des_alumno_caracteristica(id_caracteristica, nocontrol_alumno, descripcion, fecha,valor) VALUES ($id_caracteristica,'$nocontrol_alumno','$descripcion', '$fecha' , $valor)";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else {
            $mensaje = 'Invalid';
        }

        return $mensaje;
    }

    public function edit($fecha='',$nocontrol_alumno='', $id_caracteristica='', $descripcion='', $valor=''){
        /*foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }*/
        if($id_caracteristica!="" && $nocontrol_alumno!="" && $fecha!=""){
            $this->query = "UPDATE des_alumno_caracteristica SET valor=$valor, descripcion='$descripcion' WHERE id_caracteristica=" . $id_caracteristica . " AND fecha='" . $fecha . "' AND nocontrol_alumno='" . $nocontrol_alumno . "'";
            //die($this->query);
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($fecha='',$nocontrol='', $id_caracteristica='') {
        if($fecha!="" && $nocontrol!="" && $id_caracteristica!=""){
            $this->query = "DELETE FROM des_alumno_caracteristica WHERE fecha='" . $fecha . "' AND nocontrol_alumno='" . $nocontrol . "'". " AND id_caracteristica='" . $id_caracteristica . "'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getAlumno(){
        return $this->alumno;
    }

    function getCaracteristicas(){
        return $this->caracteristicas;
    }

    function getDescripcion(){
        return $this->descripcion;
    }

    function getFecha(){
        return $this->fecha;
    }

    function getValor(){
        return $this->valor;
    }

    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
