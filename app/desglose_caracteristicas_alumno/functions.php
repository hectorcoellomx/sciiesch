<?php

function desglose_caracteristicas_alumno($id_caracteristica,$nocontrol,$fecha,$accion){
	$desglose_caracteristicas_alumno = new Desglose_caracteristicas_alumno($id_caracteristica,$nocontrol,$fecha,$accion);
	$datos = array("fecha"=>$desglose_caracteristicas_alumno->getFecha(),"alumno"=>$desglose_caracteristicas_alumno->getAlumno(), "descripcion"=>$desglose_caracteristicas_alumno->getDescripcion(), "valor"=>$desglose_caracteristicas_alumno->getValor(), "caracteristicas"=>$desglose_caracteristicas_alumno->getCaracteristicas());
	return $datos;
}

function desglose_caracteristicas_alumno_lista(){
	$desglose_caracteristicas_alumno = new Desglose_caracteristicas_alumno();
	$datos=$desglose_caracteristicas_alumno->get();
    return $datos;
}
function desglose_caracteristicas_check($nocontrol,$fecha){
	$desglose_caracteristicas_alumno = new Desglose_caracteristicas_alumno();
	$datos=$desglose_caracteristicas_alumno->get($nocontrol,$fecha,"check");
  	return $datos;
}
function desglose_caracteristicas_full($nocontrol,$fecha){
	$desglose_caracteristicas_alumno = new Desglose_caracteristicas_alumno();
	$datos=$desglose_caracteristicas_alumno->get($nocontrol,$fecha,"full");
  	return $datos;
}

function desglose_caracteristicas_alumno_lista_programa($id_programa){
	$desglose_caracteristicas_alumno = new Desglose_caracteristicas_alumno();
	$datos=$desglose_caracteristicas_alumno->get($id_programa);
    return $datos;
}

function desglose_caracteristicas_alumno_nuevo($fecha,$nocontrol,$id_caracteristica,$descripcion,$valor){
	//$user_data = helper_desglose_caracteristicas_alumno_data_array();
	if(1==1){
		$desglose_caracteristicas_alumno = new Desglose_caracteristicas_alumno();
    	return $desglose_caracteristicas_alumno->set($fecha,$nocontrol,$id_caracteristica,$descripcion,$valor);
    } else{
    	return "Invalido";
    }
}

function desglose_caracteristicas_alumno_update($fecha,$nocontrol,$id_caracteristica,$descripcion,$valor){
	//$user_data = helper_desglose_caracteristicas_alumno_data_array();
	if(1==1){
		$desglose_caracteristicas_alumno = new Desglose_caracteristicas_alumno();
	    return $desglose_caracteristicas_alumno->edit($fecha,$nocontrol,$id_caracteristica,$descripcion,$valor);
    } else{
    	return "Invalid";
    }
}

function desglose_caracteristicas_alumno_delete($id_ciclo,$year,$nocontrol,$id_caracteristica){
	if($id_ciclo!="" && $year!="" && $nocontrol!="" && $id_caracteristica!=""){
		$desglose_caracteristicas_alumno = new Desglose_caracteristicas_alumno();
		return $desglose_caracteristicas_alumno->delete($id_ciclo,$year,$nocontrol,$id_caracteristica);
	} else{
    	return "Invalid";
    }
}



/* 	RECIBE POST ARRAY Y CONVIERTE A ARRAY*/

function helper_desglose_caracteristicas_alumno_data_array() {
	$user_data = array();

	if(array_key_exists('id_ciclo', $_POST)) {
		$user_data['id_ciclo'] = $_POST['id_ciclo'];
	}

	if(array_key_exists('id_caracteristica', $_POST)) {
		$user_data['id_caracteristica'] = $_POST['id_caracteristica'];
	}

	if(array_key_exists('nocontrol_alumno', $_POST)) {
		$user_data['nocontrol_alumno'] = $_POST['nocontrol_alumno'];
	}

	if(array_key_exists('descripcion', $_POST)) {
		$user_data['descripcion'] = $_POST['descripcion'];
	}

	if(array_key_exists('year', $_POST)) {
		$user_data['year'] = $_POST['year'];
	}

	if(array_key_exists('valor', $_POST)) {
		$user_data['valor'] = $_POST['valor'];
	}

	return $user_data;
}




?>
