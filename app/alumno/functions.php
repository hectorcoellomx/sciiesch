<?php

function alumno($id){
	$alumno = new Alumno($id);
	$datos = array("nocontrol"=>$alumno->getNocontrol(), "nombre"=>$alumno->getNombre(), "paterno"=>$alumno->getPaterno(), "materno"=>$alumno->getMaterno(), "edo"=>$alumno->getEdo(), "nacimiento"=>$alumno->getNacimiento(), "direccion"=>$alumno->getDireccion(), "municipio"=>$alumno->getMunicipio(), "ciudad"=>$alumno->getCiudad(), "curp"=>$alumno->getCurp(), "genero"=>$alumno->getGenero(), "email"=>$alumno->getEmail(), "telefono"=>$alumno->getTelefono(), "nacionalidad"=>$alumno->getNacionalidad(),"estado"=>$alumno->getEstado());
    return $datos;
}
function alumno_lista(){
	$alumno = new Alumno();
	$datos=$alumno->get();
    return $datos;
}
function alumno_lista_act(){
	$alumno = new Alumno();
	$datos=$alumno->get('1');
    return $datos;
}
function alumno_lista_des(){
	$alumno = new Alumno();
	$datos=$alumno->get('0');
    return $datos;
}
function alumno_lista_buscar($palabra,$lim,$join=""){
	$alumno = new Alumno();
	$datos=$alumno->get('1',$lim,$palabra,$join);
    return $datos;
}

function alumno_nuevo(){
	$user_data = helper_alumno_data_array();
	if(!empty($user_data)){
		$alumno = new Alumno();
    	return $alumno->set($user_data);
    } else{
    	return "Invalid";
    }
}

function alumno_update(){
	$user_data = helper_alumno_data_array();
	if(!empty($user_data)){
		$alumno = new Alumno();
	    return $alumno->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function alumno_activar(){
	if(isset($_POST['id'])){
		if(!empty($_POST['id'])){
			$alumno = new Alumno();
			return $alumno->activar($_POST['id']);
		} else{
	    	return "Invalid";
	    }
	} else{
    	return "Invalid";
    }
}

function alumno_desactivar(){
	if(isset($_POST['id'])){
		if(!empty($_POST['id'])){
			$alumno = new Alumno();
			return $alumno->desactivar($_POST['id']);
		} else{
	    	return "Invalid";
	    }
	} else{
    	return "Invalid";
    }
}

function alumno_delete(){
	$user_data = helper_alumno_data_array();
	if(!empty($user_data)){
		$alumno = new Alumno();
		return format_resp($alumno->delete($user_data['id']));
	} else{
    	return "Invalid";
    }
}


/* 	RECIBE POST ARRAY Y CONVIERTE A ARRAY*/

function helper_alumno_data_array() {
	$user_data = array();

	if(array_key_exists('nocontrol', $_POST)) {
		$user_data['nocontrol'] = $_POST['nocontrol'];
	}

	if(array_key_exists('nombre', $_POST)) {
		$user_data['nombre'] = $_POST['nombre'];
	}

	if(array_key_exists('paterno', $_POST)) {
		$user_data['paterno'] = $_POST['paterno'];
	}

	if(array_key_exists('materno', $_POST)) {
		$user_data['materno'] = $_POST['materno'];
	}

	if(array_key_exists('edo', $_POST)) {
		$user_data['edo'] = $_POST['edo'];
	}

	if(array_key_exists('curp', $_POST)) {
		$user_data['curp'] = $_POST['curp'];
	}

	if(array_key_exists('email', $_POST)) {
		$user_data['email'] = $_POST['email'];
	}

	if(array_key_exists('nacionalidad', $_POST)) {
		$user_data['nacionalidad'] = $_POST['nacionalidad'];
	}

	if(array_key_exists('genero', $_POST)) {
		$user_data['genero'] = $_POST['genero'];
	}

	if(array_key_exists('nacimiento', $_POST)) {
		$user_data['nacimiento'] = $_POST['nacimiento'];
	}

	if(array_key_exists('telefono', $_POST)) {
		$user_data['telefono'] = $_POST['telefono'];
	}

	if(array_key_exists('direccion', $_POST)) {
		$user_data['direccion'] = $_POST['direccion'];
	}

	if(array_key_exists('ciudad', $_POST)) {
		$user_data['ciudad'] = $_POST['ciudad'];
	}

	if(array_key_exists('id_estado', $_POST)) {
		$user_data['id_estado'] = $_POST['id_estado'];
	}

	if(array_key_exists('municipio', $_POST)) {
		$user_data['municipio'] = $_POST['municipio'];
	}

	return $user_data;
}

?>
