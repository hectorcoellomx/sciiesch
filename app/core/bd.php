<?php

abstract class bd {

    private static $db_host = "localhost";
    private static $db_user = "root";
    private static $db_pass = "";
    private static $db_name = "saiiesch";

    var $mysqli;
    protected $msj;
    protected $query;
    protected $rows = array();
    private $conn;



    function __construct() {

        $this->mysqli = new mysqli(self::$db_host, self::$db_user, self::$db_pass, self::$db_name);

        if ($this->mysqli->connect_error) {
            die('Error (' . $this->mysqli->connect_errno . ') ' . $this->mysqli->connect_error);
        }

        $this->mysqli->query("SET NAMES UTF8");
    }

    //function __destruct() {}

    function close(){
        mysqli_close($this->mysqli);
    }


    # métodos abstractos de clases que hereden
    abstract protected function get();
    abstract protected function set();
    abstract protected function edit();
    abstract protected function delete();

    # Conectar a la base de datos
    private function open_connection() {
        $this->conn = new mysqli(self::$db_host, self::$db_user, self::$db_pass, self::$db_name);
    }

    # Desconectar la base de datos
    private function close_connection() {
        $this->conn->close();
    }

    # Ejecutar un query simple del tipo INSERT, DELETE, UPDATE
    protected function execute_single_query() {
        $this->open_connection();
        try{

            if($rs=$this->conn->query($this->query)){
                $this->msj= "done";
            }
            else
                {
                    $this->msj= mysqli_errno($this->conn);
                }
        } catch (Exception $e) {
            $this->msj= mysqli_errno($this->conn);
        }
        $this->close_connection();
    }

    # Traer resultados de una consulta en un Array
    protected function get_results_from_query() {
        $this->open_connection();
        $result = $this->conn->query($this->query);
        while ($this->rows[] = $result->fetch_assoc());
        $result->close();
        $this->close_connection();
        array_pop($this->rows);
    }

 }

?>
