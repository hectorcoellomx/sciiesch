<?php

function carrera($id){
	$carrera = new Carrera($id);
	$datos = array("id"=>$carrera->getId(), "nombre"=>$carrera->getNombre(), "director"=>$carrera->getDirector(), "coordinador"=>$carrera->getCoordinador());
    return $datos;
}

function carrera_lista(){
	$carrera = new Carrera();
	$datos=$carrera->get();
    return $datos;
}

function carrera_nuevo(){
	$user_data = helper_carrera_data_array();
	if(!empty($user_data)){
		$carrera = new Carrera();
    	return $carrera->set($user_data);
    } else{
    	return "Invalido";
    }
}

function carrera_update(){
	$user_data = helper_carrera_data_array();
	if(!empty($user_data)){
		$carrera = new Carrera();
	    return $carrera->edit($user_data);
    } else{
    	return format_resp("Invalid");
    }
}

function carrera_delete(){
	$user_data = helper_carrera_data_array();
	if(!empty($user_data)){
		$carrera = new Carrera();
		return $carrera->delete($user_data['id']);
	} else{
    	return format_resp("Invalid");
    }
}


// RECIBE POST JSON Y CONVIERTE A ARRAY (For Angular)
function helper_carrera_data_json() {

	$user_data = array();
	$postdata = file_get_contents("php://input");
  $request = json_decode($postdata);
  

    if(isset($request->id)) {
		$user_data['id'] = $request->id;
	}
	if(isset($request->nombre)) {
		$user_data['nombre'] = $request->nombre;
	}

    return $user_data;
}

/* 	RECIBE POST ARRAY Y CONVIERTE A ARRAY*/

function helper_carrera_data_array() {
	$user_data = array();

	if(array_key_exists('id', $_POST)) {
		$user_data['id'] = $_POST['id'];
	}

	if(array_key_exists('nombre', $_POST)) {
		$user_data['nombre'] = $_POST['nombre'];
	}

	if(array_key_exists('director', $_POST)) {
		$user_data['director'] = $_POST['director'];
	}

	if(array_key_exists('coordinador', $_POST)) {
		$user_data['coordinador'] = $_POST['coordinador'];
	}
	return $user_data;
}




?>
