<?php

class Carrera extends bd
{
    private $id;
    private $nombre;
    private $director;
    private $coordinador;
  
    function __construct($id='') {
        if($id!=''){
            $this->query = "SELECT * FROM carrera WHERE id = '$id'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                foreach ($this->rows[0] as $propiedad=>$valor):
                $this->$propiedad = utf8_encode($valor);
                endforeach;
            }
        }
    }

    public function get($id=''){
        $this->query = "SELECT * FROM carrera";
        $this->get_results_from_query();

        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {  //$this->lista[] = $this->rows[$i];
                $cat = new carrera();
                foreach ($this->rows[$i] as $propiedad=>$valor):
                    $cat->$propiedad = utf8_encode($valor);
                endforeach;
                $datos = array("id"=>$cat->getId(), "nombre"=>$cat->getNombre(), "director"=>$cat->getDirector(), "coordinador"=>$cat->getCoordinador());
                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
        if(array_key_exists('nombre', $user_data)) {
                    foreach ($user_data as $campo=>$valor) {
                        $$campo = trim(utf8_decode($valor));
                    }
                    if($nombre!="" && $director!="" && $coordinador!=""){
                        $this->query = "INSERT INTO carrera(nombre, director, coordinador) VALUES ('$nombre', '$director', '$coordinador')";
                        $this->execute_single_query();
                        $mensaje = $this->msj;
                    } else {
                        $mensaje = 'Invalid';
                    }

            } else {
                $mensaje = 'Invalid';
            }

            return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id!="" && $nombre!="" && $director!="" && $coordinador!=""){
            $this->query = "UPDATE carrera SET nombre='$nombre', director='$director', coordinador='$coordinador' WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($user_id='') {
        if($user_id!=""){
            $this->query = "DELETE FROM carrera WHERE id = '$user_id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getId(){
        return $this->id;
    }

    function getNombre(){
        return $this->nombre;
    }

    function getDirector(){
        return $this->director;
    }

    function getCoordinador(){
        return $this->coordinador;
    }
    
    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
