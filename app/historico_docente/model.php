<?php

class Historico_docente extends bd
{
    private $docente;
	private $programa;

    function __construct($id='') {
        if($id!=''){
            $this->query = "SELECT * FROM historico_docente WHERE id = '$id'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                foreach ($this->rows[0] as $propiedad=>$valor):
                    $this->$propiedad = utf8_encode($valor);
                endforeach;
            }
        }
    }

    public function get($id_programa=''){
        if($id_programa==""){
            $this->query = "SELECT * FROM historico_docente";
        } else{
            $this->query = "SELECT * FROM historico_docente WHERE id_programa = '$id_programa'";        
        }

        $this->get_results_from_query();

        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {  
                $cat = new historico_docente();
                foreach ($this->rows[$i] as $propiedad=>$valor):
                    if($propiedad=="id_programa")
                        $cat->programa = programa($valor);
                    else if($propiedad=="nocontrol_docente")
                        $cat->docente = docente($valor);
                    else
                        $cat->$propiedad = utf8_encode($valor);
                endforeach;
                $datos = array("programa"=>$cat->getPrograma(),"docente"=>$cat->getDocente());
                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id_programa!="" && $nocontrol){
            $this->query = "INSERT INTO historico_docente(id_programa, nocontrol_docente) VALUES ($id_programa, '$nocontrol')";
            //die($this->query);
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else {
            $mensaje = 'Invalid';
        }

        return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id!="" && $nombre!=""){
            $this->query = "UPDATE municipio SET nombre='$nombre' WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($user_data=array()) {
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id_programa!="" && $nocontrol){
            $this->query = "DELETE FROM historico_docente WHERE id_programa = $id_programa AND nocontrol_docente = '$nocontrol'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

	function getDocente(){
        return $this->docente;
    }
	function getPrograma(){
        return $this->programa;
    }


    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
