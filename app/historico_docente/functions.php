<?php

function historico_docente($id,$idi){
	$historico_docente = new Historico_docente($id,$idi);
	$datos = array("cantidad"=>$historico_docente->getCantidad(), "infraestructura"=>$historico_docente->getInfraestructura(), "programa"=>$historico_docente->getPrograma());
    return $datos;
}

function historico_docente_lista(){
	$historico_docente = new Historico_docente();
	$datos=$historico_docente->get();
    return $datos;
}

function historico_docente_lista_programa($id_programa){
	$historico_docente = new Historico_docente();
	$datos=$historico_docente->get($id_programa);
    return $datos;
}

function historico_docente_nuevo($id,$nocontrol){
	$userdata = array();
	$userdata['id_programa'] = $id;
	$userdata['nocontrol'] = $nocontrol;
	if(!empty($userdata)){
		$historico_docente = new Historico_docente();
    	return $historico_docente->set($userdata);
    } else{
    	return "Invalido";
    }
}

function historico_docente_update(){
	$user_data = helper_historico_docente_data_array();
	if(!empty($user_data)){
		$historico_docente = new Historico_docente();
	    return $historico_docente->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function historico_docente_delete($id_programa,$nocontrol){
	$userdata = array();
	$userdata['id_programa'] = $id_programa;
	$userdata['nocontrol'] = $nocontrol;
	if(!empty($userdata)){
		$historico_docente = new Historico_docente();
		return $historico_docente->delete($userdata);
	} else{
    	return "Invalid";
    }
}



/* 	RECIBE POST ARRAY Y CONVIERTE A ARRAY*/

function helper_historico_docente_data_array() {
	$user_data = array();

	if(array_key_exists('cantidad', $_POST)) {
		$user_data['cantidad'] = $_POST['cantidad'];
	}

	if(array_key_exists('id_programa', $_POST)) {
		$user_data['id_programa'] = $_POST['id_programa'];
	}

	if(array_key_exists('id_infraestructura', $_POST)) {
		$user_data['id_infraestructura'] = $_POST['id_infraestructura'];
	}

	return $user_data;
}




?>
