<?php 

include ('../core/bd.php');
include ('../core/general.php');
include ('model.php');
include('functions.php');

if($_GET['detalle']=="list"){

	$respuesta = categoria_lista();

}else if($_GET['detalle']=="new"){
	
	$respuesta= categoria_nuevo();

} else if($_GET['detalle']=="delete"){
	
	$respuesta = categoria_delete();

}else if($_GET['detalle']=="update"){
	
	$respuesta = categoria_update();

}else if($_GET['detalle']!=""){

	$respuesta = categoria($_GET['detalle']);

}else{

	header('HTTP/1.1 405 Method Not Allowed');
	exit;

}

header('Content-Type: application/json; charset=utf-8');
echo json_encode($respuesta);

?>