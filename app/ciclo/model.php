<?php

class Ciclo extends bd
{
    private $id;
    private $descripcion;
    private $mes_inicio;
    private $mes_final;
    private $plan;

    function __construct($id='') {
        if($id!=''){
            $this->query = "SELECT * FROM ciclo WHERE id = '$id'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                foreach ($this->rows[0] as $propiedad=>$valor):
                    if($propiedad!="id_plan")
                        $this->$propiedad = utf8_encode($valor);
                    else
                        $this->plan = plan($valor);
                endforeach;
            }
        }
    }

    public function get($id=''){
        $this->query = "SELECT * FROM ciclo";
        $this->get_results_from_query();

        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {  //$this->lista[] = $this->rows[$i];
                $cat = new Ciclo();
                foreach ($this->rows[$i] as $propiedad=>$valor):
                    if($propiedad!="id_plan")
                        $cat->$propiedad = utf8_encode($valor);
                    else
                        $cat->plan = plan($valor);
                endforeach;
                $datos = array("id"=>$cat->getId(), "descripcion"=>$cat->getDescripcion(), "mes_inicio"=>$cat->getMes_inicio(), "mes_final"=>$cat->getMes_final(), "plan"=>$cat->getPlan());
                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
        if(array_key_exists('descripcion', $user_data) && array_key_exists('id_plan', $user_data)) {
                    foreach ($user_data as $campo=>$valor) {
                        $$campo = trim(utf8_decode($valor));
                    }
                    if($descripcion!="" && $id_plan!=""){
                        $this->query = "INSERT INTO ciclo(descripcion,id_plan) VALUES ('$descripcion',$id_plan)";
                        $this->execute_single_query();
                        $mensaje = $this->msj;
                    } else {
                        $mensaje = 'Invalid';
                    }

            } else {
                $mensaje = 'Invalid';
            }

            return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id!="" && $descripcion!="" && $id_plan!=""){
            $this->query = "UPDATE ciclo SET descripcion='$descripcion', id_plan='$id_plan' WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($user_id='') {
        if($user_id!=""){
            $this->query = "DELETE FROM ciclo WHERE id = '$user_id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getId(){
        return $this->id;
    }

    function getDescripcion(){
        return $this->descripcion;
    }

    function getMes_inicio(){
        return $this->mes_inicio;
    }

    function getMes_final(){
        return $this->mes_final;
    }

    function getPlan(){
        return $this->plan;
    }

    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
