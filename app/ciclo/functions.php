<?php

function ciclo($id){
	$ciclo = new Ciclo($id);
	$datos = array("id"=>$ciclo->getId(), "descripcion"=>$ciclo->getDescripcion(), "mes_inicio"=>$ciclo->getMes_inicio(), "mes_final"=>$ciclo->getMes_final(), "plan"=>$ciclo->getPlan());
    return $datos;
}

function ciclo_lista(){
	$ciclo = new Ciclo();
	$datos=$ciclo->get();
    return $datos;
}

function ciclo_nuevo(){
	$user_data = helper_ciclo_data_array();
	if(!empty($user_data)){
		$ciclo = new Ciclo();
    	return $ciclo->set($user_data);
    } else{
    	return "Invalid";
    }
}

function ciclo_update(){
	$user_data = helper_ciclo_data_array();
	if(!empty($user_data)){
		$ciclo = new Ciclo();
	    return $ciclo->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function ciclo_delete(){
	$user_data = helper_ciclo_data_array();
	if(!empty($user_data)){
		$ciclo = new Ciclo();
		return $ciclo->delete($user_data['id']);
	} else{
    	return "Invalid";
    }
}

// 	RECIBE POST ARRAY Y CONVIERTE A ARRAY

function helper_ciclo_data_array() {
	$user_data = array();

	if(array_key_exists('id', $_POST)) {
		$user_data['id'] = $_POST['id'];
	}

	if(array_key_exists('descripcion', $_POST)) {
		$user_data['descripcion'] = $_POST['descripcion'];
	}

	if(array_key_exists('id_plan', $_POST)) {
		$user_data['id_plan'] = $_POST['id_plan'];
	}

	return $user_data;
}




?>
