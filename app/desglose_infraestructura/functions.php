<?php

function des_infraestructura($id,$idi){
	$des_infraestructura = new Desglose_infraestructura($id,$idi);
	$datos = array("cantidad"=>$des_infraestructura->getCantidad(), "infraestructura"=>$des_infraestructura->getInfraestructura(), "programa"=>$des_infraestructura->getPrograma());
    return $datos;
}

function des_infraestructura_lista(){
	$des_infraestructura = new Desglose_infraestructura();
	$datos=$des_infraestructura->get();
    return $datos;
}

function des_infraestructura_lista_programa($id_programa){
	$des_infraestructura = new Desglose_infraestructura();
	$datos=$des_infraestructura->get($id_programa);
    return $datos;
}

function des_infraestructura_nuevo(){
	$user_data = helper_des_infraestructura_data_array();
	if(!empty($user_data)){
		$des_infraestructura = new Desglose_infraestructura();
    	return $des_infraestructura->set($user_data);
    } else{
    	return "Invalido";
    }
}

function des_infraestructura_update(){
	$user_data = helper_des_infraestructura_data_array();
	if(!empty($user_data)){
		$des_infraestructura = new Desglose_infraestructura();
	    return $des_infraestructura->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function des_infraestructura_delete($id_programa,$id_infraestructura){
	if($id_programa!="" && $id_infraestructura!=""){
		$des_infraestructura = new Desglose_infraestructura();
		return $des_infraestructura->delete($id_programa,$id_infraestructura);
	} else{
    	return "Invalid";
    }
}



/* 	RECIBE POST ARRAY Y CONVIERTE A ARRAY*/

function helper_des_infraestructura_data_array() {
	$user_data = array();

	if(array_key_exists('cantidad', $_POST)) {
		$user_data['cantidad'] = $_POST['cantidad'];
	}

	if(array_key_exists('id_programa', $_POST)) {
		$user_data['id_programa'] = $_POST['id_programa'];
	}

	if(array_key_exists('id_infraestructura', $_POST)) {
		$user_data['id_infraestructura'] = $_POST['id_infraestructura'];
	}

	return $user_data;
}




?>
