<?php

class Desglose_infraestructura extends bd
{
    private $cantidad;
    private $infraestructura;
    private $programa;

    function __construct($id='',$idi='') {
        if($id!='' && $idi!=''){
            $this->query = "SELECT * FROM infraestructura_programa WHERE id_programa = '$id' AND id_infraestructura = '$idi'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                foreach ($this->rows[0] as $propiedad=>$valor):
                    if($propiedad=="id_infraestructura"){
                        $this->infraestructura = infraestructura($valor);
                    }elseif ($propiedad=="id_programa") {
                        $this->programa = programa($valor);
                    }
                    $this->$propiedad = utf8_encode($valor);
                endforeach;
            }
        }
    }

    public function get($id_programa=''){
        
        if($id_programa=="" )
            $this->query = "SELECT * FROM infraestructura_programa";
        else
            $this->query = "SELECT * FROM infraestructura_programa WHERE id_programa='" . $id_programa ."'";

        $this->get_results_from_query();

        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) { 
                $cat = new Desglose_infraestructura();
                foreach ($this->rows[$i] as $propiedad=>$valor):
                    if($propiedad=="id_infraestructura"){
                        $cat->infraestructura = infraestructura($valor);
                    }elseif ($propiedad=="id_programa") {
                        $cat->programa = programa($valor);
                    }
                    $cat->$propiedad = utf8_encode($valor);
                endforeach;

                $datos = array("cantidad"=>$cat->getCantidad(), "infraestructura"=>$cat->getInfraestructura(), "programa"=>$cat->getPrograma());
                
                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
        if(array_key_exists('cantidad', $user_data) && array_key_exists('id_programa', $user_data) && array_key_exists('id_infraestructura', $user_data)) {
                    foreach ($user_data as $campo=>$valor) {
                        $$campo = trim(utf8_decode($valor));
                    }
                    if($cantidad!="" && $id_programa!="" && $id_infraestructura!=""){
                        $this->query = "INSERT INTO infraestructura_programa(cantidad, id_programa, id_infraestructura) VALUES ($cantidad,$id_programa,$id_infraestructura)";
                        $this->execute_single_query();
                        $mensaje = "". $this->msj;    
                    } else {
                        $mensaje = 'Invalid';
                    }
                    
            } else {
                $mensaje = 'Invalid';
            }
            return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($cantidad!="" && $id_programa!="" && $id_infraestructura!=""){
            $this->query = "UPDATE infraestructura_programa SET cantidad='$cantidad' WHERE id_programa = '$id_programa' AND id_infraestructura='$id_infraestructura'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($id_programa='',$id_infraestructura='') {
        if($id_programa!="" && $id_infraestructura!=""){
            $this->query = "DELETE FROM infraestructura_programa WHERE id_programa = '$id_programa' AND id_infraestructura='$id_infraestructura'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }


    function getCantidad(){
        return $this->cantidad;
    }

    function getInfraestructura(){
        return $this->infraestructura;
    }

    function getPrograma(){
        return $this->programa;
    }

    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
