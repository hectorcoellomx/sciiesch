<?php

class Desglose_caracteristicas_docente extends bd
{
    private $docente;
    private $caracteristicas;
    private $descripcion;
    private $valor;
    private $fecha;

    function __construct($id_caracteristica='', $nocontrol='', $fecha='', $accion='') {
        if($fecha!="" && $nocontrol!="" && $id_caracteristica!='' && $accion!=''){

            if($accion=="agregar")
              $this->query = "SELECT * FROM des_docente_caracteristica WHERE fecha='" . $fecha . "' AND nocontrol_docente='" . $nocontrol . "' AND id_caracteristica='" . $id_caracteristica . "'";
            else
              $this->query = "SELECT * FROM des_docente_caracteristica WHERE fecha<='" . $fecha . "' AND nocontrol_docente='" . $nocontrol . "' AND id_caracteristica='" . $id_caracteristica . "' ORDER BY fecha DESC";

            $this->get_results_from_query();
            if(count($this->rows) >= 1){
              //echo "a-";
                foreach ($this->rows[0] as $propiedad=>$valor):
                    if ($propiedad=="nocontrol_docente")
                        $this->docente = docente($valor);
                    elseif ($propiedad=="id_caracteristica")
                        $this->caracteristicas = caracteristicas($valor);
                    else
                        $this->$propiedad = utf8_encode($valor);
                endforeach;
            }/*else{
              echo "b";
            }*/
        }
    }

    public function get($nocontrol='', $fecha='', $accion=''){
        if($fecha!="" && $nocontrol!="" && $accion=="check")
            $this->query = "SELECT DISTINCT id_caracteristica FROM des_docente_caracteristica WHERE fecha<='" . $fecha . "' AND nocontrol_docente='" . $nocontrol . "' ORDER BY fecha DESC";

        if($fecha!="" && $nocontrol!="" && $accion=="full"){

            $sql_mayores= "SELECT id_caracteristica as id_c, nocontrol_docente as nocontrol_d,  max(fecha) as f FROM des_docente_caracteristica WHERE fecha<='" . $fecha . "' AND nocontrol_docente='" . $nocontrol . "' GROUP BY id_caracteristica";
            $this->query = "SELECT id_caracteristica, valor, nocontrol_docente, descripcion, fecha FROM des_docente_caracteristica INNER JOIN (" . $sql_mayores . ") t2 ON (des_docente_caracteristica.id_caracteristica=t2.id_c AND des_docente_caracteristica.fecha=t2.f AND des_docente_caracteristica.nocontrol_docente=t2.nocontrol_d)";

            //die($this->query);
        }

        $this->get_results_from_query();
        $lista = array();

        if (count($this->rows) >= 1) {
            if($accion=="check")
              return count($this->rows);
            else{
              for ($i=0 ; $i < count($this->rows); $i++ ) {
                  $cat = new Desglose_caracteristicas_docente();
                  foreach ($this->rows[$i] as $propiedad=>$valor):
                      if ($propiedad=="nocontrol_docente")
                          $cat->docente = docente($valor);
                      elseif ($propiedad=="id_caracteristica")
                          $cat->caracteristicas = caracteristicas($valor);
                      else
                          $cat->$propiedad = utf8_encode($valor);
                  endforeach;

                  $datos = array("docente"=>$cat->getDocente(),"descripcion"=>$cat->getDescripcion(), "valor"=>$cat->getValor(),"fecha"=>$cat->getFecha(),"caracteristicas"=>$cat->getCaracteristicas());
                  $lista[] = $datos;
              }
          }

        }else{
          if($accion=="check")
            return count($this->rows);
        }

        return $lista;

    }

    public function set($fecha='',$nocontrol_docente='', $id_caracteristica='', $descripcion='', $valor=''){
        /*foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }*/
        if($id_caracteristica!="" && $nocontrol_docente!="" && $fecha!=""){
            $this->query = "INSERT INTO des_docente_caracteristica(id_caracteristica, nocontrol_docente, descripcion, fecha,valor) VALUES ($id_caracteristica,'$nocontrol_docente','$descripcion', '$fecha' , $valor)";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else {
            $mensaje = 'Invalid';
        }

        return $mensaje;
    }

    public function edit($fecha='',$nocontrol_docente='', $id_caracteristica='', $descripcion='', $valor=''){
        /*foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }*/
        if($id_caracteristica!="" && $nocontrol_docente!="" && $fecha!=""){
            $this->query = "UPDATE des_docente_caracteristica SET valor=$valor, descripcion='$descripcion' WHERE id_caracteristica=" . $id_caracteristica . " AND fecha='" . $fecha . "' AND nocontrol_docente='" . $nocontrol_docente . "'";
            //die($this->query);
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($fecha='',$nocontrol='', $id_caracteristica='') {
        if($fecha!="" && $nocontrol!="" && $id_caracteristica!=""){
            $this->query = "DELETE FROM des_docente_caracteristica WHERE fecha='" . $fecha . "' AND nocontrol_docente='" . $nocontrol . "'". " AND id_caracteristica='" . $id_caracteristica . "'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getDocente(){
        return $this->docente;
    }

    function getCaracteristicas(){
        return $this->caracteristicas;
    }

    function getDescripcion(){
        return $this->descripcion;
    }

    function getFecha(){
        return $this->fecha;
    }

    function getValor(){
        return $this->valor;
    }

    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
