<?php

function desglose_caracteristicas_docente($id_caracteristica,$nocontrol,$fecha,$accion){
	$desglose_caracteristicas_docente = new Desglose_caracteristicas_docente($id_caracteristica,$nocontrol,$fecha,$accion);
	$datos = array("fecha"=>$desglose_caracteristicas_docente->getFecha(),"docente"=>$desglose_caracteristicas_docente->getDocente(), "descripcion"=>$desglose_caracteristicas_docente->getDescripcion(), "valor"=>$desglose_caracteristicas_docente->getValor(), "caracteristicas"=>$desglose_caracteristicas_docente->getCaracteristicas());
	return $datos;
}

function desglose_caracteristicas_docente_lista(){
	$desglose_caracteristicas_docente = new Desglose_caracteristicas_docente();
	$datos=$desglose_caracteristicas_docente->get();
    return $datos;
}
function desglose_caracteristicas_check($nocontrol,$fecha){
	$desglose_caracteristicas_docente = new Desglose_caracteristicas_docente();
	$datos=$desglose_caracteristicas_docente->get($nocontrol,$fecha,"check");
  	return $datos;
}
function desglose_caracteristicas_full($nocontrol,$fecha){
	$desglose_caracteristicas_docente = new Desglose_caracteristicas_docente();
	$datos=$desglose_caracteristicas_docente->get($nocontrol,$fecha,"full");
  	return $datos;
}

function desglose_caracteristicas_docente_lista_programa($id_programa){
	$desglose_caracteristicas_docente = new Desglose_caracteristicas_docente();
	$datos=$desglose_caracteristicas_docente->get($id_programa);
    return $datos;
}

function desglose_caracteristicas_docente_nuevo($fecha,$nocontrol,$id_caracteristica,$descripcion,$valor){
	//$user_data = helper_desglose_caracteristicas_docente_data_array();
	if(1==1){
		$desglose_caracteristicas_docente = new Desglose_caracteristicas_docente();
    	return $desglose_caracteristicas_docente->set($fecha,$nocontrol,$id_caracteristica,$descripcion,$valor);
    } else{
    	return "Invalido";
    }
}

function desglose_caracteristicas_docente_update($fecha,$nocontrol,$id_caracteristica,$descripcion,$valor){
	//$user_data = helper_desglose_caracteristicas_docente_data_array();
	if(1==1){
		$desglose_caracteristicas_docente = new Desglose_caracteristicas_docente();
	    return $desglose_caracteristicas_docente->edit($fecha,$nocontrol,$id_caracteristica,$descripcion,$valor);
    } else{
    	return "Invalid";
    }
}

function desglose_caracteristicas_docente_delete($id_ciclo,$year,$nocontrol,$id_caracteristica){
	if($id_ciclo!="" && $year!="" && $nocontrol!="" && $id_caracteristica!=""){
		$desglose_caracteristicas_docente = new Desglose_caracteristicas_docente();
		return $desglose_caracteristicas_docente->delete($id_ciclo,$year,$nocontrol,$id_caracteristica);
	} else{
    	return "Invalid";
    }
}



/* 	RECIBE POST ARRAY Y CONVIERTE A ARRAY*/

function helper_desglose_caracteristicas_docente_data_array() {
	$user_data = array();

	if(array_key_exists('id_ciclo', $_POST)) {
		$user_data['id_ciclo'] = $_POST['id_ciclo'];
	}

	if(array_key_exists('id_caracteristica', $_POST)) {
		$user_data['id_caracteristica'] = $_POST['id_caracteristica'];
	}

	if(array_key_exists('nocontrol_docente', $_POST)) {
		$user_data['nocontrol_docente'] = $_POST['nocontrol_docente'];
	}

	if(array_key_exists('descripcion', $_POST)) {
		$user_data['descripcion'] = $_POST['descripcion'];
	}

	if(array_key_exists('year', $_POST)) {
		$user_data['year'] = $_POST['year'];
	}

	if(array_key_exists('valor', $_POST)) {
		$user_data['valor'] = $_POST['valor'];
	}

	return $user_data;
}




?>
