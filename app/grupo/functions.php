<?php

function grupo($id){
	$grupo = new Grupo($id);
	$datos = array("codigo"=>$grupo->getCodigo(), "nombre"=>$grupo->getNombre(), "grado"=>$grupo->getGrado());
    return $datos;
}

function grupo_lista(){
	$grupo = new Grupo();
	$datos=$grupo->get();
    return $datos;
}

function grupo_nuevo(){
	$user_data = helper_grupo_data_array();
	if(!empty($user_data)){
		$grupo = new Grupo();
    	return $grupo->set($user_data);
    } else{
    	return "Invalido";
    }
}

function grupo_update(){
	$user_data = helper_grupo_data_array();
	if(!empty($user_data)){
		$grupo = new Grupo();
	    return $grupo->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function grupo_delete(){
	$user_data = helper_grupo_data_array();
	if(!empty($user_data)){
		$grupo = new Grupo();
		return $grupo->delete($user_data['id']);
	} else{
    	return "Invalid";
    }
}

// 	RECIBE POST ARRAY Y CONVIERTE A ARRAY

function helper_grupo_data_array() {
	$user_data = array();

	if(array_key_exists('id', $_POST)) {
		$user_data['id'] = $_POST['id'];
	}

	if(array_key_exists('modalidad', $_POST)) {
		$user_data['modalidad'] = $_POST['modalidad'];
	}
	return $user_data;
}


?>
