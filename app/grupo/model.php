<?php
class Grupo extends bd
{
    private $codigo;
    private $nombre;
  	private $grado;

    function __construct($codigo='') {
        if($codigo!=''){
            $this->query = "SELECT * FROM grupo WHERE codigo = '$codigo'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                $this->codigo = $this->rows[0]['codigo'];
                $this->nombre = utf8_encode($this->rows[0]['nombre']);
                $this->grado = $this->rows[0]['grado'];
            }
        }
    }

    public function get($id=''){
        $this->query = "SELECT * FROM grupo";
        $this->get_results_from_query();

        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {  //$this->lista[] = $this->rows[$i];
                $cat = new Grupo();
                $cat->codigo = $this->rows[$i]['codigo'];
                $cat->nombre = utf8_encode($this->rows[$i]['nombre']);
                $cat->grado = $this->rows[$i]['grado'];

                $datos = array("codigo"=>$cat->getCodigo(), "nombre"=>$cat->getNombre(), "grado"=>$cat->getGrado());
                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
        if(array_key_exists('nombre', $user_data)) {
                    foreach ($user_data as $campo=>$valor) {
                        $$campo = trim(utf8_decode($valor));
                    }
                    if($nombre!=""){
                        $this->query = "INSERT INTO municipio(nombre) VALUES ('$nombre')";
                        $this->execute_single_query();
                        $mensaje = $this->msj;
                    } else {
                        $mensaje = 'Invalid';
                    }

            } else {
                $mensaje = 'Invalid';
            }

            return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id!="" && $nombre!=""){
            $this->query = "UPDATE municipio SET nombre='$nombre' WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($user_id='') {
        if($user_id!=""){
            $this->query = "DELETE FROM municipio WHERE id = '$user_id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getCodigo(){
        return $this->codigo;
    }

    function getNombre(){
        return $this->nombre;
    }
	 function getGrado(){
        return $this->grado;
    }


    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
