<?php
session_start();
$id_sesion = session_id();
$path="../../";
$mod = array("carrera","plan","ciclo","estado","municipio","persona","docente","caracteristicas","desglose_caracteristicas_docente");
include ('../modulos.php');
include ('../sesion.php');


if(isset($_POST['id_ciclo']) && isset($_POST['year']) && isset($_POST['nocontrol_docente']) && isset($_POST['id_caracteristica']) && isset($_POST['accion'])){



  if($_POST['year']==date("Y") || $user['tipo']==0){
      if($_POST['id_ciclo']!=0){ // No es usuario nuevo
        if($user['tipo']==0){ // Si es administrador
            $cabecera= ciclo($_POST['id_ciclo']); // Toma mes del ciclo
            $mes= $cabecera['mes_inicio'];
        }else{
            $mes= date("n");
        }
      }else{ // Usuario nuevo
        $mes= 1; // Toma enero
        $_POST['year']=date("Y"); //Año actual
      }

      $lista=ciclo_lista();

      if($_POST['accion']=="agregar"){

          foreach ($lista as $l){
            if($l['mes_inicio']>=$mes || $l['mes_final']>=$mes){ // Meses en que entra y superiores
              $res=desglose_caracteristicas_docente_nuevo($l['id'],$_POST['year'],$_POST['nocontrol_docente'],$_POST['id_caracteristica'],$_POST['descripcion'],$_POST['valor']);
            }
          }

          if($res=="done")
            echo "Actualizar";

          if($res=="1062")
            echo "Reintentar";

      }
      else{
          $cad="";
          foreach ($lista as $l){
            $entrar=false;
            if($_POST['year']==date("Y")){
              if($l['mes_inicio']>=$mes || $l['mes_final']>=$mes){ // Meses en que entra y superiores
                  $entra=true;
              }
            }else{
              if($l['mes_inicio']<=$mes || $l['mes_final']>=$mes){ // Meses en que entra
                  $entra=true;
              }
            }
            if($entra){
              $res=desglose_caracteristicas_docente_update($l['id'],$_POST['year'],$_POST['nocontrol_docente'],$_POST['id_caracteristica'],$_POST['descripcion'],$_POST['valor']);
            }
          }

          if($res=="done")
            echo "Actualizar";
      }
  }else{
    echo "Solo para año actual";
  }



}else{

  echo "Reintentar";

}

?>
