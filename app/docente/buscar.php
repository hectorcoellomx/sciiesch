<?php 
include ('../core/bd.php');
include ('../carrera/model.php');
include('../carrera/functions.php');
include ('../plan/model.php');
include('../plan/functions.php');
include ('../ciclo/model.php');
include('../ciclo/functions.php');
include ('../estado/model.php');
include('../estado/functions.php');
include ('../municipio/model.php');
include('../municipio/functions.php');
include ('../persona/model.php');
include('../persona/functions.php');
include ('../programa/model.php');
include('../programa/functions.php');
include ('./model.php');
include('./functions.php');
include ('../historico_docente/model.php');
include('../historico_docente/functions.php');

//die($_POST['palabra'] . "a " . $_POST['id_programa']);
if(isset($_POST['palabra']) && isset($_POST['id_programa'])){

$lista=docente_lista_buscar(trim($_POST['palabra']),30);

$lista_des= historico_docente_lista_programa($_POST['id_programa']);
?>
<table class="table table-bordered">
  <thead>
    <tr>
     <th class="col-md-2">ID</th>
     <th>Nombre</th>
     <th class="col-md-1 acciones">Agregar</th>
    </tr>
  </thead>
  <tbody>
    <?php 

    if(is_array($lista)){

      foreach ($lista as $l) {

      	$quitar=false;
        if(is_array($lista_des)){
          foreach ($lista_des as $lis) {
            if($lis['docente']['nocontrol']==$l['nocontrol'])
              $quitar=true;
          }
        }

        if(!$quitar){
	        echo "<tr>";
	        echo "<td>" . $l['nocontrol'] . "</td><td>" . $l['nombre'] . " " . $l['paterno'] . " " . $l['materno'] . "</td>";
	        echo "<td class='acciones'>";
	        echo "<a href='?id=" . $_POST['id_programa'] . "&nocontrol=" . $l['nocontrol'] . "&type=agregar'><i class='fa fa-plus' aria-hidden='true'></i></a>";
	        echo "</td>";  
	        echo "</tr>";  
        }

      }

    } 

    ?>
  </tbody>
</table>

<?php } ?>