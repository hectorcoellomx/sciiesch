<?php

class Docente extends Persona
{
    private $ingreso;


    function __construct($nocontrol='') {
        if($nocontrol!=''){
            $this->query = "SELECT * FROM docente WHERE nocontrol = '$nocontrol'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                $i=0;
                $this->nocontrol = utf8_encode($this->rows[$i]['nocontrol']);
                $this->nombre = utf8_encode($this->rows[$i]['nombre']);
                $this->paterno = utf8_encode($this->rows[$i]['paterno']);
                $this->materno = utf8_encode($this->rows[$i]['materno']);
                $this->edo = $this->rows[$i]['edo'];
                $this->curp = $this->rows[$i]['curp'];
                $this->email = $this->rows[$i]['email'];
                $this->nacionalidad = $this->rows[$i]['nacionalidad'];
                $this->genero = $this->rows[$i]['genero'];
                $this->nacimiento = $this->rows[$i]['nacimiento'];
                $this->telefono = $this->rows[$i]['telefono'];
                $this->ingreso = $this->rows[$i]['ingreso'];
                $this->direccion = utf8_encode($this->rows[$i]['direccion']);
                $this->municipio = utf8_encode($this->rows[$i]['municipio']);
                $this->ciudad = utf8_encode($this->rows[$i]['ciudad']);
                $this->estado = estado($this->rows[$i]['id_estado']);

            }
        }
    }

    public function get($edo='',$limite='',$palabra=''){
        if($limite!="")
            $limite= " LIMIT " . $limite;

        if($palabra!="")
            $palabra = " AND (nombre LIKE '%" . $palabra . "%' OR paterno LIKE '%" . $palabra . "%' OR materno LIKE '%" . $palabra . "%')";

        if($edo==""){
            $this->query = "SELECT * FROM docente ORDER BY `docente`.`edo` DESC" . $palabra;
        }else{
            $this->query = "SELECT * FROM docente WHERE edo='" . $edo . "' " . $palabra . "ORDER BY `docente`.`edo` DESC " . $limite;
        }

        //if($limite!="")
          //  die($this->query);
        $this->get_results_from_query();

        if (count($this->rows) >= 1) {


            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {
                $cat = new Docente();

                $cat->nocontrol = utf8_encode($this->rows[$i]['nocontrol']);
                $cat->nombre = utf8_encode($this->rows[$i]['nombre']);
                $cat->paterno = utf8_encode($this->rows[$i]['paterno']);
                $cat->materno = utf8_encode($this->rows[$i]['materno']);
                $cat->edo = $this->rows[$i]['edo'];
                $cat->curp = $this->rows[$i]['curp'];
                $cat->email = $this->rows[$i]['email'];
                $cat->nacionalidad = $this->rows[$i]['nacionalidad'];
                $cat->genero = $this->rows[$i]['genero'];
                $cat->nacimiento = $this->rows[$i]['nacimiento'];
                $cat->telefono = $this->rows[$i]['telefono'];
                $cat->ingreso = $this->rows[$i]['ingreso'];
                $cat->direccion = utf8_encode($this->rows[$i]['direccion']);
                $cat->municipio = utf8_encode($this->rows[$i]['municipio']);
                $cat->ciudad = utf8_encode($this->rows[$i]['ciudad']);
                $cat->estado = estado($this->rows[$i]['id_estado']);


                $datos = array("nocontrol"=>$cat->getNocontrol(), "nombre"=>$cat->getNombre(), "paterno"=>$cat->getPaterno(), "materno"=>$cat->getMaterno(), "edo"=>$cat->getEdo(), "nacimiento"=>$cat->getNacimiento(), "ingreso"=>$cat->getIngreso(), "direccion"=>$cat->getDireccion(), "municipio"=>$cat->getMunicipio(), "ciudad"=>$cat->getCiudad(), "curp"=>$cat->getCurp(), "genero"=>$cat->getGenero(), "email"=>$cat->getEmail(), "telefono"=>$cat->getTelefono(), "nacionalidad"=>$cat->getNacionalidad(),"estado"=>$cat->getEstado());
                $lista[] = $datos;
            }
            return $lista;
        }

    }

    public function set($user_data=array()){
            foreach ($user_data as $campo=>$valor) {
                $$campo = trim(utf8_decode($valor));
            }
            if($nocontrol!="" && $nombre!="" && $paterno!="" && $materno!="" && $nacimiento!="" && $direccion!="" && $municipio!="" && $ciudad!="" && $curp!="" && $genero!="" && $email!="" && $telefono!="" && $nacionalidad!=""  && $ingreso!="" && $edo!="" && $id_estado!=""){
                $this->query = "INSERT INTO docente(nocontrol, nombre, paterno, materno, nacimiento, direccion, municipio, ciudad, curp, genero, email, telefono, nacionalidad, ingreso, edo, id_estado) VALUES ('$nocontrol','$nombre','$paterno','$materno','$nacimiento','$direccion','$municipio','$ciudad', '$curp', $genero, '$email', '$telefono', '$nacionalidad', '$ingreso', $edo,$id_estado)";
                $this->execute_single_query();
                $mensaje = $this->msj;
            } else {
                $mensaje = 'Invalid';
            }
            return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($nocontrol!="" && $nombre!="" && $paterno!="" && $materno!="" && $nacimiento!="" && $direccion!="" && $municipio!="" && $ciudad!="" && $curp!="" && $genero!="" && $email!="" && $telefono!="" && $nacionalidad!=""  && $ingreso!="" && $id_estado!=""){
            $this->query = "UPDATE docente SET nombre='$nombre', paterno='$paterno', materno='$materno', nacimiento='$nacimiento', direccion='$direccion', municipio='$municipio', ciudad='$ciudad', curp='$curp', genero=$genero, email='$email', telefono='$telefono', nacionalidad='$nacionalidad', ingreso='$ingreso', id_estado=$id_estado, edo=$edo WHERE nocontrol = '$nocontrol'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($user_id='') {
        if($user_id!=""){
            $this->query = "DELETE FROM docente WHERE nocontrol = '$user_id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    public function activar($user_id=''){
        if($user_id!=""){
            $this->query = "UPDATE docente SET edo=1 WHERE nocontrol = '$user_id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;
    }

    public function desactivar($user_id=''){
        if($user_id!=""){
            $this->query = "UPDATE docente SET edo=0 WHERE nocontrol = '$user_id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;
    }


    function getIngreso(){
        return $this->ingreso;
    }


    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
