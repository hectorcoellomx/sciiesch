<?php

function docente($id){
	$docente = new Docente($id);
	$datos = array("nocontrol"=>$docente->getNocontrol(), "nombre"=>$docente->getNombre(), "paterno"=>$docente->getPaterno(), "materno"=>$docente->getMaterno(), "edo"=>$docente->getEdo(), "nacimiento"=>$docente->getNacimiento(), "ingreso"=>$docente->getIngreso(), "direccion"=>$docente->getDireccion(), "municipio"=>$docente->getMunicipio(), "ciudad"=>$docente->getCiudad(), "curp"=>$docente->getCurp(), "genero"=>$docente->getGenero(), "email"=>$docente->getEmail(), "telefono"=>$docente->getTelefono(), "nacionalidad"=>$docente->getNacionalidad(),"estado"=>$docente->getEstado());
    return $datos;
}
function docente_lista(){
	$docente = new Docente();
	$datos=$docente->get();
    return $datos;
}
function docente_lista_act(){
	$docente = new Docente();
	$datos=$docente->get('1');
    return $datos;
}
function docente_lista_des(){
	$docente = new Docente();
	$datos=$docente->get('0');
    return $datos;
}
function docente_lista_buscar($palabra,$lim){
	$docente = new Docente();
	$datos=$docente->get('1',$lim,$palabra);
    return $datos;
}

function docente_nuevo(){
	$user_data = helper_docente_data_array();
	if(!empty($user_data)){
		$docente = new Docente();
    	return $docente->set($user_data);
    } else{
    	return "Invalid";
    }
}

function docente_update(){
	$user_data = helper_docente_data_array();
	if(!empty($user_data)){
		$docente = new Docente();
	    return $docente->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function docente_activar(){
	if(isset($_POST['id'])){
		if(!empty($_POST['id'])){
			$docente = new Docente();
			return $docente->activar($_POST['id']);
		} else{
	    	return "Invalid";
	    }
	} else{
    	return "Invalid";
    }
}

function docente_desactivar(){
	if(isset($_POST['id'])){
		if(!empty($_POST['id'])){
			$docente = new Docente();
			return $docente->desactivar($_POST['id']);
		} else{
	    	return "Invalid";
	    }
	} else{
    	return "Invalid";
    }
}

function docente_delete(){
	$user_data = helper_docente_data_array();
	if(!empty($user_data)){
		$docente = new Docente();
		return format_resp($docente->delete($user_data['id']));
	} else{
    	return "Invalid";
    }
}


/* 	RECIBE POST ARRAY Y CONVIERTE A ARRAY*/

function helper_docente_data_array() {
	$user_data = array();

	if(array_key_exists('nocontrol', $_POST)) {
		$user_data['nocontrol'] = $_POST['nocontrol'];
	}

	if(array_key_exists('nombre', $_POST)) {
		$user_data['nombre'] = $_POST['nombre'];
	}

	if(array_key_exists('paterno', $_POST)) {
		$user_data['paterno'] = $_POST['paterno'];
	}

	if(array_key_exists('materno', $_POST)) {
		$user_data['materno'] = $_POST['materno'];
	}

	if(array_key_exists('edo', $_POST)) {
		$user_data['edo'] = $_POST['edo'];
	}

	if(array_key_exists('curp', $_POST)) {
		$user_data['curp'] = $_POST['curp'];
	}

	if(array_key_exists('email', $_POST)) {
		$user_data['email'] = $_POST['email'];
	}

	if(array_key_exists('nacionalidad', $_POST)) {
		$user_data['nacionalidad'] = $_POST['nacionalidad'];
	}

	if(array_key_exists('genero', $_POST)) {
		$user_data['genero'] = $_POST['genero'];
	}

	if(array_key_exists('nacimiento', $_POST)) {
		$user_data['nacimiento'] = $_POST['nacimiento'];
	}

	if(array_key_exists('telefono', $_POST)) {
		$user_data['telefono'] = $_POST['telefono'];
	}

	if(array_key_exists('ingreso', $_POST)) {
		$user_data['ingreso'] = $_POST['ingreso'];
	}

	if(array_key_exists('direccion', $_POST)) {
		$user_data['direccion'] = $_POST['direccion'];
	}

	if(array_key_exists('ciudad', $_POST)) {
		$user_data['ciudad'] = $_POST['ciudad'];
	}

	if(array_key_exists('id_estado', $_POST)) {
		$user_data['id_estado'] = $_POST['id_estado'];
	}

	if(array_key_exists('municipio', $_POST)) {
		$user_data['municipio'] = $_POST['municipio'];
	}

	return $user_data;
}

?>
