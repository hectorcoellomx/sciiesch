<?php

class Estado extends bd
{
    private $id;
    private $nombre;

    function __construct($id='') {
        if($id!=''){
            $this->query = "SELECT * FROM estado WHERE id = '$id'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                /*foreach ($this->rows[0] as $propiedad=>$valor):
                    $this->$propiedad = utf8_encode($valor);
                endforeach;*/
                $this->id= $this->rows[0]['id'];
                $this->nombre= $this->rows[0]['nombre'];
            }
        }
    }

    public function get($id=''){
        $this->query = "SELECT * FROM estado";
        $this->get_results_from_query();

        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {  //$this->lista[] = $this->rows[$i];
                $cat = new estado();
                $cat->id= $this->rows[$i]['id'];
                $cat->nombre= utf8_encode($this->rows[$i]['nombre']);

                $datos = array("id"=>$cat->getId(), "nombre"=>$cat->getNombre());
                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
        if(array_key_exists('nombre', $user_data)) {
                    foreach ($user_data as $campo=>$valor) {
                        $$campo = trim(utf8_decode($valor));
                    }
                    if($nombre!=""){
                        $this->query = "INSERT INTO estado(nombre) VALUES ('$nombre')";
                        $this->execute_single_query();
                        $mensaje = $this->msj;
                    } else {
                        $mensaje = 'Invalid';
                    }

            } else {
                $mensaje = 'Invalid';
            }

            return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id!="" && $nombre!=""){
            $this->query = "UPDATE estado SET nombre='$nombre' WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($user_id='') {
        if($user_id!=""){
            $this->query = "DELETE FROM estado WHERE id = '$user_id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getId(){
        return $this->id;
    }

    function getNombre(){
        return $this->nombre;
    }

    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
