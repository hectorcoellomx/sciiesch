<?php

function estado($id){
	$estado = new Estado($id);
	$datos = array("id"=>$estado->getId(), "nombre"=>$estado->getNombre());
    return $datos;
}

function estado_lista(){
	$estado = new Estado();
	$datos=$estado->get();
    return $datos;
}

function estado_nuevo(){
	$user_data = helper_estado_data_array();
	if(!empty($user_data)){
		$estado = new Estado();
    	return $estado->set($user_data);
    } else{
    	return "Invalido";
    }
}

function estado_update(){
	$user_data = helper_estado_data_array();
	if(!empty($user_data)){
		$estado = new Estado();
	    return $estado->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function estado_delete(){
	$user_data = helper_estado_data_array();
	if(!empty($user_data)){
		$estado = new Estado();
		return $estado->delete($user_data['id']);
	} else{
    	return "Invalid";
    }
}

// 	RECIBE POST ARRAY Y CONVIERTE A ARRAY

function helper_estado_data_array() {
	$user_data = array();

	if(array_key_exists('id', $_POST)) {
		$user_data['id'] = $_POST['id'];
	}

	if(array_key_exists('nombre', $_POST)) {
		$user_data['nombre'] = $_POST['nombre'];
	}
	return $user_data;
}


?>
