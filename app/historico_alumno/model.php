<?php

class Historico_alumno extends bd
{
    private $tipo;
	private $grupo;
    private $alumno;
	private $programa;

    function __construct($id='') {
        if($id!=''){
            $this->query = "SELECT * FROM historico_alumno WHERE id = '$id'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
              $this->tipo = $this->rows[0]['tipo'];
              $this->grupo = grupo($this->rows[0]['grupo']);
              $this->programa = programa($this->rows[0]['programa']);
              $this->alumno = alumno($this->rows[0]['alumno']);
            }
        }
    }

    public function get($id_programa='', $grupo=''){
        //die($id_programa);
        if($id_programa==""){
            $this->query = "SELECT * FROM historico_alumno";
        } else{
            if($grupo=="")
                $this->query = "SELECT * FROM historico_alumno WHERE id_programa = '$id_programa' ORDER BY `codigo_grupo` ASC";
            else
                $this->query = "SELECT * FROM historico_alumno WHERE id_programa = '$id_programa' AND codigo_grupo = '$grupo'";
        }

        $this->get_results_from_query();

        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {
                $cat = new historico_alumno();
                $cat->tipo = $this->rows[$i]['tipo'];
                $cat->grupo = grupo($this->rows[$i]['codigo_grupo']);
                $cat->programa = programa($this->rows[$i]['id_programa']);
                $cat->alumno = alumno($this->rows[$i]['nocontrol_alumno']);

                $datos = array("programa"=>$cat->getPrograma(),"alumno"=>$cat->getAlumno(),"grupo"=>$cat->getGrupo(),"tipo"=>$cat->getTipo());
                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id_programa!="" && $nocontrol!="" && $grupo!="" && $tipo!=""){
            $this->query = "INSERT INTO historico_alumno(id_programa, nocontrol_alumno, codigo_grupo, tipo) VALUES ($id_programa, '$nocontrol', '$grupo',$tipo)";
            //die($this->query);
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else {
            $mensaje = 'Invalid';
        }

        return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id!="" && $tipo!=""){
            $this->query = "UPDATE historico_alumno SET tipo='$tipo' WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($user_data=array()) {
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id_programa!="" && $nocontrol){
            $this->query = "DELETE FROM historico_alumno WHERE id_programa = $id_programa AND nocontrol_alumno = '$nocontrol'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getTipo(){
          return $this->tipo;
      }
    function getGrupo(){
          return $this->grupo;
      }
	function getAlumno(){
        return $this->alumno;
    }
	function getPrograma(){
        return $this->programa;
    }


    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
