<?php

function historico_alumno($id,$idi){
	$historico_alumno = new Historico_alumno($id,$idi);
	$datos = array("tipo"=>$historico_alumno->getTipo(), "grupo"=>$historico_alumno->getGrupo(), "alumno"=>$historico_alumno->getAlumno(), "programa"=>$historico_alumno->getPrograma());
    return $datos;
}

function historico_alumno_lista(){
	$historico_alumno = new Historico_alumno();
	$datos=$historico_alumno->get();
    return $datos;
}

function historico_alumno_lista_programa($id_programa){
	$historico_alumno = new Historico_alumno();
	$datos=$historico_alumno->get($id_programa);
    return $datos;
}
function historico_alumno_lista_programa_grupo($id_programa, $codigo_grupo){
	$historico_alumno = new Historico_alumno();
	$datos=$historico_alumno->get($id_programa, $codigo_grupo);
    return $datos;
}

function historico_alumno_nuevo($id,$nocontrol,$tipo,$grupo){
	$userdata = array();
	$userdata['id_programa'] = $id;
	$userdata['nocontrol'] = $nocontrol;
	$userdata['tipo'] = $tipo;
	$userdata['grupo'] = $grupo;
	if(!empty($userdata)){
		$historico_alumno = new Historico_alumno();
    	return $historico_alumno->set($userdata);
    } else{
    	return "Invalido";
    }
}

function historico_alumno_update(){
	$user_data = helper_historico_alumno_data_array();
	if(!empty($user_data)){
		$historico_alumno = new Historico_alumno();
	    return $historico_alumno->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function historico_alumno_delete($id_programa,$nocontrol){
	$userdata = array();
	$userdata['id_programa'] = $id_programa;
	$userdata['nocontrol'] = $nocontrol;
	if(!empty($userdata)){
		$historico_alumno = new Historico_alumno();
		return $historico_alumno->delete($userdata);
	} else{
    	return "Invalid";
    }
}



/* 	RECIBE POST ARRAY Y CONVIERTE A ARRAY*/

function helper_historico_alumno_data_array() {
	$user_data = array();

	if(array_key_exists('tipo', $_POST)) {
		$user_data['tipo'] = $_POST['tipo'];
	}

	if(array_key_exists('nocontrol_alumno', $_POST)) {
		$user_data['nocontrol_alumno'] = $_POST['nocontrol_alumno'];
	}

	if(array_key_exists('codigo_grupo', $_POST)) {
		$user_data['codigo_grupo'] = $_POST['codigo_grupo'];
	}

	if(array_key_exists('id_programa', $_POST)) {
		$user_data['id_programa'] = $_POST['id_programa'];
	}



	return $user_data;
}




?>
