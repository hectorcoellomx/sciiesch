<?php

if(!isset($path)){
	$path="";
}

include ($path . 'app/core/bd.php');

if(isset($mod)){
	foreach ($mod as $m) {
		include ($path . 'app/' . $m . '/model.php');
		include($path . 'app/' . $m . '/functions.php');
	}
}

function rutas($cadena){

	$arr= explode(",",$cadena);

	$mayor = (count($arr)>1)? true : false;

	echo "<div class='rutas'>";

	foreach($arr as $a) {
		$des=$a;

		if($a=="infraestructura" || $a=="docente" || $a=="alumno")
			$des="lista";

		$des= str_replace("his","",$des);

		echo "<a href='" . $a . ".php'>" . $des . "</a>";
	}

	echo "</div>";
}

?>
