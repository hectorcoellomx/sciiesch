<?php

class Caracteristicas extends bd
{
    private $id;
    private $nombre;
	private $asignado;

    function __construct($id='') {
        if($id!=''){
            $this->query = "SELECT * FROM caracteristicas WHERE id = '$id'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                foreach ($this->rows[0] as $propiedad=>$valor):
                $this->$propiedad = utf8_encode($valor);
                endforeach;
            }
        }
    }

    public function get($id=''){
        if($id=="")
            $this->query = "SELECT * FROM caracteristicas";
        else 
            $this->query = "SELECT * FROM caracteristicas WHERE asignado=2 OR asignado=" . $id;
        

        $this->get_results_from_query();

        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) { 
                $cat = new Caracteristicas();
                foreach ($this->rows[$i] as $propiedad=>$valor):
                    $cat->$propiedad = utf8_encode($valor);
                endforeach;
                $datos = array("id"=>$cat->getId(), "nombre"=>$cat->getNombre(), "asignado"=>$cat->getAsignado());
                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($nombre!="" && $asignado!=""){
            $this->query = "INSERT INTO caracteristicas(nombre,asignado) VALUES ('$nombre',$asignado)";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else {
            $mensaje = 'Invalid';
        }

        return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id!="" && $nombre!="" && $asignado!=""){
            $this->query = "UPDATE caracteristicas SET nombre='$nombre', asignado=$asignado WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($user_id='') {
        if($user_id!=""){
            $this->query = "DELETE FROM caracteristicas WHERE id = '$user_id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getId(){
        return $this->id;
    }
	function getNombre(){
        return $this->nombre;
    }
	function getAsignado(){
        return $this->asignado;
    }


    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
