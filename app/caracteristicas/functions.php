<?php

function caracteristicas($id){
	$caracteristicas = new Caracteristicas($id);
	$datos = array("id"=>$caracteristicas->getId(), "nombre"=>$caracteristicas->getNombre(), "asignado"=>$caracteristicas->getAsignado());
    return $datos;
}

function caracteristicas_lista_alumnos(){
	$caracteristicas = new Caracteristicas();
	$datos=$caracteristicas->get('0');
    return $datos;
}

function caracteristicas_lista_docentes(){
	$caracteristicas = new Caracteristicas();
	$datos=$caracteristicas->get('1');
    return $datos;
}

function caracteristicas_nuevo(){
	$user_data = helper_caracteristicas_data_array();
	if(!empty($user_data)){
		$caracteristicas = new Caracteristicas();
    	return $caracteristicas->set($user_data);
    } else{
    	return "Invalido";
    }
}

function caracteristicas_update(){
	$user_data = helper_caracteristicas_data_array();
	if(!empty($user_data)){
		$caracteristicas = new Caracteristicas();
	    return $caracteristicas->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function caracteristicas_delete(){
	$user_data = helper_caracteristicas_data_array();
	if(!empty($user_data)){
		$caracteristicas = new Caracteristicas();
		return $caracteristicas->delete($user_data['id']);
	} else{
    	return "Invalid";
    }
}

// 	RECIBE POST ARRAY Y CONVIERTE A ARRY

function helper_caracteristicas_data_array() {
	$user_data = array();

	if(array_key_exists('id', $_POST)) {
		$user_data['id'] = $_POST['id'];
	}

	if(array_key_exists('nombre', $_POST)) {
		$user_data['nombre'] = $_POST['nombre'];
	}

	if(array_key_exists('asignado', $_POST)) {
		$user_data['asignado'] = $_POST['asignado'];
	}

	return $user_data;
}


?>
