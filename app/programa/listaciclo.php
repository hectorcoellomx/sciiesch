<?php 
include ('../core/bd.php');
include ('../carrera/model.php');
include('../carrera/functions.php');
include ('../plan/model.php');
include('../plan/functions.php');
include ('../ciclo/model.php');
include('../ciclo/functions.php');
include ('./model.php');
include('./functions.php');

if(isset($_POST['year']) && isset($_POST['idcarrera'])){
	
	$lista= programa_lista_carrera_year($_POST['year'],$_POST['idcarrera']);

	if(is_array($lista)){
		$opciones="<option selected disabled>Seleccionar</option>";
		foreach ($lista as $l) {
			$opciones .= '<option value="' . $l['id'] . '">' . strtoupper($l['ciclo']['plan']['modalidad']) . ' (' . $l['ciclo']['descripcion'] . ')</option>';
		}
		echo "<select onchange='verPrograma(this.value);'>" . $opciones . "</select>";
	} else{
		echo "<select></select>";;
	}
}else{
	echo "Error";
}



?>