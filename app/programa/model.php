<?php

class Programa extends bd
{
    private $id;
    private $year;
    private $ciclo;
    private $carrera;

    function __construct($id='') {
        if($id!=''){
            $this->query = "SELECT * FROM programa WHERE id = '$id'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                foreach ($this->rows[0] as $propiedad=>$valor):
                    if($propiedad=="id_ciclo")
                        $this->ciclo = ciclo($valor);
                    elseif($propiedad=="id_carrera")
                        $this->carrera = carrera($valor);
                    else
                        $this->$propiedad = utf8_encode($valor);
                endforeach;
            }
        }
    }

    public function get($year='',$id_carrera='',$lista=''){

        $onlyyear=false;

        if($year!="" ){
            if($id_carrera!="")
                $this->query = "SELECT * FROM programa WHERE year='" . $year ."' && id_carrera='" . $id_carrera ."'";
            else
                $this->query = "SELECT * FROM programa WHERE year='" . $year ."'";
        }else{
            if($id_carrera!=""){
                $this->query = "SELECT DISTINCT year FROM programa WHERE id_carrera='" . $id_carrera ."' ORDER BY year DESC";
                $onlyyear=true;
            }
            else{ 
                if($lista=="")
                    $this->query = "SELECT * FROM programa";
                else
                    $this->query = "SELECT DISTINCT id_carrera FROM programa";
            }
        }

        $this->get_results_from_query();

        if (count($this->rows) >= 1) {  
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {  //$this->lista[] = $this->rows[$i];
                $cat = new Programa();
                foreach ($this->rows[$i] as $propiedad=>$valor):
                    if($propiedad=="id_ciclo")
                        $cat->ciclo = ciclo($valor);
                    elseif($propiedad=="id_carrera")
                        $cat->carrera = carrera($valor);
                    else
                        $cat->$propiedad = utf8_encode($valor);
                endforeach;

                if(!$onlyyear){ //if($lista!=""){ echo "string"; }
                    $datos = array("id"=>$cat->getId(), "year"=>$cat->getYear(), "ciclo"=>$cat->getCiclo(), "carrera"=>$cat->getCarrera());
                }else{
                    $datos = array("year"=>$cat->getYear());
                }

                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
                    foreach ($user_data as $campo=>$valor) {
                        $$campo = trim(utf8_decode($valor));
                    }

                    if($year!="" && $id_ciclo!="" && $id_carrera!=""){
                        //$year=date("Y");
                        $this->query = "INSERT INTO programa(year,id_ciclo,id_carrera) VALUES ($year,$id_ciclo, $id_carrera)";
                        $this->execute_single_query();
                        $mensaje = $this->msj;
                    } else {
                        $mensaje = 'Invalid';
                    }
            return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id!="" && $year!="" && $id_ciclo!="" && $id_carrera!=""){
            $year=date("Y");
            $this->query = "UPDATE programa SET year='$year', id_ciclo='$id_ciclo', id_carrera='$id_carrera' WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($user_id='') {
        if($user_id!=""){
            $this->query = "DELETE FROM programa WHERE id = '$user_id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getId(){
        return $this->id;
    }

    function getYear(){
        return $this->year;
    }

    function getCiclo(){
        return $this->ciclo;
    }

    function getCarrera(){
        return $this->carrera;
    }

    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
