<?php

function programa($id){
	$programa = new Programa($id);
	$datos = array("id"=>$programa->getId(), "year"=>$programa->getYear(), "ciclo"=>$programa->getCiclo(), "carrera"=>$programa->getCarrera());
    return $datos;
}

function programa_lista(){
	$programa = new Programa();
	$datos=$programa->get();
    return $datos;
}
function programa_carreras(){
	$programa = new Programa();
	$datos=$programa->get('','','carreras');
    return $datos;
}
function programa_lista_carrera($id_carrera){
	$programa = new Programa();
	$datos=$programa->get('',$id_carrera);
    return $datos;
}
function programa_lista_carrera_year($year,$id_carrera){
	$programa = new Programa();
	$datos=$programa->get($year,$id_carrera);
    return $datos;
}

function programa_nuevo(){
	$user_data = helper_programa_data_array();
	if(!empty($user_data)){
		$programa = new Programa();
    	return $programa->set($user_data);
    } else{
    	return "Invalid";
    }
}

function programa_update(){
	$user_data = helper_programa_data_array();
	if(!empty($user_data)){
		$programa = new Programa();
	    return $programa->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function programa_delete(){
	$user_data = helper_programa_data_array();
	if(!empty($user_data)){
		$programa = new Programa();
		return $programa->delete($user_data['id']);
	} else{
    	return "Invalid";
    }
}

// 	RECIBE POST ARRAY Y CONVIERTE A ARRAY

function helper_programa_data_array() {
	$user_data = array();

	if(array_key_exists('id', $_POST)) {
		$user_data['id'] = $_POST['id'];
	}

	if(array_key_exists('year', $_POST)) {
		$user_data['year'] = $_POST['year'];
	}

	if(array_key_exists('id_ciclo', $_POST)) {
		$user_data['id_ciclo'] = $_POST['id_ciclo'];
	}

	if(array_key_exists('id_carrera', $_POST)) {
		$user_data['id_carrera'] = $_POST['id_carrera'];
	}

	return $user_data;
}




?>
