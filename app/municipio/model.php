<?php

class Municipio extends bd
{
    private $id;
    private $nombre;
    private $estado;

    function __construct($id='') {
        if($id!=''){
            $this->query = "SELECT * FROM municipio WHERE id = '$id'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                foreach ($this->rows[0] as $propiedad=>$valor):
                    if($propiedad!="id_estado")
                        $this->$propiedad = utf8_encode($valor);
                    else
                        $this->estado = estado($valor);
                endforeach;
            }
        }
    }

    public function get($id=''){
        $this->query = "SELECT * FROM municipio";
        $this->get_results_from_query();

        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {  
                $cat = new municipio();
                /*foreach ($this->rows[$i] as $propiedad=>$valor):
                    if($propiedad!="id_estado")
                        $cat->$propiedad = utf8_encode($valor);
                    else
                        $cat->estado = estado($valor);
                endforeach;*/
                $cat->id = $this->rows[$i]['id'];
                $cat->nombre = utf8_encode($this->rows[$i]['nombre']);
                $cat->estado = new estado();//estado($this->rows[$i]['id_estado']);

                $datos = array("id"=>$cat->getId(), "nombre"=>$cat->getNombre(), "estado"=>$cat->getEstado());

                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
        if(array_key_exists('nombre', $user_data)) {
                    foreach ($user_data as $campo=>$valor) {
                        $$campo = trim(utf8_decode($valor));
                    }
                    if($nombre!=""){
                        $this->query = "INSERT INTO municipio(nombre) VALUES ('$nombre')";
                        $this->execute_single_query();
                        $mensaje = $this->msj;
                    } else {
                        $mensaje = 'Invalid';
                    }

            } else {
                $mensaje = 'Invalid';
            }

            return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id!="" && $nombre!=""){
            $this->query = "UPDATE municipio SET nombre='$nombre' WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($user_id='') {
        if($user_id!=""){
            $this->query = "DELETE FROM municipio WHERE id = '$user_id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getId(){
        return $this->id;
    }

    function getNombre(){
        return $this->nombre;
    }

    function getEstado(){
        return $this->estado;
    }


    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
