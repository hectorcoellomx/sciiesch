<?php

function municipio($id){
	$municipio = new Municipio($id);
	$datos = array("id"=>$municipio->getId(), "nombre"=>$municipio->getNombre(), "estado"=>$municipio->getEstado());
    return $datos;
}

function municipio_lista(){
	$municipio = new Municipio();
	$datos=$municipio->get();
    return $datos;
}

function municipio_nuevo(){
	$user_data = helper_municipio_data_array();
	if(!empty($user_data)){
		$municipio = new Municipio();
    	return $municipio->set($user_data);
    } else{
    	return "Invalido";
    }
}

function municipio_update(){
	$user_data = helper_municipio_data_array();
	if(!empty($user_data)){
		$municipio = new Municipio();
	    return $municipio->edit($user_data);
    } else{
    	return "Invalid";
    }
}

function municipio_delete(){
	$user_data = helper_municipio_data_array();
	if(!empty($user_data)){
		$municipio = new Municipio();
		return $municipio->delete($user_data['id']);
	} else{
    	return "Invalid";
    }
}

// 	RECIBE POST ARRAY Y CONVIERTE A ARRAY

function helper_municipio_data_array() {
	$user_data = array();

	if(array_key_exists('id', $_POST)) {
		$user_data['id'] = $_POST['id'];
	}

	if(array_key_exists('nombre', $_POST)) {
		$user_data['nombre'] = $_POST['nombre'];
	}

	if(array_key_exists('id_estado', $_POST)) {
		$user_data['id_estado'] = $_POST['id_estado'];
	}
	return $user_data;
}


?>
