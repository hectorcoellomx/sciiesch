<?php

class Infraestructura extends bd
{
    private $id;
    private $tipo;
    private $edo;

    function __construct($id='') {
        if($id!=''){
            $this->query = "SELECT * FROM infraestructura WHERE id = '$id'";
            $this->get_results_from_query();
            if(count($this->rows) == 1){
                foreach ($this->rows[0] as $propiedad=>$valor):
                $this->$propiedad = utf8_encode($valor);
                endforeach;
            }
        }
    }

    public function get($edo=''){
        if($edo==""){
            $this->query = "SELECT * FROM infraestructura ORDER BY `infraestructura`.`edo` DESC";
        }else{
            $this->query = "SELECT * FROM infraestructura WHERE edo='" . $edo ."' ORDER BY `infraestructura`.`edo` DESC";
        }
        $this->get_results_from_query();
        if (count($this->rows) >= 1) {
            $lista = array();

            for ($i=0 ; $i < count($this->rows); $i++ ) {  //$this->lista[] = $this->rows[$i];
                $cat = new Infraestructura();
                foreach ($this->rows[$i] as $propiedad=>$valor):
                    $cat->$propiedad = utf8_encode($valor);
                endforeach;
                $datos = array("id"=>$cat->getId(), "tipo"=>$cat->getTipo(), "edo"=>$cat->getEdo());
                $lista[] = $datos;
            }

            return $lista;
        }

    }

    public function set($user_data=array()){
            foreach ($user_data as $campo=>$valor) {
                $$campo = trim(utf8_decode($valor));
            }
            if($tipo!="" && $edo!=""){
                $this->query = "INSERT INTO infraestructura(tipo,edo) VALUES ('$tipo',$edo)";
                $this->execute_single_query();
                $mensaje = $this->msj;
            } else {
                $mensaje = 'Invalid';
            }

            return $mensaje;
    }

    public function edit($user_data=array()){
        foreach ($user_data as $campo=>$valor) {
            $$campo = trim(utf8_decode($valor));
        }
        if($id!="" && $tipo!=""){
            $this->query = "UPDATE infraestructura SET tipo='$tipo', edo='$edo' WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        }else {
            $mensaje = 'Invalid';
        }
        return $mensaje;
    }

    public function delete($id='') {
        if($id!=""){
            $this->query = "DELETE FROM infraestructura WHERE id = '$id'";
            $this->execute_single_query();
            $mensaje = $this->msj;
        } else{
            $mensaje = 'Invalid';
        }

        return $mensaje;

    }

    function getId(){
        return $this->id;
    }

    function getTipo(){
        return $this->tipo;
    }

    function getEdo(){
        return $this->edo;
    }

    # Método destructor del objeto
    function __destruct() {
        unset($this);
    }

}


?>
