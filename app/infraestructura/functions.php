<?php

function infraestructura($id){
	$infraestructura = new Infraestructura($id);
	$datos = array("id"=>$infraestructura->getId(), "tipo"=>$infraestructura->getTipo(), "edo"=>$infraestructura->getEdo());
    return $datos;
}

function infraestructura_lista(){
	$infraestructura = new Infraestructura();
	$datos=$infraestructura->get();
    return $datos;
}
function infraestructura_lista_act(){
	$infraestructura = new Infraestructura();
	$datos=$infraestructura->get('1');
    return $datos;
}
function infraestructura_lista_des(){
	$infraestructura = new Infraestructura();
	$datos=$infraestructura->get('0');
    return $datos;
}
function infraestructura_nuevo(){
	$user_data = helper_infraestructura_data_array();
	if(!empty($user_data)){
		$infraestructura = new Infraestructura();
    	return $infraestructura->set($user_data);
    } else{
    	return "Invalido";
    }
}

function infraestructura_update(){
	$user_data = helper_infraestructura_data_array();
	if(!empty($user_data)){
		$infraestructura = new Infraestructura();
	    return $infraestructura->edit($user_data);
    } else{
    	return format_resp("Invalid");
    }
}

function infraestructura_delete(){
	$user_data = helper_infraestructura_data_array();
	if(!empty($user_data)){
		$infraestructura = new Infraestructura();
		return $infraestructura->delete($user_data['id']);
	} else{
    	return format_resp("Invalid");
    }
}


// RECIBE POST JSON Y CONVIERTE A ARRAY (For Angular)
function helper_infraestructura_data_json() {

	$user_data = array();
	$postdata = file_get_contents("php://input");
  $request = json_decode($postdata);
  

    if(isset($request->id)) {
		$user_data['id'] = $request->id;
	}
	if(isset($request->nombre)) {
		$user_data['nombre'] = $request->nombre;
	}

    return $user_data;
}

/* 	RECIBE POST ARRAY Y CONVIERTE A ARRAY*/

function helper_infraestructura_data_array() {
	$user_data = array();

	if(array_key_exists('id', $_POST)) {
		$user_data['id'] = $_POST['id'];
	}

	if(array_key_exists('tipo', $_POST)) {
		$user_data['tipo'] = $_POST['tipo'];
	}

	if(array_key_exists('edo', $_POST)) {
		$user_data['edo'] = $_POST['edo'];
	}

	return $user_data;
}




?>
