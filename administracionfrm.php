<?php
session_start();
$id_sesion = session_id();
$mod = array("usuario");
include ('app/modulos.php');
include ('app/sesion.php');

if($user['tipo']!=0){
  header('location:index.php'); die();
}

$tipo=0;

$update=false;
  if(isset($_GET["type"]) && isset($_GET["id"])){
    if($_GET["type"]=="update" && $_GET["id"]!=""){
        $item=usuario($_GET["id"]);
        $update=true;
    }
  }


 if(isset($_POST["submit"])){
  if(isset($_POST["ac"])){
    if($_POST["ac"]=="actualizar"){
      $actualiza = usuario_update();
      header('location:administracion.php'); die();
    }else {
      $nuevo = usuario_nuevo();
      header('location:administracion.php'); die();
    }


  }
 }
?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=0; include 'inc/header.php'; ?>

    <section id="principal">

      <div class="container">

          <div class="row">
          <div class="col-md-12">
          <form id="formulario" method="post" class="formulario">
              <div class="frmtitulo"><?php if($update){ echo "Actualizar Usuario"; } else { echo "Agregar Usuario"; } ?></div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Username
                </div>
                <div class="dato col-md-8">
                    <input name="username" type="text" required value="<?php if($update){ echo $item['username']; }?>">
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Nombre Completo
                </div>
                <div class="dato col-md-8">
                    <input name="nombre" type="text" required value="<?php if($update){ echo $item['nombre']; } ?>">
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Password
                </div>
                <div class="dato col-md-8">
                    <input name="password" type="text">
                </div>
              </div>

              <?php if($update){ ?>

              <div class="fila row">
                <div class="tit col-md-4">
                    Cambiar contraseña
                </div>
                <div class="dato col-md-8" style="text-align: left;">
                  <div><input type="radio" name="cambiarpass" style="width: 10px;" value="1">
                  &nbsp;<span>Si</span>
                  </div>
                  <div>
                  <input type="radio" name="cambiarpass" style="width: 10px;" value="0" checked>
                  &nbsp;<span>No</span>
                  </div>
                </div>
              </div>
              <br>
              <?php } ?>

               <!--div class="fila row">
                <div class="tit col-md-4">
                    Tipo
                </div>
                <div class="dato col-md-8">
                    <select name="tipo">
                      <option value="0" <?php if($update){ echo ($item['tipo']==0)? "selected":""; } ?>>
                        Administrador
                      </option>
                      <option value="1" <?php if($update){ echo ($item['tipo']==1)? "selected":""; } ?>>
                        Personal
                      </option>
                    </select>
                </div>

              </div-->

              <div class="botones">
              <input type="hidden" name="ac" value="<?php echo ($update)? "actualizar":"agregar"; ?>">
              <input name="id" type="hidden" value="<?php if($update){ echo $item['id']; }?>">
                <input class="boton" type="submit" name="submit" value="Guardar">
                <a href='administracion.php' class="boton">Cancelar</a>
              </div>

              </form>
            </div>

        </div>

      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
