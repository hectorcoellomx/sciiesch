<?php
session_start();
$id_sesion = session_id();
$mod = array("carrera","plan", "ciclo", "estado","municipio","persona","docente", "caracteristicas", "desglose_caracteristicas_docente");
include ('app/modulos.php');
include ('app/sesion.php');

// IS UPDATE
$update=false;

// IS NORMAL (Necesita ID)
 if(isset($_GET["ciclo"]) && isset($_GET["year"]) && isset($_GET["nocontrol"])){
    if(trim($_GET["ciclo"])=="" && trim($_GET["year"])=="" && trim($_GET['nocontrol'])==""){
      header("Location: docente.php");
    }
 }else{
    header("Location: docente.php");
 }


$yaexiste=false;

 //POST SUBMIT
  if(isset($_POST["submit"])){

    if(!isset($_POST["update"])){ // FOR CREATE
      if($_POST['accion']=="agregar")
        $res=desglose_caracteristicas_docente_nuevo();
      else
        $res=desglose_caracteristicas_docente_update(); //$res=desglose_caracteristicas_docente_delete($_POST['id_ciclo'],$_POST['year'],$_POST['nocontrol_docente'],$_POST['id_caracteristica']);

      if($res!="done"){
        if($res=="1062")
          $yaexiste=true;
      }
    }
  }

$id_ciclo = $_GET["ciclo"];
$year = $_GET["year"];
$nocontrol = trim($_GET['nocontrol']);


if($id_ciclo!="activos") {

  //DATOS DEL CICLO
  $cabecera= ciclo($id_ciclo);
  if($cabecera['id']==""){
    header("Location: docente.php");
  }

  $descripcion=$cabecera['descripcion'];
  $mes_inicio=$cabecera['mes_inicio'];
  $mes_final=$cabecera['mes_final'];

  // Todos los meses y años anteriores al mes actual toma mes final
  if(($year==date("Y") && $mes_final< date("n")) || $year<date("Y")){
    //Toma mes final
    if($mes_final<10)
      $mes= "0".$mes_final;
    else
      $mes = $mes_final;

  }else{ // Mes igual o meses superiores y años superiores
      $mes=date("m");
      $year= date("Y");
  }

} else {
  $descripcion= "PRIMERA VEZ";
  $mes= "01"; //$mes=date("m");
}

$fecha= "01-". $mes ."-". $year;

//DATOS DEL DOCENTE
$cabecera2= docente($nocontrol);
if($cabecera2['nocontrol']==""){
  header("Location: docente.php");
}
$nombre_completo = $cabecera2['nombre'] . " " . $cabecera2['paterno'] . " " . $cabecera2['materno'];

// LISTA DE CARACTERISTICAS
$lista=caracteristicas_lista_docentes();

//CICLO ABIERTO O CERRADO
$abierto=false;
if($user['tipo']==0){ // Si es Admin siempre es abierto
    $abierto=true;
}else{
  if($_GET["year"]==date("Y")){
      if($id_ciclo=="activos"){ // SI ES PRIMERA VEZ
        $abierto=true;
      }else{
        if($mes_inicio<=date("n") && $mes_final>=date("n")){ // Si es un ciclo activo
          $abierto=true;
        }
      }
    }
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=5; include 'inc/header.php'; ?>

    <section id="principal">

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <form id="formulario" method="post" class="formulario">
              <div class="frmtitulo">Características del Docente</div>

              <?php
                echo "<div>" . $descripcion . " - "  . $mes . "/" . $year . "<br>";
                $link= "?ciclo=" . $id_ciclo . "&year=" . $_GET["year"] . "&nocontrol=" . $nocontrol;
                echo "<a href='".$link."'>" . $nombre_completo . "</a><br>";

                $ruta = (isset($_GET['historial'])) ? 'docentehis.php?id=' . $_GET['historial'] : 'docente.php';
                echo "<a href='".$ruta."' class='boton'>Regresar</a></div>";
               ?>
            </form>
            <br>
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="col-md-5">Nombre</th>
                 <th class="col-md-1 acciones">Fecha</th>
                 <th class="col-md-2 acciones">Caracteristica</th>
                 <th class="col-md-2 acciones">Valor</th>
                 <th class="col-md-2 acciones">Extra</th>
                </tr>
              </thead>
              <tbody>
                <?php

                  if(is_array($lista)){
                    foreach ($lista as $l) {
                      $accion="agregar";
                      $des="";
                      $val=0;
                      $f="";
                      $car=desglose_caracteristicas_docente($l['id'],$nocontrol,strtotime($fecha),"consultar");
                      if($car['docente']['nocontrol']!=""){
                        $accion="actualizar";
                        $des= $car['descripcion'];
                        $val= $car['valor'];
                        $f= date ('m/Y', $car['fecha']);
                      }


                      echo "<tr class='op-".$accion."'>";
                      echo '<form id="formulario" method="post" class="formulario">';
                      echo "<td>" . $l['nombre'];

                      /*echo '<input type="hidden" name="year" required value="' . $year . '" >';
                      echo '<input type="hidden" name="nocontrol_docente" required value="' . $nocontrol . '" >';
                      echo '<input type="hidden" name="id_caracteristica" required value="' . $l['id'] . '" >';

                      echo '<input type="hidden" id="acc' . $l['id'] . '" name="accion" required value="' . $accion . '" >';*/

                      echo "</td>";
                      echo "<td class='acciones'><span id='fc" . $l['id'] . "'>" . $f . "</span></td>";
                      echo "<td class='acciones'>";

                      if ($abierto) {
                        if($id_ciclo=="activos"){
                          $id_ciclo=0;
                        }
                        echo '<input id="car' . $l['id'] . '" class="boton" onclick="actualizar(\'' . strtotime($fecha) . '\',\'' . $nocontrol . '\',' . $l['id'] . ',' . $id_ciclo . ')"; type="button" name="submit" value="' . $accion. '">';
                      }else{
                        echo "Ciclo cerrado";
                      }

                      echo "</td>";

                        //echo "<td><input type='text' name='valor' value='" . $val . "'>";
                        echo "<td class='acciones'>";
                        if ($abierto) {
                          echo '<select id="valor' . $l['id'] . '" name="valor">';
                          $sel = ($val == 0) ? 'selected' : '';
                          echo '<option value="0">Falso</option>';
                          $sel = ($val == 1) ? 'selected' : '';
                          echo '<option value="1" ' . $sel . '>Verdadero</option>';
                          echo '</select>';
                        }else{
                            $sel = ($val == 0) ? 'False' : 'Verdadero';
                            echo $sel;
                        }

                        echo "</td>";
                        echo "<td class='acciones'>";
                        if ($abierto) {
                          echo "<input id='des" . $l['id'] . "' type='text' name='descripcion' value='" . $des . "'></td>";
                        }else{
                            echo ($des != "") ? $des : "--";
                        }
                        echo '</form>';
                        echo "</tr>";

                    }
                  }
                ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>
    <?php if($yaexiste){
        echo "<script>alert('No se ha podido guardar el registo. Este dato ya existe registrado.');</script>";
      } ?>
  </body>
</html>
