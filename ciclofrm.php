<?php
session_start();
$id_sesion = session_id();
$mod = array("plan", "ciclo");
include ('app/modulos.php');
include ('app/sesion.php');

if($user['tipo']!=0){
  header('Location: ciclo.php');
}

 //POST
  if(isset($_POST["submit"])){

    if(!isset($_POST["update"])){
      $res=ciclo_nuevo();
      if($res=="done")
        header('Location: ciclo.php');
    }else{
      $res=ciclo_update();
      if($res=="done")
        header('Location: ciclo.php');
    }
  }

  // GET UPDATE OR CREATE
  $update=false;
  if(isset($_GET["type"]) && $_GET["type"]=="update"){
    if(isset($_GET["id"])){
      if($_GET["id"]!=""){
        $item=ciclo($_GET["id"]);
        $update=true;
      }
    }
  }

  $lista=plan_lista();

?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=5; include 'inc/header.php'; ?>

    <section id="principal">

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <form id="formulario" method="post" class="formulario">
              <div class="frmtitulo"><?php if($update){ echo "Actualizar ciclo"; } else { echo "Agregar ciclo"; } ?></div>
              <div class="fila row">
                <div class="tit col-md-4">
                    Descripcion
                </div>
                <div class="dato col-md-8">
                    <input type="text" name="descripcion" placeholder="Ej. ENE/JUL" required <?php if($update){ echo "value=" . $item["descripcion"]; } ?>>
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Plan
                </div>
                <div class="dato col-md-8">
                    <select name="id_plan">
                    <?php
                      if(is_array($lista)){
                        foreach ($lista as $l) {
                          $sel="";
                          if($update && $item["plan"]["id"]==$l['id']){
                            $sel="selected";
                          }
                          echo '<option value="' . $l['id'] . '" ' . $sel . '>' . $l['modalidad'] . '</option>';
                        }
                      }
                    ?>
                    </select>
                </div>
              </div>

               <?php if($update){ echo "<input type='hidden' name='id' value='" . $item["id"] . "'>"; } ?>
              <?php if($update){ echo "<input type='hidden' name='update' value='true'>"; } ?>

              <div class="botones">
                <input class="boton" type="submit" name="submit" value="Guardar">
                <a href='ciclo.php' class="boton">Cancelar</a>
              </div>

            </form>
          </div>
        </div>
      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
