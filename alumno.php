<?php
session_start();
$id_sesion = session_id();
$mod = array("estado","municipio","persona","alumno");
include ('app/modulos.php');
include ('app/sesion.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=2; include 'inc/header.php'; 

        if(isset($_GET["disp"])){
          if($_GET["disp"]=="desactivados"){
            $lista=alumno_lista_des();
            $visual=0;
            $rcomp="?disp=desactivados"; 
          }elseif($_GET["disp"]=="todos"){
            $lista=alumno_lista();
            $visual=2;
            $rcomp="?disp=todos"; 
          }
          else{
            $lista=alumno_lista_act();
            $visual=1;
            $rcomp=""; 
          }
        }else{
          $lista=alumno_lista_act();
          $visual=1; 
          $rcomp="";
        }
    ?>


    <section id="principal">

      <div class="container">
        <?php rutas("alumnohis,alumno"); ?>
        <div class="row">
          <div class="col-md-10">
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="col-md-2">ID</th>
                 <th>Nombre</th>
                 <th class="col-md-2 acciones">Acción</th>
                </tr>
              </thead>
              <tbody>
                <?php 

                if(is_array($lista)){

                  foreach ($lista as $l) {
                    $st="";
                    $tipo="trash";
                    if($l['edo']==0){
                      $st="des";
                      $tipo="caret-square-o-up";
                    }
                    echo "<tr class='" . $st . "'>";
                    echo "<td>" . $l['nocontrol'] . "</td><td>" . $l['nombre'] . " " . $l['paterno'] . " " . $l['materno'] . "</td>";
                    echo "<td class='acciones'>";
                    echo "<a href='alumnofrm.php?id=" . $l['nocontrol'] . "&type=update'><i class='fa fa-pencil' aria-hidden='true'></i></a>";
                    echo "<a onclick='loadWindow(500,210);loadScreens(\"inc/selciclo_al.php?nocontrol=" . $l['nocontrol'] . "\",\"window\",this);'><i class='fa fa-asterisk' aria-hidden='true'></i></a>";
                    echo "<a onclick='loadWindow(500,175);loadScreens(\"inc/desactivar.php?id=" . $l['nocontrol'] . "&nombre=" . $l['nombre'] . " " . $l['paterno'] . " " . $l['materno'] . "&entidad=alumno&tipo=" . $st . "&finaldest=alumno.php" . $rcomp . "\",\"window\",this);'><i class='fa fa-" . $tipo . "' aria-hidden='true'></i></a>";
                    echo "</td>";  
                    echo "</tr>";  
                  }

                } 

                ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-2">
              <div class="opciones">
                <span id="titulo">Alumno</span>
                  <a class="boton" href="alumnofrm.php">Agregar</a>
              </div>
              <br>
              <div class="opciones">
                <span id="titulo">Visualización</span>
                  <a class="boton" href="alumno.php" <?php if($visual!=1) echo "style='opacity:0.5'" ?>>Activados</a>
                  <a class="boton" href="alumno.php?disp=desactivados" <?php if($visual!=0) echo "style='opacity:0.5'" ?>>Desactivados</a>
                  <a class="boton" href="alumno.php?disp=todos" <?php if($visual!=2) echo "style='opacity:0.5'" ?>>Todos</a>
              </div>
          </div> 
        </div>
      </div>
    
    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>