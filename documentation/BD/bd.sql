/*CREATE DATABASE sciiesch;*/
/*Use saiiesch;*/

CREATE TABLE estado (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(100) NOT NULL)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE municipio (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(100) NOT NULL,
id_estado INT UNSIGNED,
FOREIGN KEY (id_estado) REFERENCES estado(id))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE plan (
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
modalidad VARCHAR(100) NOT NULL)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE ciclo(
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
descripcion VARCHAR(100) NOT NULL,
mes_inicio TINYINT(2) NOT NULL,
mes_final TINYINT(2) NOT NULL,
id_plan INT UNSIGNED  NOT NULL,
FOREIGN KEY (id_plan) REFERENCES plan(id))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE infraestructura(
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
tipo VARCHAR(100) NOT NULL,
edo TINYINT(1) NOT NULL)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE carrera(
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(100) NOT NULL,
director VARCHAR(100) NOT NULL,
coordinador VARCHAR(100) NOT NULL)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE docente (
nocontrol VARCHAR(12) PRIMARY KEY,
nombre VARCHAR(50) NOT NULL,
paterno VARCHAR(50) NOT NULL,
materno VARCHAR(50) NOT NULL,
edo TINYINT(1) NOT NULL,
curp VARCHAR(20) NOT NULL,
email VARCHAR(50) NOT NULL,
nacionalidad VARCHAR(30) NOT NULL,
genero TINYINT(1) NOT NULL,
nacimiento DATE NOT NULL,
telefono VARCHAR(15) NOT NULL,
ingreso DATE NOT NULL,
direccion VARCHAR(140) NOT NULL,
ciudad VARCHAR(60) NOT NULL,
municipio VARCHAR(60) NOT NULL,
id_estado INT UNSIGNED  NOT NULL,
FOREIGN KEY (id_estado) REFERENCES estado(id))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE alumno (
nocontrol VARCHAR(12) PRIMARY KEY,
nombre VARCHAR(50) NOT NULL,
paterno VARCHAR(50) NOT NULL,
materno VARCHAR(50) NOT NULL,
edo TINYINT(1) NOT NULL,
curp VARCHAR(20) NOT NULL,
email VARCHAR(50) NOT NULL,
nacionalidad VARCHAR(30) NOT NULL,
genero TINYINT(1) NOT NULL,
nacimiento DATE NOT NULL,
telefono VARCHAR(15) NOT NULL,
direccion VARCHAR(140) NOT NULL,
ciudad VARCHAR(60) NOT NULL,
municipio VARCHAR(60) NOT NULL,
id_estado INT UNSIGNED  NOT NULL,
FOREIGN KEY (id_estado) REFERENCES estado(id))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE grupo(
codigo VARCHAR(12) PRIMARY KEY,
nombre VARCHAR(2) NOT NULL,
grado INT(2) NOT NULL)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE programa(
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
year INT(4) NOT NULL,
id_ciclo INT UNSIGNED  NOT NULL,
id_carrera INT UNSIGNED  NOT NULL,
FOREIGN KEY (id_carrera) REFERENCES carrera(id),
FOREIGN KEY (id_ciclo) REFERENCES ciclo(id),
UNIQUE KEY `thekey` (`year`,`id_ciclo`,`id_carrera`))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE caracteristicas(
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
nombre VARCHAR(40) NOT NULL,
asignado INT UNSIGNED  NOT NULL)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE historico_alumno(
id_programa INT UNSIGNED  NOT NULL,
codigo_grupo VARCHAR(12) NOT NULL,
nocontrol_alumno VARCHAR(12) NOT NULL,
tipo TINYINT(1) NOT NULL,
FOREIGN KEY (id_programa) REFERENCES programa(id),
FOREIGN KEY (codigo_grupo) REFERENCES grupo(codigo),
FOREIGN KEY (nocontrol_alumno) REFERENCES alumno(nocontrol),
PRIMARY KEY (id_programa, nocontrol_alumno))
ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE des_alumno_caracteristica(
descripcion VARCHAR(12) NOT NULL,
nocontrol_alumno VARCHAR(12) NOT NULL,
id_caracteristica INT UNSIGNED NOT NULL,
fecha VARCHAR(12) NOT NULL,
valor TINYINT(1) NOT NULL,
FOREIGN KEY (nocontrol_alumno) REFERENCES alumno(nocontrol),
FOREIGN KEY (id_caracteristica) REFERENCES caracteristicas(id),
PRIMARY KEY (nocontrol_alumno, id_caracteristica,fecha))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE historico_docente(
id_programa INT UNSIGNED  NOT NULL,
nocontrol_docente VARCHAR(12) NOT NULL,
FOREIGN KEY (id_programa) REFERENCES programa(id),
FOREIGN KEY (nocontrol_docente) REFERENCES docente(nocontrol),
PRIMARY KEY (id_programa, nocontrol_docente))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE des_docente_caracteristica(
descripcion VARCHAR(12) NOT NULL,
nocontrol_docente VARCHAR(12) NOT NULL,
id_caracteristica INT UNSIGNED NOT NULL,
fecha VARCHAR(12) NOT NULL,
valor TINYINT(1) NOT NULL,
FOREIGN KEY (nocontrol_docente) REFERENCES docente(nocontrol),
FOREIGN KEY (id_caracteristica) REFERENCES caracteristicas(id),
PRIMARY KEY (nocontrol_docente, id_caracteristica,fecha))
ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE infraestructura_programa(
cantidad TINYINT(3) NOT NULL,
id_infraestructura INT UNSIGNED NOT NULL,
id_programa INT UNSIGNED NOT NULL,
FOREIGN KEY (id_infraestructura) REFERENCES infraestructura(id),
FOREIGN KEY (id_programa) REFERENCES programa(id),
PRIMARY KEY (id_infraestructura, id_programa))
ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `administrador` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `tipo` TINYINT(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `administrador` ( `id` , `username` , `password` , `nombre` , `tipo`) VALUES ('1', 'admin', MD5( 'prueba' ) , 'Administrador', '0');
INSERT INTO `administrador` ( `id` , `username` , `password` , `nombre` , `tipo`) VALUES ('2', 'personal1', MD5( 'personal1' ) , 'Personal Uno', '1');

CREATE TABLE IF NOT EXISTS `sesiones` (
  `iduser` int(10) NOT NULL,
  `sesion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `grupo` (`codigo`, `nombre`, `grado`) VALUES
('GRP1A', 'A',1),
('GRP2A', 'A',2),
('GRP3A', 'A',3),
('GRP4A', 'A',4),
('GRP5A', 'A',5),
('GRP6A', 'A',6),
('GRP7A', 'A',7),
('GRP8A', 'A',8),
('GRP9A', 'A',9);

INSERT INTO `plan` (`id`, `modalidad`) VALUES (1, 'Semestral'), (2, 'Cuatrimestral'), (3, 'Corporativo');

INSERT INTO `carrera` (`id`, `nombre`, `director`, `coordinador`)
VALUES (NULL, 'Lic. en Telecomunicaciones', 'Bernardo Gómez Díaz', 'Luis Montes'),
(NULL, 'Lic. en Comercio Electrónico', 'Jorge Castañeda', 'Mariana Cruz'),
(NULL, 'Lic. en Informática Administrativa', 'Gabriel Solís', 'Sofía Cruz'),
(NULL, 'Lic. en Ingeniaría en Sistemas Computacionales', 'Luis Cruz', 'Brenda Corrales');

INSERT INTO `ciclo` (`id`, `descripcion`, `mes_inicio`, `mes_final`, `id_plan`) VALUES
(NULL, 'ENE/JUN', 1, 6, '1'),
(NULL, 'AGO/DIC', 8, 12, '1'),
(NULL, 'ENE/ABR', 1, 4, '2'),
(NULL, 'MAY/AGO', 5, 8, '2'),
(NULL, 'SEP/DIC', 9, 12, '2'),
(NULL, 'ENE/ABR', 1, 4, '3'),
(NULL, 'MAY/AGO', 5, 8, '3'),
(NULL, 'SEP/DIC', 9, 12, '3');

INSERT INTO `infraestructura` (`id`, `tipo`, `edo`) VALUES (NULL, 'Talleres', '1'), (NULL, 'Laboratorios', '1'), (NULL, 'Equipos de cómputa', '1'), (NULL, 'Licencias', '1'), (NULL, 'Títulos', '1'), (NULL, 'Ejemplares', '1'), (NULL, 'Libros prestados', '1'), (NULL, 'Recursos digitales', '1');

INSERT INTO `caracteristicas` (`id`,`nombre`, `asignado`) VALUES
(1,'Plaza Base',1),
(2,'Discapacidad',2),
(3,'Doctorado',1),
(4,'Maestría',1),
(5,'Especialidad',1),
(6,'Licenciatura',1),
(7,'Técnico Superior Universitario',1),
(8,'Otro grado de estudios',1),
(9,'Participación en SNI',1),
(10,'P. de Carrera con Formación Profesional en la docencia',1),
(11,'P. de Asignatura con Formación Profesional en la docencia',1),
(12,'P. de Carrera con Participación en tareas de gestión académica',1),
(13,'P. de Asignatura con Participación en tareas de gestión académica',1),
(14,'Movilidad Nacional',1),
(15,'Movilidad Internacional',1),
(16,'Trabaja simultánemente a sus estudios',0),
(17,'Labora en actividades afines a estudios',0),
(18,'Part. en Activ. de Serv. a la Comunidad',0),
(19,'Part. en Actividades de Emprendedurismo',0);

INSERT INTO `estado` (`id`, `nombre`) VALUES
(1, 'Aguascalientes'),
(2, 'Baja California'),
(3, 'Baja California Sur'),
(4, 'Campeche'),
(5, 'Coahuila'),
(6, 'Colima'),
(7, 'Chiapas'),
(8, 'Chihuahua'),
(9, 'Cd de México (CDMX)'),
(10, 'Durango'),
(11, 'Guanajuato'),
(12, 'Guerrero'),
(13, 'Hidalgo'),
(14, 'Jalisco'),
(15, 'México'),
(16, 'Michoacán'),
(17, 'Morelos'),
(18, 'Nayarit'),
(19, 'Nuevo León'),
(20, 'Oaxaca'),
(21, 'Puebla'),
(22, 'Querétaro'),
(23, 'Quintana Roo'),
(24, 'San Luis Potosí'),
(25, 'Sinaloa'),
(26, 'Sonora'),
(27, 'Tabasco'),
(28, 'Tamaulipas'),
(29, 'Tlaxcala'),
(30, 'Veracruz'),
(31, 'Yucatán'),
(32, 'Zacatecas');



INSERT INTO `municipio` (`id`, `id_estado`, `nombre`) VALUES
(81, 7, 'Acacoyagua'),
(82, 7, 'Acala'),
(83, 7, 'Acapetahua'),
(84, 7, 'Altamirano'),
(85, 7, 'Amatán'),
(86, 7, 'Amatenango de la Frontera'),
(87, 7, 'Amatenango del Valle'),
(88, 7, 'Angel Albino Corzo'),
(89, 7, 'Arriaga'),
(90, 7, 'Bejucal de Ocampo'),
(91, 7, 'Bella Vista'),
(92, 7, 'Berriozábal'),
(93, 7, 'Bochil'),
(94, 7, 'El Bosque'),
(95, 7, 'Cacahoatán'),
(96, 7, 'Catazajá'),
(97, 7, 'Cintalapa'),
(98, 7, 'Coapilla'),
(99, 7, 'Comitán de Domínguez'),
(100, 7, 'La Concordia'),
(101, 7, 'Copainalá'),
(102, 7, 'Chalchihuitán'),
(103, 7, 'Chamula'),
(104, 7, 'Chanal'),
(105, 7, 'Chapultenango'),
(106, 7, 'Chenalhó'),
(107, 7, 'Chiapa de Corzo'),
(108, 7, 'Chiapilla'),
(109, 7, 'Chicoasén'),
(110, 7, 'Chicomuselo'),
(111, 7, 'Chilón'),
(112, 7, 'Escuintla'),
(113, 7, 'Francisco León'),
(114, 7, 'Frontera Comalapa'),
(115, 7, 'Frontera Hidalgo'),
(116, 7, 'La Grandeza'),
(117, 7, 'Huehuetán'),
(118, 7, 'Huixtán'),
(119, 7, 'Huitiupán'),
(120, 7, 'Huixtla'),
(121, 7, 'La Independencia'),
(122, 7, 'Ixhuatán'),
(123, 7, 'Ixtacomitán'),
(124, 7, 'Ixtapa'),
(125, 7, 'Ixtapangajoya'),
(126, 7, 'Jiquipilas'),
(127, 7, 'Jitotol'),
(128, 7, 'Juárez'),
(129, 7, 'Larráinzar'),
(130, 7, 'La Libertad'),
(131, 7, 'Mapastepec'),
(132, 7, 'Las Margaritas'),
(133, 7, 'Mazapa de Madero'),
(134, 7, 'Mazatán'),
(135, 7, 'Metapa'),
(136, 7, 'Mitontic'),
(137, 7, 'Motozintla'),
(138, 7, 'Nicolás Ruíz'),
(139, 7, 'Ocosingo'),
(140, 7, 'Ocotepec'),
(141, 7, 'Ocozocoautla de Espinosa'),
(142, 7, 'Ostuacán'),
(143, 7, 'Osumacinta'),
(144, 7, 'Oxchuc'),
(145, 7, 'Palenque'),
(146, 7, 'Pantelhó'),
(147, 7, 'Pantepec'),
(148, 7, 'Pichucalco'),
(149, 7, 'Pijijiapan'),
(150, 7, 'El Porvenir'),
(151, 7, 'Villa Comaltitlán'),
(152, 7, 'Pueblo Nuevo Solistahuacán'),
(153, 7, 'Rayón'),
(154, 7, 'Reforma'),
(155, 7, 'Las Rosas'),
(156, 7, 'Sabanilla'),
(157, 7, 'Salto de Agua'),
(158, 7, 'San Cristóbal de las Casas'),
(159, 7, 'San Fernando'),
(160, 7, 'Siltepec'),
(161, 7, 'Simojovel'),
(162, 7, 'Sitalá'),
(163, 7, 'Socoltenango'),
(164, 7, 'Solosuchiapa'),
(165, 7, 'Soyaló'),
(166, 7, 'Suchiapa'),
(167, 7, 'Suchiate'),
(168, 7, 'Sunuapa'),
(169, 7, 'Tapachula'),
(170, 7, 'Tapalapa'),
(171, 7, 'Tapilula'),
(172, 7, 'Tecpatán'),
(173, 7, 'Tenejapa'),
(174, 7, 'Teopisca'),
(175, 7, 'Tila'),
(176, 7, 'Tonalá'),
(177, 7, 'Totolapa'),
(178, 7, 'La Trinitaria'),
(179, 7, 'Tumbalá'),
(180, 7, 'Tuxtla Gutiérrez'),
(181, 7, 'Tuxtla Chico'),
(182, 7, 'Tuzantán'),
(183, 7, 'Tzimol'),
(184, 7, 'Unión Juárez'),
(185, 7, 'Venustiano Carranza'),
(186, 7, 'Villa Corzo'),
(187, 7, 'Villaflores'),
(188, 7, 'Yajalón'),
(189, 7, 'San Lucas'),
(190, 7, 'Zinacantán'),
(191, 7, 'San Juan Cancuc'),
(192, 7, 'Aldama'),
(193, 7, 'Benemérito de las Américas'),
(194, 7, 'Maravilla Tenejapa'),
(195, 7, 'Marqués de Comillas'),
(196, 7, 'Montecristo de Guerrero'),
(197, 7, 'San Andrés Duraznal'),
(198, 7, 'Santiago el Pinar');
