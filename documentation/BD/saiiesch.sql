-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-07-2018 a las 18:22:50
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `saiiesch`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE IF NOT EXISTS `administrador` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `tipo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`id`, `username`, `password`, `nombre`, `tipo`) VALUES
(1, 'admin', 'c893bad68927b457dbed39460e6afd62', 'Administrador', 0),
(2, 'personal1', 'a039dc98d877db12416fa4989507205c', 'Personal Uno', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE IF NOT EXISTS `alumno` (
  `nocontrol` varchar(12) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `paterno` varchar(50) NOT NULL,
  `materno` varchar(50) NOT NULL,
  `edo` tinyint(1) NOT NULL,
  `curp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nacionalidad` varchar(30) NOT NULL,
  `genero` tinyint(1) NOT NULL,
  `nacimiento` date NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `direccion` varchar(140) NOT NULL,
  `ciudad` varchar(60) NOT NULL,
  `municipio` varchar(60) NOT NULL,
  `id_estado` int(10) unsigned NOT NULL,
  PRIMARY KEY (`nocontrol`),
  KEY `id_estado` (`id_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`nocontrol`, `nombre`, `paterno`, `materno`, `edo`, `curp`, `email`, `nacionalidad`, `genero`, `nacimiento`, `telefono`, `direccion`, `ciudad`, `municipio`, `id_estado`) VALUES
('5675657', 'Oscar', 'Copaza', 'Castillo', 1, 'ut75757657', 'oscar@live.com.mx', 'mexicana', 1, '1980-10-15', '6786786868', 'Conocido', 'Tuxtla', 'Tuxtla', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caracteristicas`
--

CREATE TABLE IF NOT EXISTS `caracteristicas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(40) NOT NULL,
  `asignado` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `caracteristicas`
--

INSERT INTO `caracteristicas` (`id`, `nombre`, `asignado`) VALUES
(1, 'Plaza Base', 1),
(2, 'Discapacidad', 2),
(3, 'Doctorado', 1),
(4, 'Maestría', 1),
(5, 'Especialidad', 1),
(6, 'Licenciatura', 1),
(7, 'Técnico Superior Universitario', 1),
(8, 'Otro grado de estudios', 1),
(9, 'Participación en SNI', 1),
(10, 'P. de Carrera con Formación Profesional ', 1),
(11, 'P. de Asignatura con Formación Profesion', 1),
(12, 'P. de Carrera con Participación en tarea', 1),
(13, 'P. de Asignatura con Participación en ta', 1),
(14, 'Movilidad Nacional', 1),
(15, 'Movilidad Internacional', 1),
(16, 'Trabaja simultánemente a sus estudios', 0),
(17, 'Labora en actividades afines a estudios', 0),
(18, 'Part. en Activ. de Serv. a la Comunidad', 0),
(19, 'Part. en Actividades de Emprendedurismo', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrera`
--

CREATE TABLE IF NOT EXISTS `carrera` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `director` varchar(100) NOT NULL,
  `coordinador` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `carrera`
--

INSERT INTO `carrera` (`id`, `nombre`, `director`, `coordinador`) VALUES
(1, 'Lic. en Telecomunicaciones', 'Bernardo Gómez Díaz', 'Luis Montes'),
(2, 'Lic. en Comercio Electrónico', 'Jorge Castañeda', 'Mariana Cruz'),
(3, 'Lic. en Informática Administrativa', 'Gabriel Solís', 'Sofía Cruz'),
(4, 'Lic. en Ingeniaría en Sistemas Computacionales', 'Luis Cruz', 'Brenda Corrales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciclo`
--

CREATE TABLE IF NOT EXISTS `ciclo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  `mes_inicio` tinyint(2) NOT NULL,
  `mes_final` tinyint(2) NOT NULL,
  `id_plan` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_plan` (`id_plan`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `ciclo`
--

INSERT INTO `ciclo` (`id`, `descripcion`, `mes_inicio`, `mes_final`, `id_plan`) VALUES
(1, 'ENE/JUN', 1, 6, 1),
(2, 'AGO/DIC', 8, 12, 1),
(3, 'ENE/ABR', 1, 4, 2),
(4, 'MAY/AGO', 5, 8, 2),
(5, 'SEP/DIC', 9, 12, 2),
(6, 'ENE/ABR', 1, 4, 3),
(7, 'MAY/AGO', 5, 8, 3),
(8, 'SEP/DIC', 9, 12, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `des_alumno_caracteristica`
--

CREATE TABLE IF NOT EXISTS `des_alumno_caracteristica` (
  `descripcion` varchar(12) NOT NULL,
  `nocontrol_alumno` varchar(12) NOT NULL,
  `id_caracteristica` int(10) unsigned NOT NULL,
  `fecha` varchar(12) NOT NULL,
  `valor` tinyint(1) NOT NULL,
  PRIMARY KEY (`nocontrol_alumno`,`id_caracteristica`,`fecha`),
  KEY `id_caracteristica` (`id_caracteristica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `des_alumno_caracteristica`
--

INSERT INTO `des_alumno_caracteristica` (`descripcion`, `nocontrol_alumno`, `id_caracteristica`, `fecha`, `valor`) VALUES
('', '5675657', 2, '1483225200', 0),
('', '5675657', 16, '1483225200', 0),
('', '5675657', 17, '1483225200', 0),
('', '5675657', 18, '1483225200', 0),
('', '5675657', 19, '1483225200', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `des_docente_caracteristica`
--

CREATE TABLE IF NOT EXISTS `des_docente_caracteristica` (
  `descripcion` varchar(12) NOT NULL,
  `nocontrol_docente` varchar(12) NOT NULL,
  `id_caracteristica` int(10) unsigned NOT NULL,
  `fecha` varchar(12) NOT NULL,
  `valor` tinyint(1) NOT NULL,
  PRIMARY KEY (`nocontrol_docente`,`id_caracteristica`,`fecha`),
  KEY `id_caracteristica` (`id_caracteristica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `docente`
--

CREATE TABLE IF NOT EXISTS `docente` (
  `nocontrol` varchar(12) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `paterno` varchar(50) NOT NULL,
  `materno` varchar(50) NOT NULL,
  `edo` tinyint(1) NOT NULL,
  `curp` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `nacionalidad` varchar(30) NOT NULL,
  `genero` tinyint(1) NOT NULL,
  `nacimiento` date NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `ingreso` date NOT NULL,
  `direccion` varchar(140) NOT NULL,
  `ciudad` varchar(60) NOT NULL,
  `municipio` varchar(60) NOT NULL,
  `id_estado` int(10) unsigned NOT NULL,
  PRIMARY KEY (`nocontrol`),
  KEY `id_estado` (`id_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado`
--

CREATE TABLE IF NOT EXISTS `estado` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Volcado de datos para la tabla `estado`
--

INSERT INTO `estado` (`id`, `nombre`) VALUES
(1, 'Aguascalientes'),
(2, 'Baja California'),
(3, 'Baja California Sur'),
(4, 'Campeche'),
(5, 'Coahuila'),
(6, 'Colima'),
(7, 'Chiapas'),
(8, 'Chihuahua'),
(9, 'Cd de México (CDMX)'),
(10, 'Durango'),
(11, 'Guanajuato'),
(12, 'Guerrero'),
(13, 'Hidalgo'),
(14, 'Jalisco'),
(15, 'México'),
(16, 'Michoacán'),
(17, 'Morelos'),
(18, 'Nayarit'),
(19, 'Nuevo León'),
(20, 'Oaxaca'),
(21, 'Puebla'),
(22, 'Querétaro'),
(23, 'Quintana Roo'),
(24, 'San Luis Potosí'),
(25, 'Sinaloa'),
(26, 'Sonora'),
(27, 'Tabasco'),
(28, 'Tamaulipas'),
(29, 'Tlaxcala'),
(30, 'Veracruz'),
(31, 'Yucatán'),
(32, 'Zacatecas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo`
--

CREATE TABLE IF NOT EXISTS `grupo` (
  `codigo` varchar(12) NOT NULL,
  `nombre` varchar(2) NOT NULL,
  `grado` int(2) NOT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `grupo`
--

INSERT INTO `grupo` (`codigo`, `nombre`, `grado`) VALUES
('GRP1A', 'A', 1),
('GRP2A', 'A', 2),
('GRP3A', 'A', 3),
('GRP4A', 'A', 4),
('GRP5A', 'A', 5),
('GRP6A', 'A', 6),
('GRP7A', 'A', 7),
('GRP8A', 'A', 8),
('GRP9A', 'A', 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historico_alumno`
--

CREATE TABLE IF NOT EXISTS `historico_alumno` (
  `id_programa` int(10) unsigned NOT NULL,
  `codigo_grupo` varchar(12) NOT NULL,
  `nocontrol_alumno` varchar(12) NOT NULL,
  `tipo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_programa`,`nocontrol_alumno`),
  KEY `codigo_grupo` (`codigo_grupo`),
  KEY `nocontrol_alumno` (`nocontrol_alumno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `historico_alumno`
--

INSERT INTO `historico_alumno` (`id_programa`, `codigo_grupo`, `nocontrol_alumno`, `tipo`) VALUES
(1, 'GRP1A', '5675657', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historico_docente`
--

CREATE TABLE IF NOT EXISTS `historico_docente` (
  `id_programa` int(10) unsigned NOT NULL,
  `nocontrol_docente` varchar(12) NOT NULL,
  PRIMARY KEY (`id_programa`,`nocontrol_docente`),
  KEY `nocontrol_docente` (`nocontrol_docente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infraestructura`
--

CREATE TABLE IF NOT EXISTS `infraestructura` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(100) NOT NULL,
  `edo` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `infraestructura`
--

INSERT INTO `infraestructura` (`id`, `tipo`, `edo`) VALUES
(1, 'Talleres', 1),
(2, 'Laboratorios', 1),
(3, 'Equipos de cómputa', 1),
(4, 'Licencias', 1),
(5, 'Títulos', 1),
(6, 'Ejemplares', 1),
(7, 'Libros prestados', 1),
(8, 'Recursos digitales', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `infraestructura_programa`
--

CREATE TABLE IF NOT EXISTS `infraestructura_programa` (
  `cantidad` tinyint(3) NOT NULL,
  `id_infraestructura` int(10) unsigned NOT NULL,
  `id_programa` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_infraestructura`,`id_programa`),
  KEY `id_programa` (`id_programa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE IF NOT EXISTS `municipio` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `id_estado` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_estado` (`id_estado`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=199 ;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`id`, `nombre`, `id_estado`) VALUES
(81, 'Acacoyagua', 7),
(82, 'Acala', 7),
(83, 'Acapetahua', 7),
(84, 'Altamirano', 7),
(85, 'Amatán', 7),
(86, 'Amatenango de la Frontera', 7),
(87, 'Amatenango del Valle', 7),
(88, 'Angel Albino Corzo', 7),
(89, 'Arriaga', 7),
(90, 'Bejucal de Ocampo', 7),
(91, 'Bella Vista', 7),
(92, 'Berriozábal', 7),
(93, 'Bochil', 7),
(94, 'El Bosque', 7),
(95, 'Cacahoatán', 7),
(96, 'Catazajá', 7),
(97, 'Cintalapa', 7),
(98, 'Coapilla', 7),
(99, 'Comitán de Domínguez', 7),
(100, 'La Concordia', 7),
(101, 'Copainalá', 7),
(102, 'Chalchihuitán', 7),
(103, 'Chamula', 7),
(104, 'Chanal', 7),
(105, 'Chapultenango', 7),
(106, 'Chenalhó', 7),
(107, 'Chiapa de Corzo', 7),
(108, 'Chiapilla', 7),
(109, 'Chicoasén', 7),
(110, 'Chicomuselo', 7),
(111, 'Chilón', 7),
(112, 'Escuintla', 7),
(113, 'Francisco León', 7),
(114, 'Frontera Comalapa', 7),
(115, 'Frontera Hidalgo', 7),
(116, 'La Grandeza', 7),
(117, 'Huehuetán', 7),
(118, 'Huixtán', 7),
(119, 'Huitiupán', 7),
(120, 'Huixtla', 7),
(121, 'La Independencia', 7),
(122, 'Ixhuatán', 7),
(123, 'Ixtacomitán', 7),
(124, 'Ixtapa', 7),
(125, 'Ixtapangajoya', 7),
(126, 'Jiquipilas', 7),
(127, 'Jitotol', 7),
(128, 'Juárez', 7),
(129, 'Larráinzar', 7),
(130, 'La Libertad', 7),
(131, 'Mapastepec', 7),
(132, 'Las Margaritas', 7),
(133, 'Mazapa de Madero', 7),
(134, 'Mazatán', 7),
(135, 'Metapa', 7),
(136, 'Mitontic', 7),
(137, 'Motozintla', 7),
(138, 'Nicolás Ruíz', 7),
(139, 'Ocosingo', 7),
(140, 'Ocotepec', 7),
(141, 'Ocozocoautla de Espinosa', 7),
(142, 'Ostuacán', 7),
(143, 'Osumacinta', 7),
(144, 'Oxchuc', 7),
(145, 'Palenque', 7),
(146, 'Pantelhó', 7),
(147, 'Pantepec', 7),
(148, 'Pichucalco', 7),
(149, 'Pijijiapan', 7),
(150, 'El Porvenir', 7),
(151, 'Villa Comaltitlán', 7),
(152, 'Pueblo Nuevo Solistahuacán', 7),
(153, 'Rayón', 7),
(154, 'Reforma', 7),
(155, 'Las Rosas', 7),
(156, 'Sabanilla', 7),
(157, 'Salto de Agua', 7),
(158, 'San Cristóbal de las Casas', 7),
(159, 'San Fernando', 7),
(160, 'Siltepec', 7),
(161, 'Simojovel', 7),
(162, 'Sitalá', 7),
(163, 'Socoltenango', 7),
(164, 'Solosuchiapa', 7),
(165, 'Soyaló', 7),
(166, 'Suchiapa', 7),
(167, 'Suchiate', 7),
(168, 'Sunuapa', 7),
(169, 'Tapachula', 7),
(170, 'Tapalapa', 7),
(171, 'Tapilula', 7),
(172, 'Tecpatán', 7),
(173, 'Tenejapa', 7),
(174, 'Teopisca', 7),
(175, 'Tila', 7),
(176, 'Tonalá', 7),
(177, 'Totolapa', 7),
(178, 'La Trinitaria', 7),
(179, 'Tumbalá', 7),
(180, 'Tuxtla Gutiérrez', 7),
(181, 'Tuxtla Chico', 7),
(182, 'Tuzantán', 7),
(183, 'Tzimol', 7),
(184, 'Unión Juárez', 7),
(185, 'Venustiano Carranza', 7),
(186, 'Villa Corzo', 7),
(187, 'Villaflores', 7),
(188, 'Yajalón', 7),
(189, 'San Lucas', 7),
(190, 'Zinacantán', 7),
(191, 'San Juan Cancuc', 7),
(192, 'Aldama', 7),
(193, 'Benemérito de las Américas', 7),
(194, 'Maravilla Tenejapa', 7),
(195, 'Marqués de Comillas', 7),
(196, 'Montecristo de Guerrero', 7),
(197, 'San Andrés Duraznal', 7),
(198, 'Santiago el Pinar', 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `plan`
--

CREATE TABLE IF NOT EXISTS `plan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `modalidad` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `plan`
--

INSERT INTO `plan` (`id`, `modalidad`) VALUES
(1, 'Semestral'),
(2, 'Cuatrimestral'),
(3, 'Corporativo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programa`
--

CREATE TABLE IF NOT EXISTS `programa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `year` int(4) NOT NULL,
  `id_ciclo` int(10) unsigned NOT NULL,
  `id_carrera` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `thekey` (`year`,`id_ciclo`,`id_carrera`),
  KEY `id_carrera` (`id_carrera`),
  KEY `id_ciclo` (`id_ciclo`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `programa`
--

INSERT INTO `programa` (`id`, `year`, `id_ciclo`, `id_carrera`) VALUES
(1, 2017, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sesiones`
--

CREATE TABLE IF NOT EXISTS `sesiones` (
  `iduser` int(10) NOT NULL,
  `sesion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `sesiones`
--

INSERT INTO `sesiones` (`iduser`, `sesion`, `fecha`) VALUES
(1, 'oa4p7s04rae5v3sc2kd16kfk27', '2018-07-26 11:55:52');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD CONSTRAINT `alumno_ibfk_1` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id`);

--
-- Filtros para la tabla `ciclo`
--
ALTER TABLE `ciclo`
  ADD CONSTRAINT `ciclo_ibfk_1` FOREIGN KEY (`id_plan`) REFERENCES `plan` (`id`);

--
-- Filtros para la tabla `des_alumno_caracteristica`
--
ALTER TABLE `des_alumno_caracteristica`
  ADD CONSTRAINT `des_alumno_caracteristica_ibfk_1` FOREIGN KEY (`nocontrol_alumno`) REFERENCES `alumno` (`nocontrol`),
  ADD CONSTRAINT `des_alumno_caracteristica_ibfk_2` FOREIGN KEY (`id_caracteristica`) REFERENCES `caracteristicas` (`id`);

--
-- Filtros para la tabla `des_docente_caracteristica`
--
ALTER TABLE `des_docente_caracteristica`
  ADD CONSTRAINT `des_docente_caracteristica_ibfk_1` FOREIGN KEY (`nocontrol_docente`) REFERENCES `docente` (`nocontrol`),
  ADD CONSTRAINT `des_docente_caracteristica_ibfk_2` FOREIGN KEY (`id_caracteristica`) REFERENCES `caracteristicas` (`id`);

--
-- Filtros para la tabla `docente`
--
ALTER TABLE `docente`
  ADD CONSTRAINT `docente_ibfk_1` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id`);

--
-- Filtros para la tabla `historico_alumno`
--
ALTER TABLE `historico_alumno`
  ADD CONSTRAINT `historico_alumno_ibfk_1` FOREIGN KEY (`id_programa`) REFERENCES `programa` (`id`),
  ADD CONSTRAINT `historico_alumno_ibfk_2` FOREIGN KEY (`codigo_grupo`) REFERENCES `grupo` (`codigo`),
  ADD CONSTRAINT `historico_alumno_ibfk_3` FOREIGN KEY (`nocontrol_alumno`) REFERENCES `alumno` (`nocontrol`);

--
-- Filtros para la tabla `historico_docente`
--
ALTER TABLE `historico_docente`
  ADD CONSTRAINT `historico_docente_ibfk_1` FOREIGN KEY (`id_programa`) REFERENCES `programa` (`id`),
  ADD CONSTRAINT `historico_docente_ibfk_2` FOREIGN KEY (`nocontrol_docente`) REFERENCES `docente` (`nocontrol`);

--
-- Filtros para la tabla `infraestructura_programa`
--
ALTER TABLE `infraestructura_programa`
  ADD CONSTRAINT `infraestructura_programa_ibfk_1` FOREIGN KEY (`id_infraestructura`) REFERENCES `infraestructura` (`id`),
  ADD CONSTRAINT `infraestructura_programa_ibfk_2` FOREIGN KEY (`id_programa`) REFERENCES `programa` (`id`);

--
-- Filtros para la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD CONSTRAINT `municipio_ibfk_1` FOREIGN KEY (`id_estado`) REFERENCES `estado` (`id`);

--
-- Filtros para la tabla `programa`
--
ALTER TABLE `programa`
  ADD CONSTRAINT `programa_ibfk_1` FOREIGN KEY (`id_carrera`) REFERENCES `carrera` (`id`),
  ADD CONSTRAINT `programa_ibfk_2` FOREIGN KEY (`id_ciclo`) REFERENCES `ciclo` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
