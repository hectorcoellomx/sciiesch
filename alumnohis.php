<?php
session_start();
$id_sesion = session_id();
$mod = array("carrera", "plan","ciclo","estado","municipio","persona","alumno","programa","grupo","historico_alumno","caracteristicas", "desglose_caracteristicas_alumno");

include ('app/modulos.php');
include ('app/sesion.php');

$bygrupos=false;
$lista_his_count=0;
$lista_his_total_count=0;
$lista_his= "";
$lista_his_total= "";

if(isset($_GET["id"])){
  if(trim($_GET["id"])!=""){
    if(!isset($_GET["grupo"])){
        $lista_his_total= historico_alumno_lista_programa($_GET["id"]);
        $lista_his_total_count= count($lista_his_total);
        $lista_grupos = grupo_lista();
    }
    else{
        $lista_his_total= historico_alumno_lista_programa($_GET["id"]);
        $lista_his_total_count= count($lista_his_total);

        $lista_his= historico_alumno_lista_programa_grupo($_GET["id"],$_GET["grupo"]);
        $lista_his_count= count($lista_his);
        $gruposel = grupo($_GET["grupo"]);
        $bygrupos=true;
    }

    $cabecera = programa($_GET["id"]);
    if($cabecera['id']==""){
      header("Location: infraestructurahis.php");
    }
  }
}


$totalc=count(caracteristicas_lista_alumnos());

?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=2; include 'inc/header.php'; ?>

    <section id="principal">

      <div class="container">
        <div class="row">
          <div class="col-md-10">

          <?php
            $lista_carreras= programa_carreras(); //$lista_carreras=carrera_lista();
            include ('inc/buscador.php');
          ?>

          <br>
          <?php

           if(isset($_GET["id"]) && trim($_GET["id"])!=""){

            echo "<div class='cabecera'>" . $cabecera['carrera']['nombre']  . " - " . $cabecera['ciclo']['plan']['modalidad'] . " (" . $cabecera['ciclo']['descripcion'] .  " " . $cabecera['year'] . ")</div>";

              echo "<div>";
                echo "<div>" . $lista_his_total_count . " alumno(s) registrado(s)</div><br>";

                /*if($lista_his_count>0 || $lista_his_total_count>0){
                  echo "<a class='boton btnmin' href='./reporte/alumno.php?id=" . $_GET["id"] . "' target='blank'>Rep. General</a>";
                  echo "<a class='boton btnmin' href='./reporte/alumno_nvoingreso.php?id=" . $_GET["id"] . "' target='blank'>N. Ingreso</a>";
                  echo "<a class='boton btnmin' href='./reporte/alumno_remediales.php?id=" . $_GET["id"] . "' target='blank'>C. Remediales</a>";
                  echo "<a class='boton btnmin' href='./reporte/alumno_caracteristica.php?id=" . $_GET["id"] . "&tipo=14' target='blank'>Movilidad</a>";
                  echo "<a class='boton btnmin' href='./reporte/alumno_caracteristica.php?id=" . $_GET["id"] . "&tipo=16' target='blank'>Trabajan</a>";
                  echo "<a class='boton btnmin' href='./reporte/alumno_caracteristica.php?id=" . $_GET["id"] . "&tipo=17' target='blank'>A. Afines</a>";
                  echo "<a class='boton btnmin' href='./reporte/alumno_caracteristica.php?id=" . $_GET["id"] . "&tipo=18' target='blank'>S. Comunidad</a>";
                  echo "<a class='boton btnmin' href='./reporte/alumno_caracteristica.php?id=" . $_GET["id"] . "&tipo=19' target='blank'>Emprendedurismo</a>";
                  echo "<a class='boton btnmin' href='./reporte/alumno_ultimo.php?id=" . $_GET["id"] . "' target='blank'>Último Gdo.</a>";                   
                  }*/
                
               echo "</div>";


              if($bygrupos){
                 echo "<div id='area-grupos'><span>Seleccionado</span>";
                 echo "&nbsp;&nbsp;&nbsp;<strong>". $gruposel['grado'] . " Grado | Grupo " . $gruposel['nombre'] . "</strong>";
                 echo "&nbsp;&nbsp;".$lista_his_count . " alumno(s) registrado(s)";
                 echo "&nbsp;&nbsp;<a class='' href='alumnohis.php?id=" . $_GET["id"] . "'>[cerrar]</a>";
                 if($lista_his_count>0){
                    echo "<a class='boton btnmin btnright' onclick='loadWindow(500,210);loadScreens(\"inc/reinscribirsel.php?nocontrol=" . $_GET["id"] . "\",\"window\",this);' style='margin-top: 10px; margin-right: 10px;'>Reinscribir</a>";
                  }
                 echo "</div><br>";

                 echo "<a class='boton btnmin' href='alumnohisfrm.php?id=" . $_GET["id"] . "&grupo=".$_GET['grupo']."'>Agregar Alumno</a><br>";

               }else{

                ?>
                    
                    <form method="get">
                     Grupo:
                     <select name="grupo" style="width: 100px;">
                       <?php
                         $elegido="";
                         if(is_array($lista_grupos)){
                             foreach ($lista_grupos as $l) {
                              $sel="";
                              if($grupo_curso!="" && $grupo_curso==$l['codigo']){
                                $sel="selected";
                                $elegido= $l['grado'] . $l['nombre'];
                              }
                               echo '<option value="' . $l['codigo'] . '" ' . $sel . '>' . $l['grado'] . " - " . $l['nombre'] . '</option>';
                             }
                          }
                        ?>
                     </select>
                     <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>">
                     <input class="boton" type="submit" style="display: inline-block; width: 148px;" value="Seleccionar Grupo">
                    </form>

               <?php }

              ?>
            <br>
            <?php if(is_array($lista_his) && $bygrupos){ ?>
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="col-md-1">ID</th>
                 <th>Nombre</th>
                 <th class="col-md-1 acciones">Semestre</th>
                 <th class="col-md-1 acciones">Tipo</th>
                 <th class="col-md-1 acciones">Caract.</th>
                 <th class="col-md-1 acciones">Quitar</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $mes= ($cabecera['ciclo']['mes_final']<10) ? "0" . $cabecera['ciclo']['mes_final'] : $cabecera['ciclo']['mes_final'];
                  $fecha= "01-" . $mes . "-" . $cabecera['year'];
                  $color = " cbold";
                  $grpsel="";

                  foreach ($lista_his as $l) {

                      $totalcactivas= desglose_caracteristicas_check($l['alumno']['nocontrol'],strtotime($fecha));
                      $st= ($l['alumno']['edo']==0)? "des" : "";

                      if($l['grupo']['codigo']!=$grpsel){
                        if($color==" cnormal"){
                          $color=" cbold";
                        }else {
                          $color = " cnormal";
                        }

                      }



                      echo "<tr class='" . $st . $color . "'>";
                      echo "<td>" . $l['alumno']['nocontrol'] . "</td><td>" . $l['alumno']['nombre'] . " " . $l['alumno']['paterno'] . " " . $l['alumno']['materno'] . "</td>";
                      echo "<td class='acciones'><a href='alumnohis.php?id=" . $cabecera['id'] . "&grupo=".$l['grupo']['codigo'] . "'>" . $l['grupo']['grado'] . $l['grupo']['nombre'] . "</a></td>";
                      echo ($l['tipo']==0)? "<td>Normal</td>" : "<td>Remedial</td>";
                      echo "<td class='acciones'><a href='alumnocar.php?ciclo=" . $cabecera['ciclo']['id'] . "&year=".$cabecera['year']."&nocontrol=" . $l['alumno']['nocontrol'] . "&historial=".$cabecera['id']."'>(" . $totalcactivas . "/" . $totalc . ")</a></td>";
                      echo "<td class='acciones'>";
                      echo "<a href='alumnohisfrm.php?id=" . $cabecera['id'] . "&nocontrol=" . $l['alumno']['nocontrol'] . "&type=eliminar'><i class='fa fa-minus' aria-hidden='true'></i></a>";
                      echo "</td>";
                      echo "</tr>";

                      $grpsel=  $l['grupo']['codigo'];

                  }



                ?>
              </tbody>
            </table>
            <?php } } ?>
          </div>
          <div class="col-md-2">
              <div class="opciones">
                <span id="titulo">Alumnos</span>
                  <a class="boton" href="alumno.php">Ver lista</a>
              </div>
          
              <?php if(isset($_GET["id"]) && trim($_GET["id"])!=""){  ?>
                <br><br>
              <div class="opciones">
                <span id="titulo">Reportes</span>
                  <?php 

                  if($lista_his_count>0 || $lista_his_total_count>0){
                  echo "<a class='boton' href='./reporte/alumno.php?id=" . $_GET["id"] . "' target='blank'>General</a>";
                  echo "<a class='boton' href='./reporte/alumno_nvoingreso.php?id=" . $_GET["id"] . "' target='blank'>Nvo. Ingreso</a>";
                  echo "<a class='boton' href='./reporte/alumno_remediales.php?id=" . $_GET["id"] . "' target='blank'>C. Remediales</a>";
                  echo "<a class='boton' href='./reporte/alumno_caracteristica.php?id=" . $_GET["id"] . "&tipo=14' target='blank'>Movilidad</a>";
                  echo "<a class='boton' href='./reporte/alumno_caracteristica.php?id=" . $_GET["id"] . "&tipo=16' target='blank'>Trabajan</a>";
                  echo "<a class='boton' href='./reporte/alumno_caracteristica.php?id=" . $_GET["id"] . "&tipo=17' target='blank'>Act. Afines</a>";
                  echo "<a class='boton' href='./reporte/alumno_caracteristica.php?id=" . $_GET["id"] . "&tipo=18' target='blank'>Serv. Comun.</a>";
                  echo "<a class='boton' href='./reporte/alumno_caracteristica.php?id=" . $_GET["id"] . "&tipo=19' target='blank'>Emprendedur.</a>";
                  echo "<a class='boton' href='./reporte/alumno_ultimo.php?id=" . $_GET["id"] . "' target='blank'>Último Grado</a>";                   
                  }

                  ?>
              </div>

              <?php } ?>

          </div>
        </div>
      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
