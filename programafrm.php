<?php
session_start();
$id_sesion = session_id();
$mod = array("carrera", "plan","ciclo", "programa");
include ('app/modulos.php');
include ('app/sesion.php');

if($user['tipo']!=0){
  header('Location: programa.php');
}

 //POST
  if(isset($_POST["submit"])){

    if(!isset($_POST["update"])){
      $res=programa_nuevo();
      if($res=="done")
        header('Location: programa.php');
    }else{
      $res=programa_update();
      if($res=="done")
        header('Location: programa.php');
    }
  }

  // GET UPDATE OR CREATE
  $update=false;
  if(isset($_GET["type"]) && $_GET["type"]=="update"){
    if(isset($_GET["id"])){
      if($_GET["id"]!=""){
        $item=programa($_GET["id"]);
        $update=true;
      }
    }
  }

  $lista=ciclo_lista();

  $lista2=carrera_lista();

?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=5; include 'inc/header.php'; ?>

    <section id="principal">

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <form id="formulario" method="post" class="formulario">
              <div class="frmtitulo"><?php if($update){ echo "Actualizar programa"; } else { echo "Agregar programa"; } ?></div>
              <div class="fila row">
                <div class="tit col-md-4">
                    Año
                </div>
                <div class="dato col-md-8">
                    <select name="year">
                      <?php
                        $y=date("Y");
                        //echo "<input type='text' disabled value='".$y."'>";
                        //echo "<input type='hidden' name='year' value='".$y."'>";
                        while ($y>=2010) {
                          $sele="";
                          if($update && $item["year"]==$y){
                            $sele="selected";
                          }
                          echo "<option value='" . $y . "' " . $sele . ">" . $y . "</option>";
                          $y--;
                        }
                      ?>
                    </select>

                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Ciclo
                </div>
                <div class="dato col-md-8">
                    <select name="id_ciclo">
                    <?php
                      if(is_array($lista)){
                        foreach ($lista as $l) {
                          $sel="";
                          if($update && $item["ciclo"]["id"]==$l['id']){
                            $sel="selected";
                          }
                          echo '<option value="' . $l['id'] . '" ' . $sel . '>' . strtoupper($l['plan']['modalidad']) . ' (' . $l['descripcion'] . ')</option>';
                        }
                      }
                    ?>
                    </select>
                </div>
              </div>

              <div class="fila row">
                <div class="tit col-md-4">
                    Carrera
                </div>
                <div class="dato col-md-8">
                    <select name="id_carrera">
                    <?php
                      if(is_array($lista2)){
                        foreach ($lista2 as $l) {
                          $sel="";
                          if($update && $item["carrera"]["id"]==$l['id']){
                            $sel="selected";
                          }
                          echo '<option value="' . $l['id'] . '" ' . $sel . '>' . $l['nombre'] . '</option>';
                        }
                      }
                    ?>
                    </select>
                </div>
              </div>

               <?php if($update){ echo "<input type='hidden' name='id' value='" . $item["id"] . "'>"; } ?>
              <?php if($update){ echo "<input type='hidden' name='update' value='true'>"; } ?>

              <div class="botones">
                <input class="boton" type="submit" name="submit" value="Guardar">
                <a href='programa.php' class="boton">Cancelar</a>
              </div>

            </form>
          </div>
        </div>
      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
