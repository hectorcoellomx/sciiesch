
$(document).ready(function(){

	$("#fondo, #window .cerrar").click(function(){
		$("#fondo").hide();
		$("#window").hide();
    });

});

function loadScreens(file,content){
	$.ajax({
		url:file,
		success: function(e){
			$("#"+content).html(e);
		}
	})
}

function loadWindow(w,h){
	var x= -1*(w/2);
	var y= -1*(h/2);
	$("#fondo").show();
	$("#window").css("width", w + "px");
	$("#window").css("height", h + "px");
	$("#window").css("margin-top", y + "px");
	$("#window").css("margin-left", x + "px");
	$("#window").show();
}

function deleteItem(iditem,entidad,finaldest,id2){

	$.post("./app/"+entidad+"/"+entidad+".php", {id: iditem,id2: id2}, function(data, status){
        if(data=="done"){
        	window.location.href = "./" + finaldest;
        }else{
        	var deserror="";
        	switch(data) {
			    case "1451":
			        deserror="No se puede eliminar este elemento ya que se encuentra asociado a otro registro";
			        break;
			    default:
			       deserror= "Error: No." + data;
			}
			$("#fondo").hide();
			$("#window").hide();
        	alert(deserror)
        }
    });


}

function desactivarItem(iditem,entidad,finaldest,tipo){

	$.post("./app/"+entidad+"/"+entidad+".php", {id: iditem,tipo: tipo}, function(data, status){
        if(data=="done"){
        	window.location.href = "./" + finaldest;
        }else{
        	var deserror="";
        	switch(data) {
			    case "1451":
			        deserror="No se puede eliminar este elemento ya que se encuentra asociado a otro registro";
			        break;
			    default:
			       deserror= "Error: No." + data;
			}
			$("#fondo").hide();
			$("#window").hide();
        	alert(deserror)
        }
    });


}

function cargarYear(id_carrera){
	$.post("./app/programa/listayear.php", {id: id_carrera}, function(data, status){
        if(data!="Error"){
        	$("#area_carreras").html(data);
        	$("#area_year").html("<select><option selected disabled>Seleccionar</option></select>");
        }else{
        	alert("Error: " + data)
        }
    });

}

function cargarCiclo(year,id_carrera){
	$.post("./app/programa/listaciclo.php", {year: year,idcarrera: id_carrera}, function(data, status){
        if(data!="Error"){
        	$("#area_year").html(data);
        }else{
        	alert("Error: " + data)
        }
    });
}

function verPrograma(id){
	window.location.href = "?id=" + id;
}

function irCiclo(id){
	var year = $("#year").val();
	var ciclo = $("#id_ciclo").val();
	var nocontrol = $("#nocontrol_docente").val();
	if(ciclo!=null && year!=null){
		window.location.href = "./docentecar.php?ciclo=" + ciclo + "&year=" + year + "&nocontrol=" + nocontrol;
	}
}

function irCicloAl(id){
	var year = $("#year").val();
	var ciclo = $("#id_ciclo").val();
	var nocontrol = $("#nocontrol_alumno").val();
	if(ciclo!=null && year!=null){
		window.location.href = "./alumnocar.php?ciclo=" + ciclo + "&year=" + year + "&nocontrol=" + nocontrol;
	}
}

function cargardocentes(tipo,id_programa){
	var cadena = $(".busqueda #cadena").val();
	if(cadena.trim()!="" && tipo!="all"){
		$.post("./app/docente/buscar.php", {palabra: cadena.trim(),id_programa:id_programa}, function(data, status){
	        if(data!="Error"){
	        	$("#docentebuscar").html(data);
	        	$("#filtro").html('Filtro: "' + cadena.trim() + '"');
	        }else{
	        	alert("Error: " + data);
	        }
	    });
	}
	if(tipo=="all" && cadena.trim()==""){
		$.post("./app/docente/buscar.php", {palabra: '',id_programa:id_programa}, function(data, status){
	        if(data!="Error"){
	        	$("#docentebuscar").html(data);
	        	$("#filtro").html('');
	        }else{
	        	alert("Error: " + data);
	        }
	    });
	}
}

function actualizar(fecha,nocontrol,id_caracteristica,id_ciclo){
	$("#car"+id_caracteristica).val("Procesando...");
	var valor= $("#valor"+id_caracteristica).val();
	var descripcion= $("#des"+id_caracteristica).val();
	
	$.post("./app/docente/actualizar_caracteristica.php", {valor: valor, descripcion: descripcion, fecha: fecha, nocontrol_docente: nocontrol, id_caracteristica: id_caracteristica, id_ciclo: id_ciclo}, function(data, status){
        if(data!="Error"){
			if(data!="Reintentar"){
				$("#car"+id_caracteristica).parent().parent().removeClass('op-agregar');
				$("#car"+id_caracteristica).parent().parent().addClass('op-actualizar');
				$("#car"+id_caracteristica).val("actualizar");
				$("#fc"+id_caracteristica).html(data);
			}else{
				$("#car"+id_caracteristica).val(data);
			}
        }else{
        	$("#car"+id_caracteristica).val("Reintentar");
        	alert("Error: " + data)
        }
    });
}

function actualizar_al(fecha,nocontrol,id_caracteristica,id_ciclo){
	$("#car"+id_caracteristica).val("Procesando...");
	var valor= $("#valor"+id_caracteristica).val();
	var descripcion= $("#des"+id_caracteristica).val();
	
	$.post("./app/alumno/actualizar_caracteristica.php", {valor: valor, descripcion: descripcion, fecha: fecha, nocontrol_alumno: nocontrol, id_caracteristica: id_caracteristica, id_ciclo: id_ciclo}, function(data, status){
        if(data!="Error"){
			if(data!="Reintentar"){
				$("#car"+id_caracteristica).parent().parent().removeClass('op-agregar');
				$("#car"+id_caracteristica).parent().parent().addClass('op-actualizar');
				$("#car"+id_caracteristica).val("actualizar");
				$("#fc"+id_caracteristica).html(data);
			}else{
				$("#car"+id_caracteristica).val(data);
			}
        }else{
        	$("#car"+id_caracteristica).val("Reintentar");
        	alert("Error: " + data)
        }
    });
}


function closeWindow(){
	$("#fondo").hide();
	$("#window").hide();
}
