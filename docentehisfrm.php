<?php
session_start();
$id_sesion = session_id();
$mod = array("carrera", "plan","ciclo","estado","municipio","persona","docente","programa","historico_docente");
include ('app/modulos.php');
include ('app/sesion.php');
// IS UPDATE
$update=false;
if(isset($_GET["type"]) && $_GET["type"]=="update"){
  if(isset($_GET["id"]) && isset($_GET["idi"])){
    if($_GET["id"]!=""){
      $update=true;
    }
  }
}else{

// IS NORMAL (Necesita ID)
 if(isset($_GET["id"])){
    if(trim($_GET["id"])==""){
      header("Location: docentehis.php");
    }
 }else{
    header("Location: docentehis.php");
 }

}

$yaexiste=false;

 //POST SUBMIT
  if(isset($_GET["type"])){

    if($_GET["type"]=="agregar"){ // FOR CREATE
      historico_docente_nuevo($_GET["id"],$_GET["nocontrol"]);
      header('Location: ?id='.$_GET["id"]);
    }elseif($_GET["type"]=="eliminar"){ // FOR UPDATE
      historico_docente_delete($_GET["id"],$_GET["nocontrol"]);
      header('Location: docentehis.php?id='.$_GET["id"]);
    }
  }


$lista=docente_lista_act();
$cabecera= programa(trim($_GET['id']));
if($cabecera['id']==""){
  header("Location: docentehis.php");
}
$lista_des= historico_docente_lista_programa($_GET["id"]);

?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=3; include 'inc/header.php'; ?>

    <section id="principal">

      <div class="container">
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6 center">

              <div class="frmtitulo"><?php if($update){ echo "Actualizar registro"; } else { echo "Agregar Docente"; } ?></div>

              <?php
                echo "<div>Agregar a " . $cabecera['carrera']['nombre']  . " - " . $cabecera['ciclo']['plan']['modalidad'] . " (" . $cabecera['ciclo']['descripcion'] .  " " . $cabecera['year'] . ")</div>";
                if($update)
                  echo "<b>" . $item['infraestructura']['tipo'] . "</b><br>";
               ?>
               <a href='docentehis.php?id=<?php echo $cabecera["id"]; ?>' class="boton" style="display:inline-block">Regresar</a>
               <div class="busqueda">
                Filtro:&nbsp;
                <input id="cadena" onkeyup="cargardocentes('all',<?php echo $cabecera['id']; ?>)" type="text" placeholder="Nombre o Apellidos"> <?php ?>
                <span style="display:inline-block" class="boton" onclick="cargardocentes('normal',<?php echo $cabecera['id']; ?>)">Buscar</span><span id="filtro"></span>
               </div>
              <div class="fila row">
                <div class="tit col-md-12" id="docentebuscar">


              <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="col-md-2">ID</th>
                 <th>Nombre</th>
                 <th class="col-md-1 acciones">Agregar</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  if(is_array($lista)){
                    foreach ($lista as $l) {
                      $quitar=false;
                      if(is_array($lista_des)){
                        foreach ($lista_des as $lis) {
                          if($lis['docente']['nocontrol']==$l['nocontrol'])
                            $quitar=true;
                        }
                      }
                      if(!$quitar){
                        $st="";
                        if($l['edo']==0){
                          $st="des";
                        }
                        echo "<tr class='" . $st . "'>";
                        echo "<td>" . $l['nocontrol'] . "</td><td>" . $l['nombre'] . " " . $l['paterno'] . " " . $l['materno'] . "</td>";
                        echo "<td class='acciones'>";
                        echo "<a href='?id=" . $cabecera['id'] . "&nocontrol=" . $l['nocontrol'] . "&type=agregar'><i class='fa fa-plus' aria-hidden='true'></i></a>";
                        echo "</td>";
                        echo "</tr>";
                      }
                    }
                  }
                ?>
                </tbody>
              </table>


              </div>
              </div>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>
    <?php if($yaexiste){
        echo "<script>alert('No se ha podido guardar el registo. Este dato ya existe registrado.');</script>";
      } ?>
  </body>
</html>
