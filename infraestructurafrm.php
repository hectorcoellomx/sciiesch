<?php
session_start();
$id_sesion = session_id();
$mod = array("infraestructura");
include ('app/modulos.php');
include ('app/sesion.php');
 
 //POST
  if(isset($_POST["submit"])){

    if(!isset($_POST["update"])){
      $res=infraestructura_nuevo();
      if($res=="done")
        header('Location: infraestructura.php');
    }else{
      $res=infraestructura_update();
      if($res=="done")
        header('Location: infraestructura.php');
    }
  }

  // GET UPDATE OR CREATE
  $update=false;
  if(isset($_GET["type"]) && $_GET["type"]=="update"){
    if(isset($_GET["id"])){
      if($_GET["id"]!=""){
        $item=infraestructura($_GET["id"]);
        $update=true;
      }
    }
  }

?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=4; include 'inc/header.php'; ?>
    
    <section id="principal">

      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <form id="formulario" method="post" class="formulario">
              <div class="frmtitulo"><?php if($update){ echo "Actualizar infraestructura"; } else { echo "Agregar infraestructura"; } ?></div>
              <div class="fila row">
                <div class="tit col-md-4">
                    Tipo
                </div>
                <div class="dato col-md-8">
                    <input type="text" name="tipo" required <?php if($update){ echo "value=" . $item["tipo"]; } ?>>
                </div>
              </div>
              <div class="fila row">
                <div class="tit col-md-4">
                    Status
                </div>
                <div class="dato col-md-8">
                    <select name="edo">
                      <option value="0" <?php if($update && $item["edo"]==0){ echo "selected";} ?>>Desactivado</option>
                      <option value="1" <?php if($update && $item["edo"]==1){ echo "selected";} if(!$update) { echo "selected"; } ?>>Activado</option>
                    </select>

                </div>
              </div>
               <?php if($update){ echo "<input type='hidden' name='id' value='" . $item["id"] . "'>"; } ?>
              <?php if($update){ echo "<input type='hidden' name='update' value='true'>"; } ?>

              <div class="botones">
                <input class="boton" type="submit" name="submit" value="Guardar">
                <a href='infraestructura.php' class="boton">Cancelar</a>
              </div>

            </form>
          </div>
        </div>
      </div>
    
    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
