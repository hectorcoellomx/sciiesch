<?php
session_start();
$id_sesion = session_id();
$mod = array("carrera", "plan","ciclo", "programa");
include ('app/modulos.php');
include ('app/sesion.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=5; include 'inc/header.php'; ?>
    
    <?php $lista=programa_lista(); ?>    

    <section id="principal">

      <div class="container">
        <?php rutas("programa"); ?>
        <div class="row">
          <div class="col-md-10">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th class="col-md-1">Id</th>
                  <th>Carrera</th>
                  <th>Año</th>
                  <th>Periodo</th>
                  <th>Modalidad</th>
                  <th class="col-md-1 acciones">Acción</th>
                </tr>
              </thead>
              <tbody>
                
                <?php 

                  if(is_array($lista)){

                  foreach ($lista as $l) {
                    echo "<tr>";
                    echo "<td>" . $l['id'] . "</td><td>" . $l['carrera']['nombre'] . "</td><td>" . $l['year'] . "</td><td>" . $l['ciclo']['descripcion'] . "</td><td>" . $l['ciclo']['plan']['modalidad'] . "</td>";
                    echo "<td class='acciones'>";
                    if($user['tipo']==0){
                      echo "<a href='programafrm.php?id=" . $l['id'] . "&type=update'><i class='fa fa-pencil' aria-hidden='true'></i></a>";
                      echo "<a onclick='loadWindow(500,175);loadScreens(\"inc/eliminar.php?id=" . $l['id'] . "&nombre=" . $l['year'] . "&entidad=programa&finaldest=programa.php\",\"window\",this);'><i class='fa fa-trash' aria-hidden='true'></i></a>";
                    } else{
                      echo "N/A";
                    }
                    echo "</td>";  
                    echo "</tr>";  
                  }

                }

                ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-2">
              <div class="opciones">
                <span id="titulo">Programa</span>
                    <?php if($user['tipo']==0){ ?>
                      <a class="boton" href="programafrm.php">Agregar</a>
                    <?php } ?>
                    <a class="boton" href="carrera.php">Carrera</a>
                    <a class="boton" href="plan.php">Plan</a>
                    <a class="boton" href="ciclo.php">Ciclo</a>
              </div>
          </div> 
        </div>
      </div>
    
    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
