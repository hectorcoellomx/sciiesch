<?php

$path="../";

$mod = array("carrera", "plan","ciclo","estado","municipio","persona","docente","programa","historico_docente","caracteristicas", "desglose_caracteristicas_docente");

include ('../app/modulos.php');

require('./fpdf/fpdf.php');
require('./fpdi/fpdi.php');

if(isset($_GET["id"])){
  if(trim($_GET["id"])!=""){
    $lista_his= historico_docente_lista_programa($_GET["id"]);
    $cabecera = programa($_GET["id"]);
    if($cabecera['id']==""){
      header("Location: docentehis.php");
    }
  }
}else{
  header("Location: ../docentehis.php");
}


$hombres=0;
$mujeres=0;
$De0a19=0;
$De20a24=0;
$De25a29=0;
$De30oMas=0;
$discapaciad=0;
$doctorado=0;
$maestria=0;
$especialidad=0;
$licenciatura=0;
$tecnico=0;
$otrogrado=0;
$extranjeros=0;

$maximo=false;

foreach ($lista_his as $l) {
  if($l['docente']['tipo']==1){
      $y=date("Y", strtotime($l['docente']['nacimiento']));
      $actual = $cabecera['year'];
      $edad= 1 * ($actual) - $y;
      
      if($edad<=19)
        $De0a19++;
      elseif($edad>=20 && $edad<=24)
        $De20a24++;
      elseif($edad>=25 && $edad<=29)
        $De25a29++;
      elseif($edad>=30)
        $De30oMas++;



      if($l['docente']['genero']==1)
        $hombres++;
      else
        $mujeres++;

      if($l['docente']['nacionalidad']=="extranjera")
        $extranjeros++;

      $dis=desglose_caracteristicas_docente($cabecera['ciclo']['id'],$cabecera['year'],$l['docente']['nocontrol'],1);

      $array_des = desglose_caracteristicas_docente_lista_check($cabecera['ciclo']['id'],$cabecera['year'],$l['docente']['nocontrol']);

      foreach ($array_des as $lis) {
        if($lis['caracteristicas']['id']==1){
          if($lis['valor']==1){
            $discapaciad++;
          }
        }
        elseif($lis['caracteristicas']['id']==2 && !$maximo){
          if($lis['valor']==1){
            $doctorado++;
            $maximo=true;
          }
        }
        elseif($lis['caracteristicas']['id']==3 && !$maximo){
          if($lis['valor']==1){
            $maestria++;
            $maximo=true;
          }
        }
        elseif($lis['caracteristicas']['id']==4 && !$maximo){
          if($lis['valor']==1){
            $especialidad++;
            $maximo=true;
          }
        }
        elseif($lis['caracteristicas']['id']==5 && !$maximo){
          if($lis['valor']==1){
            $licenciatura++;
            $maximo=true;
          }
        }
        elseif($lis['caracteristicas']['id']==6 && !$maximo){
          if($lis['valor']==1){
            $tecnico++;
            $maximo=true;
          }
        }
        elseif($lis['caracteristicas']['id']==7 && !$maximo){
          if($lis['valor']==1){
            $otrogrado++;
            $maximo=true;
          }
        }
      }

      /*if($dis['valor']==1)
        $discapaciad++;*/

  }
}


//die("---");

$carrera = utf8_decode($cabecera['carrera']['nombre']);
$ciclo= utf8_decode($cabecera['ciclo']['plan']['modalidad'] . " " . $cabecera['ciclo']['descripcion'] . " "  . $cabecera['year']);

$pdf = new FPDI('P','mm','Letter');
$pdf->setSourceFile("template2.pdf");
$tplIdx = $pdf->importPage(1, '/MediaBox');
$pdf->AddPage();
$pdf->useTemplate($tplIdx, 0, 0, 218);
$pdf->SetTextColor(255,255,255);
$pdf->SetFont('Helvetica','',8);
$pdf->Cell(188,5,"REPORTE DE DOCENTES DE BASE - "  . strtoupper($ciclo),0,1,'R');
$pdf->SetFont('Helvetica','B',12);
$pdf->Cell(188,5,$carrera,0,0,'R');
$pdf->Ln();
$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(188,5,$ciclo,0,0,'R');

$pdf->Ln(16);
$pdf->SetFillColor(22,111,117);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(120,5,utf8_decode("Descripción"),1,0,'L',true);
$pdf->Cell(68,5,"Cantidad",1,1,'L',true);

$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Helvetica','',9);

$pdf->Cell(120,5,utf8_decode('No. de Docentes'),1);
$pdf->Cell(68,5,$hombres+$mujeres,1,1);
$pdf->Cell(120,5,utf8_decode('No. de Docentes Hombres'),1);
$pdf->Cell(68,5,$hombres,1,1);
$pdf->Cell(120,5,utf8_decode('No. de Docentes Mujeres'),1);
$pdf->Cell(68,5,$mujeres,1,1);
$pdf->Cell(120,5,utf8_decode('Docentes de 19 Años o menos'),1);
$pdf->Cell(68,5,$De0a19,1,1);
$pdf->Cell(120,5,utf8_decode('Docentes de 20 a 24 Años'),1);
$pdf->Cell(68,5,$De20a24,1,1);
$pdf->Cell(120,5,utf8_decode('Docentes de 25 a 29 Años'),1);
$pdf->Cell(68,5,$De25a29,1,1);
$pdf->Cell(120,5,utf8_decode('Docentes de 30 Años o más'),1);
$pdf->Cell(68,5,$De30oMas,1,1);
$pdf->Cell(120,5,utf8_decode('Con discapaciad'),1);
$pdf->Cell(68,5,$discapaciad,1,1);
$pdf->Cell(120,5,utf8_decode('Extranjeros'),1);
$pdf->Cell(68,5,$extranjeros,1,1);
$pdf->Cell(120,5,utf8_decode('Docentes con grado doctorado'),1);
$pdf->Cell(68,5,$doctorado,1,1);
$pdf->Cell(120,5,utf8_decode('Docentes con grado maestria'),1);
$pdf->Cell(68,5,$maestria,1,1);
$pdf->Cell(120,5,utf8_decode('Docentes con grado especialidad'),1);
$pdf->Cell(68,5,$especialidad,1,1);
$pdf->Cell(120,5,utf8_decode('Docentes con grado licenciatura'),1);
$pdf->Cell(68,5,$licenciatura,1,1);
$pdf->Cell(120,5,utf8_decode('Docentes con grado tecnico'),1);
$pdf->Cell(68,5,$tecnico,1,1);
$pdf->Cell(120,5,utf8_decode('Docentes con otro grado'),1);
$pdf->Cell(68,5,$otrogrado,1,1);


/*foreach ($lista_des as $l) {
  //$pdf->Cell(120,5,utf8_decode($l['infraestructura']['tipo']),1);
  //$pdf->Cell(68,5,$l['cantidad'],1,1);
}*/
                  

$pdf->Output();
?>