<?php

$path="../";
$mod = array("carrera", "plan","ciclo", "infraestructura", "programa", "desglose_infraestructura");
include ('../app/modulos.php');

require('./fpdf/fpdf.php');
require('./fpdi/fpdi.php');

if(isset($_GET["id"])){
  if(trim($_GET["id"])!=""){
    $lista_des= des_infraestructura_lista_programa($_GET["id"]);
    $cabecera = programa($_GET["id"]);
    if($cabecera['id']==""){
      header("Location: ../infraestructurahis.php");
    }
  }
}else{
	header("Location: ../infraestructurahis.php");
}

$carrera = utf8_decode($cabecera['carrera']['nombre']);
$ciclo= utf8_decode($cabecera['ciclo']['plan']['modalidad'] . " " . $cabecera['ciclo']['descripcion'] . " "  . $cabecera['year']);

$pdf = new FPDI('P','mm','Letter');
$pdf->setSourceFile("template2.pdf");
$tplIdx = $pdf->importPage(1, '/MediaBox');
$pdf->AddPage();
$pdf->useTemplate($tplIdx, 0, 0, 218);
$pdf->SetTextColor(255,255,255);
$pdf->SetFont('Helvetica','',8);
$pdf->Cell(188,5,"REPORTE DE INFRAESTRUCTURA - "  . strtoupper($ciclo),0,1,'R');
$pdf->SetFont('Helvetica','B',12);
$pdf->Cell(188,5,$carrera,0,0,'R');
$pdf->Ln();
$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(188,5,$ciclo,0,0,'R');

$pdf->Ln(16);
$pdf->SetFillColor(22,111,117);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(120,5,"Tipo",1,0,'L',true);
$pdf->Cell(68,5,"Cantidad",1,1,'L',true);

$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Helvetica','',9);
foreach ($lista_des as $l) {
	$pdf->Cell(120,5,utf8_decode($l['infraestructura']['tipo']),1);
	$pdf->Cell(68,5,$l['cantidad'],1,1);
}
                  

$pdf->Output();
?>