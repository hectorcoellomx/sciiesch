<?php

$path="../";

$mod = array("carrera", "plan","ciclo","estado","municipio","persona","alumno","programa","grupo","historico_alumno","caracteristicas", "desglose_caracteristicas_alumno");

include ('../app/modulos.php');

require('./fpdf/fpdf.php');
require('./fpdi/fpdi.php');

if(isset($_GET["id"])){
  if(trim($_GET["id"])!=""){
    $lista_his= $lista_his= historico_alumno_lista_programa($_GET["id"]); // LISTA DE ALUM ASIGANOS A PROGRAMA
    $cabecera = programa($_GET["id"]);
    if($cabecera['id']==""){
      header("Location: docentehis.php");
    }
  }
}else{
  header("Location: ../alumnohis.php");
}


$hombres=0;
$mujeres=0;
$De0a19=0;
$De20a24=0;
$De25a29=0;
$De30oMas=0;
$discapaciad=0;
$extranjeros=0;


foreach ($lista_his as $l) {
  $y=date("Y", strtotime($l['alumno']['nacimiento']));
  $actual = $cabecera['year'];
  $edad= 1 * ($actual) - $y;
  
  if($edad<=19)
    $De0a19++;
  elseif($edad>=20 && $edad<=24)
    $De20a24++;
  elseif($edad>=25 && $edad<=29)
    $De25a29++;
  elseif($edad>=30)
    $De30oMas++;

  
  if($l['alumno']['genero']==1)
    $hombres++;
  else
    $mujeres++;

  if($l['alumno']['nacionalidad']=="extranjera")
    $extranjeros++;
  
  $mes= ($cabecera['ciclo']['mes_final']<10) ? "0" . $cabecera['ciclo']['mes_final'] : $cabecera['ciclo']['mes_final'];
  $fecha= "01-" . $mes . "-" . $cabecera['year'];

  $dis=desglose_caracteristicas_alumno(2,$l['alumno']['nocontrol'],strtotime($fecha),"consultar"); // 

  if($dis['valor']==1)
    $discapaciad++;
}

$carrera = utf8_decode($cabecera['carrera']['nombre']);
$ciclo= utf8_decode($cabecera['ciclo']['plan']['modalidad'] . " " . $cabecera['ciclo']['descripcion'] . " "  . $cabecera['year']);

$pdf = new FPDI('P','mm','Letter');
$pdf->setSourceFile("template2.pdf");
$tplIdx = $pdf->importPage(1, '/MediaBox');
$pdf->AddPage();
$pdf->useTemplate($tplIdx, 0, 0, 218);
$pdf->SetTextColor(255,255,255);
$pdf->SetFont('Helvetica','',8);
$pdf->Cell(188,5,"REPORTE GRAL DE ALUMNOS - " . strtoupper($ciclo),0,1,'R');
$pdf->SetFont('Helvetica','B',12);
$pdf->Cell(188,5,$carrera,0,0,'R');
$pdf->Ln();
$pdf->SetFont('Helvetica','',9);
//$pdf->Cell(188,5,$ciclo,0,0,'R');

$pdf->Ln(16);
$pdf->SetFillColor(22,111,117);
$pdf->SetFont('Helvetica','B',9);
$pdf->Cell(120,5,utf8_decode("Descripción"),1,0,'L',true);
$pdf->Cell(68,5,"Cantidad",1,1,'L',true);

$pdf->SetTextColor(0,0,0);
$pdf->SetFont('Helvetica','',9);

$pdf->Cell(120,5,utf8_decode('No. de Alumnos'),1);
$pdf->Cell(68,5,$hombres+$mujeres,1,1);
$pdf->Cell(120,5,utf8_decode('No. de Alumnos Hombres'),1);
$pdf->Cell(68,5,$hombres,1,1);
$pdf->Cell(120,5,utf8_decode('No. de Alumnos Mujeres'),1);
$pdf->Cell(68,5,$mujeres,1,1);
$pdf->Cell(120,5,utf8_decode('Alumnos de 19 Años o menos'),1);
$pdf->Cell(68,5,$De0a19,1,1);
$pdf->Cell(120,5,utf8_decode('Alumnos de 20 a 24 Años'),1);
$pdf->Cell(68,5,$De20a24,1,1);
$pdf->Cell(120,5,utf8_decode('Alumnos de 25 a 29 Años'),1);
$pdf->Cell(68,5,$De25a29,1,1);
$pdf->Cell(120,5,utf8_decode('Alumnos de 30 Años o más'),1);
$pdf->Cell(68,5,$De30oMas,1,1);
$pdf->Cell(120,5,utf8_decode('Con discapaciad'),1);
$pdf->Cell(68,5,$discapaciad,1,1);
$pdf->Cell(120,5,utf8_decode('Extranjeros'),1);
$pdf->Cell(68,5,$extranjeros,1,1);

/*foreach ($lista_des as $l) {
	//$pdf->Cell(120,5,utf8_decode($l['infraestructura']['tipo']),1);
	//$pdf->Cell(68,5,$l['cantidad'],1,1);
}*/


$pdf->Output();
?>
