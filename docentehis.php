<?php
session_start();
$id_sesion = session_id();
$mod = array("carrera", "plan","ciclo","estado","municipio","persona","docente","programa","historico_docente","caracteristicas", "desglose_caracteristicas_docente");

include ('app/modulos.php');
include ('app/sesion.php');

if(isset($_GET["id"])){
  if(trim($_GET["id"])!=""){
    $lista_his= historico_docente_lista_programa($_GET["id"]);
    $cabecera = programa($_GET["id"]);
    if($cabecera['id']==""){
      header("Location: docentehis.php");
    }
  }
}


$totalc=count(caracteristicas_lista_docentes());

?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=3; include 'inc/header.php'; ?>

    <section id="principal">

      <div class="container">
        <div class="row">
          <div class="col-md-10">

          <?php
            $lista_carreras= programa_carreras(); //$lista_carreras=carrera_lista();
            include ('inc/buscador.php');
          ?>

          <br><br>
          <?php

           if(isset($_GET["id"]) && trim($_GET["id"])!=""){


              echo "<div class='cabecera'>" . $cabecera['carrera']['nombre']  . " - " . $cabecera['ciclo']['plan']['modalidad'] . " (" . $cabecera['ciclo']['descripcion'] .  " " . $cabecera['year'] . ")</div>";

              echo "<div>";
              echo "<a class='boton btnmin' href='docentehisfrm.php?id=" . $_GET["id"] . "'>Agregar</a>";

              /*if(count($lista_his)>0){
                echo "<a class='boton btnmin btnright' href='./reporte/docente_tipo.php?id=" . $_GET["id"] . "&tipo=1' target='blank'>Reporte Carrera</a>";
                echo "<a class='boton btnmin btnright' href='./reporte/docente_tipo.php?id=" . $_GET["id"] . "&tipo=0' target='blank'>Reporte Asignatura</a>";
                echo "<a class='boton btnmin btnright' href='./reporte/docente.php?id=" . $_GET["id"] . "' target='blank'>Reporte General</a>";
              }*/


              echo "</div>";


            ?>
            <br>
            <?php if(is_array($lista_his)){ ?>
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="col-md-2">ID</th>
                 <th>Nombre</th>
                 <th class="col-md-2 acciones">Características</th>
                 <th class="col-md-1 acciones">Quitar</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $mes= ($cabecera['ciclo']['mes_final']<10) ? "0" . $cabecera['ciclo']['mes_final'] : $cabecera['ciclo']['mes_final'];
                  $fecha= "01-" . $mes . "-" . $cabecera['year'];
                  //echo $fecha;

                  foreach ($lista_his as $l) {

                    $totalcactivas= desglose_caracteristicas_check($l['docente']['nocontrol'],strtotime($fecha));
                      $st= ($l['docente']['edo']==0)? "des" : "";
                      echo "<tr class='" . $st ."'>";
                      echo "<td>" . $l['docente']['nocontrol'] . "</td><td>" . $l['docente']['nombre'] . " " . $l['docente']['paterno'] . " " . $l['docente']['materno'] . "</td>";
                      echo "<td class='acciones'><a href='docentecar.php?ciclo=" . $cabecera['ciclo']['id'] . "&year=".$cabecera['year']."&nocontrol=" . $l['docente']['nocontrol'] . "&historial=".$cabecera['id']."'>(" . $totalcactivas . "/" . $totalc . ")</a></td>";
                      echo "<td class='acciones'>";
                      echo "<a href='docentehisfrm.php?id=" . $cabecera['id'] . "&nocontrol=" . $l['docente']['nocontrol'] . "&type=eliminar'><i class='fa fa-minus' aria-hidden='true'></i></a>";
                      echo "</td>";
                      echo "</tr>";

                  }



                ?>
              </tbody>
            </table>
            <?php } } ?>
          </div>
          <div class="col-md-2">
              <div class="opciones">
                <span id="titulo">Docentes</span>
                  <a class="boton" href="docente.php">Ver lista</a>
              </div>

              <?php if(isset($_GET["id"]) && trim($_GET["id"])!=""){  ?>
                <br><br>
              <div class="opciones">
                <span id="titulo">Reportes</span>
                  <?php 

                  if(count($lista_his)>0){
                    echo "<a class='boton' href='./reporte/docente_tipo.php?id=" . $_GET["id"] . "&tipo=1' target='blank'>Carrera</a>";
                    echo "<a class='boton' href='./reporte/docente_tipo.php?id=" . $_GET["id"] . "&tipo=0' target='blank'>Asignatura</a>";
                    echo "<a class='boton' href='./reporte/docente.php?id=" . $_GET["id"] . "' target='blank'>General</a>";
                  }

                  ?>
              </div>

              <?php } ?>

          </div>
        </div>
      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
