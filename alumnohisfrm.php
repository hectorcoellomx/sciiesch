<?php
session_start();
$id_sesion = session_id();
$mod = array("carrera", "plan","ciclo","estado","municipio","persona","alumno","programa","grupo","historico_alumno");
include ('app/modulos.php');
include ('app/sesion.php');
// IS UPDATE
$update=false;
if(isset($_GET["type"]) && $_GET["type"]=="update"){
  if(isset($_GET["id"]) && isset($_GET["idi"])){
    if($_GET["id"]!=""){
      $update=true;
    }
  }
}else{

    // IS NORMAL (Necesita ID y GRUPO)
    if(isset($_GET["id"]) && isset($_GET["grupo"])){
      if(trim($_GET["id"])=="" || trim($_GET["grupo"])==""){
        header("Location: alumnohis.php");
      }
    }else{
      header("Location: alumnohis.php");
    }

    $tipo_curso= (isset($_GET["tipo"]))? $_GET["tipo"] : "";
    $grupo_curso= (isset($_GET["grupo"]))? $_GET["grupo"] : "";
}

$yaexiste=false;

 //POST SUBMIT
  if(isset($_GET["type"])){

    if($_GET["type"]=="agregar"){ // FOR CREATE
      historico_alumno_nuevo($_GET["id"],$_GET["nocontrol"],$_GET["tipo"],$_GET["grupo"]);
      header('Location: ?id='.$_GET["id"].'&grupo='.$_GET["grupo"].'&tipo='.$_GET["tipo"]);
    }elseif($_GET["type"]=="eliminar"){ // FOR UPDATE
      historico_alumno_delete($_GET["id"],$_GET["nocontrol"]);
      header('Location: alumnohis.php?id='.$_GET["id"]);
    }
  }

$cabecera= programa(trim($_GET['id']));

if($cabecera['id']==""){
  header("Location: alumnohis.php");
}


//$lista_grupos = grupo_lista();

?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=2; include 'inc/header.php'; ?>

    <section id="principal">

      <div class="container">
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6 center">

              <div class="frmtitulo">
                <?php if($update){ echo "Actualizar registro"; } else { echo "Agregar alumno"; } ?>
              </div>

              <?php
                echo "<div>" . $cabecera['carrera']['nombre']  . " - " . $cabecera['ciclo']['plan']['modalidad'] . " (" . $cabecera['ciclo']['descripcion'] .  " " . $cabecera['year'] . ")</div>";
                if($update)
                  echo "<b>" . $item['infraestructura']['tipo'] . "</b><br>";
               ?>
               <a href='alumnohis.php?id=<?php echo $cabecera["id"]; ?>&grupo=<?php echo $_GET["grupo"]; ?>' class="boton" style="display:inline-block">Regresar</a>

               <div class="row">
                  <?php if($tipo_curso==""){
                       echo "<br>Debes elegir un grupo y tipo de curso:<br><br>";
                       $sub="ELEGIR";
                     } else{
                       $sub="CAMBIAR";
                     }?>
                 <form id="frmgrupo" action="./alumnohisfrm.php" method="get">
                     Tipo de curso:
                     <select name="tipo">
                       <option value="0" <?php echo ($tipo_curso=="0")? "selected" : ""; ?>>
                        Normal
                       </option>
                       <option value="1" <?php echo ($tipo_curso=="1")? "selected" : ""; ?>>
                        Remedial
                       </option>
                     </select>
                     <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>">
                     <input type="hidden" name="grupo" value="<?php echo $_GET["grupo"]; ?>">
                     <input class="boton" type="submit" value="<?php echo $sub; ?>">
                 </form>
               </div>
               <br>
               <?php if($tipo_curso!="" && $grupo_curso!="") {

                      $cad = (isset($_GET['cadena']))? $_GET['cadena'] : "a";
                      $join= "LEFT JOIN historico_alumno ON nocontrol = nocontrol_alumno AND id_programa='".$_GET["id"]."' ";
                      $lista= alumno_lista_buscar($cad,6,$join);
                      ?>

                 Grupo elegido:
                 <?php
                 echo $grupo_curso . " - ";
                 echo ($tipo_curso==0)? "Normal" : "Remedial";
                 ?>
                 <div class="busqueda">
                  
                  <form method="get">
                  <input name="id" type="hidden" value="<?php echo $cabecera['id']; ?>">
                  <input name="grupo" type="hidden" value="<?php echo $grupo_curso; ?>">
                  <input name="tipo" type="hidden" value="<?php echo $tipo_curso; ?>">
                  <input id="cadena" name="cadena" type="text" placeholder="Nombre o Apellidos">
                  <input class="boton" style="display: inline-block; width: 75px;" type="submit" value="Buscar">
                   <!--input id="cadena" onkeyup="cargaralumnos('all',<?php echo $cabecera['id']; ?>)" type="text" placeholder="Nombre o Apellidos"> <?php ?>
                  <span style="display:inline-block" class="boton" aonclick="cargarAlumnos('normal',<?php echo $cabecera['id']; ?>)">Buscar</span><span id="filtro"></span-->                   
                  </form>
 
                 </div>

              <div class="fila row">
                <div class="tit col-md-12" id="alumnobuscar">

                  <table class="table table-bordered">
                  <thead>
                    <tr>
                     <th class="col-md-2">ID</th>
                     <th>Nombre</th>
                     <th class="col-md-1 acciones">Agregar</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                      if(is_array($lista)){
                        foreach ($lista as $l) {
                  
                          $alta=false;
                          if($l['nocontrol_alumno']!=""){
                            $alta=true;
                          }
                          
                            $st="";
                            if($l['edo']==0){
                              $st="des";
                            }
                            echo "<tr class='" . $st . "'>";
                            echo "<td>" . $alta . $l['nocontrol'] . "</td><td>" . $l['nombre'] . " " . $l['paterno'] . " " . $l['materno'] . "</td>";
                            echo "<td class='acciones'>";
                            if(!$alta){
                              echo "<a href='?id=" . $cabecera['id'] . "&nocontrol=" . $l['nocontrol'] . "&tipo=" . $tipo_curso . "&grupo=" . $grupo_curso . "&type=agregar'><i class='fa fa-plus' aria-hidden='true'></i></a>";
                            }else{
                              echo "Alta";
                            }
                            echo "</td>";
                            echo "</tr>";
                        }
                      }
                    ?>
                    </tbody>
                  </table>

                </div>
              </div>
              <?php } ?>
          </div>
          <div class="col-md-3"></div>
        </div>
      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>
    <?php if($yaexiste){
        echo "<script>alert('No se ha podido guardar el registo. Este dato ya existe registrado.');</script>";
      } ?>
  </body>
</html>
