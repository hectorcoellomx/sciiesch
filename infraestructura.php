<?php
session_start();
$id_sesion = session_id();
$mod = array("infraestructura");
include ('app/modulos.php');
include ('app/sesion.php');
?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=4; include 'inc/header.php'; ?>
    
    <?php

        if(isset($_GET["disp"])){
          if($_GET["disp"]=="desactivados"){
            $lista=infraestructura_lista_des();
            $visual=0; 
          }elseif($_GET["disp"]=="todos"){
            $lista=infraestructura_lista();
            $visual=2; 
          }
          else{
            $lista=infraestructura_lista_act();
            $visual=1; 
          }
        }else{
          $lista=infraestructura_lista_act();
          $visual=1; 
        }
    ?>

    <section id="principal">

      <div class="container">
        <?php rutas("infraestructurahis,infraestructura"); ?>
        <div class="row">
          <div class="col-md-10">
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="col-md-2">ID</th>
                 <th>Nombre</th>
                 <th class="col-md-1 acciones">Acción</th>
                </tr>
              </thead>
              <tbody>
                <?php 

                if(is_array($lista)){

                  foreach ($lista as $l) {
                    $st="";
                    if($l['edo']==0){
                      $st="des";
                    }
                    echo "<tr class='" . $st . "'>";
                    echo "<td>" . $l['id'] . "</td><td>" . $l['tipo'] . "</td>";
                    echo "<td class='acciones'>";
                    echo "<a href='infraestructurafrm.php?id=" . $l['id'] . "&type=update'><i class='fa fa-pencil' aria-hidden='true'></i></a>";
                    echo "</td>";  
                    echo "</tr>";  
                  }

                } 

                ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-2">
              <div class="opciones">
                <span id="titulo">Infraestructura</span>
                  <a class="boton" href="infraestructurafrm.php">Agregar</a>
              </div>
              <br>
              <div class="opciones">
                <span id="titulo">Visualización</span>
                  <a class="boton" href="infraestructura.php" <?php if($visual!=1) echo "style='opacity:0.5'" ?>>Activados</a>
                  <a class="boton" href="infraestructura.php?disp=desactivados" <?php if($visual!=0) echo "style='opacity:0.5'" ?>>Desactivados</a>
                  <a class="boton" href="infraestructura.php?disp=todos" <?php if($visual!=2) echo "style='opacity:0.5'" ?>>Todos</a>
              </div>
          </div> 
        </div>
      </div>
    
    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
