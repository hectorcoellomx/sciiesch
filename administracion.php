<?php
session_start();
$id_sesion = session_id();
$mod = array("usuario");
include ('app/modulos.php');
include ('app/sesion.php');

if($user['tipo']!=0){
  header('location:index.php'); die();
}

$tipo=0;
$update=false;
?>
<!DOCTYPE html>
<html>
  <head>
    <title>SCI IESCH</title>
    <?php include 'inc/head_common.php'; ?>
  </head>
  <body>
    <?php $menu=0; include 'inc/header.php'; ?>

    <?php $lista=usuario_lista(); ?>

    <section id="principal">

      <div class="container">

        <div class="row">
          <div class="col-md-10">
            <table class="table table-bordered">
              <thead>
                <tr>
                 <th class="col-md-2">ID</th>
                 <th class="col-md-2">Username</th>
                 <th class="col-md-5">Nombre</th>
                 <th>Tipo</th>
                 <th class="col-md-1 acciones">Acción</th>
                </tr>
              </thead>
              <tbody>
                <?php

                if(is_array($lista)){

                  foreach ($lista as $l) {

                    echo "<tr>";
                    echo "<td>" . $l['id'] . "</td>";
                    echo "<td>" . $l['username'] . "</td>";
                    echo "<td>" . $l['nombre'] . "</td>";
                    echo "<td>";
                    echo ($l['tipo']==0)? "Administrador" : "Personal";
                    echo "</td>";
                    echo "<td class='acciones'>";
                    echo "<a href='administracionfrm.php?id=" . $l['id'] . "&type=update'><i class='fa fa-pencil' aria-hidden='true'></i></a>";
                    if($l['id']!="1")
                      echo "<a onclick='loadWindow(500,175);loadScreens(\"inc/eliminar.php?id=" . $l['id'] . "&nombre=" . $l['nombre'] . "&entidad=usuario&finaldest=administracion.php\",\"window\",this);'><i class='fa fa-trash' aria-hidden='true'></i></a>";
                    echo "</td>";
                    echo "</tr>";
                  }

                }

                ?>
              </tbody>
            </table>
          </div>
          <div class="col-md-2">
              <div class="opciones">
                <span id="titulo">Administradores</span>
                  <a class="boton" href="administracionfrm.php">Agregar</a>
              </div>
          </div>
        </div>

      </div>

    </section>


    <?php include 'inc/footer.php'; ?>
    <?php include 'inc/footer_common.php'; ?>

  </body>
</html>
